#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 09:16:24 2018

Based on: https://likegeeks.com/python-gui-examples-tkinter-tutorial/

@author: farid
"""

from Tkinter import *
#from Tkinter.ttk import *
import ttk

#--------------------  first GUI window  --------------------
window = Tk()
# window geometry
window.geometry('350x200')
# window title
window.title("Welcome to Farid's validation benchmark app")
 
# Label
lbl = Label(window, text="Hello" , font=("Arial Bold", 20))
lbl.grid(column=0, row=0)

# Get input using entry class
txt = Entry(window,width=10)
# Disable entry widget
#txt = Entry(window,width=10, state='disabled')

txt.grid(column=0, row=5)
# Set focus to entry widget
txt.focus()

#-------------------- combobox widget --------------------
# Label
lbl = Label(window, text="Select your input:")
lbl.grid(column=0, row=8)
#Add a combobox widget
combo = Combobox(window)
combo['values']= (1, 2, 3, 4, 5, "Text")
combo.current(0) #set the selected item
combo.grid(column=1, row=8)

# get the select item from Combobox 
combo.get()
print "combo.get():", combo.get()

#------------------ Checkbutton widget -------------------
# Add a Checkbutton widget
chk_state = IntVar()
 
chk_state.set(1) #set check state
 
chk = Checkbutton(window, text='Choose', var=chk_state)
 
chk.grid(column=0, row=9)


#------------------ Radio buttons widgets -------------------
rad1 = Radiobutton(window,text='First', value=1)
rad2 = Radiobutton(window,text='Second', value=2)
rad3 = Radiobutton(window,text='Third', value=3)
 
rad1.grid(column=0, row=10)
rad2.grid(column=1, row=10)
rad3.grid(column=2, row=10)


#Handle button click event
def clicked():
    res = "Welcome to " + txt.get()
    lbl.configure(text=res)
    
# Add button
btn = Button(window, text="Select", command=clicked)
btn.grid(column=1, row=0)


btn = Button(window, text="Run", bg="orange", fg="red", command=clicked)
btn.grid(column=2, row=12)







window.mainloop()

