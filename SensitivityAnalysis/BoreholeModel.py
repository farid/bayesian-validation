#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 14:48:07 2018

@author: farid
"""
import math
import numpy as np

def BoreholeModel(xx):
    """
    % BOREHOLE FUNCTION
    %
    % Authors: Sonja Surjanovic, Simon Fraser University
    %          Derek Bingham, Simon Fraser University
    % Questions/Comments: Please email Derek Bingham at dbingham@stat.sfu.ca.
    %
    % Copyright 2013. Derek Bingham, Simon Fraser University.
    %
    % THERE IS NO WARRANTY, EXPRESS OR IMPLIED. WE DO NOT ASSUME ANY LIABILITY
    % FOR THE USE OF THIS SOFTWARE.  If software is modified to produce
    % derivative works, such modified software should be clearly marked.
    % Additionally, this program is free software; you can redistribute it 
    % and/or modify it under the terms of the GNU General Public License as 
    % published by the Free Software Foundation; version 2.0 of the License. 
    % Accordingly, this program is distributed in the hope that it will be 
    % useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
    % of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
    % General Public License for more details.
    %
    % For function details and reference information, see:
    % https://www.sfu.ca/~ssurjano/borehole.html
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % OUTPUT AND INPUT:
    %
    % y  = water flow rate
    % xx = [rw, r, Tu, Hu, Tl, Hl, L, Kw]
    %
    """
    
    rw = xx[:,0]
    r  = xx[:,1]
    Tu = xx[:,2]
    Hu = xx[:,3]
    Tl = xx[:,4]
    Hl = xx[:,5]
    L  = xx[:,6]
    Kw = xx[:,7]
    
    frac1 = 2 * math.pi * Tu * (Hu-Hl)
    
    frac2a = 2 * L * Tu / (np.log(r/rw) * rw**2 * Kw)
    frac2b = Tu / Tl
    frac2 = np.log(r/rw) * (1+frac2a+frac2b)
    
    y = frac1 / frac2
    
    return np.array([y[:,None]])