#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 2018

Sobol’ sensitivity analysis

@author: Farid Mohammadi
"""
import warnings
warnings.filterwarnings("ignore")
import sys
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
plt.rcParams.update({'font.size': 20})
from SALib.sample import saltelli
from SALib.analyze import sobol
#from SALib.test_functions import Ishigami
import Ishigami
import aPC_Sensitivity
import numpy as np
import math
from tqdm import tqdm
import os
from scipy import stats
import scipy as sp
from sklearn import metrics
from math import sqrt
import time
import seaborn as sns; sns.set(style="ticks", color_codes=True)
import pandas as pd

import BoreholeModel


def Visualization(NofPa, SobolIndices):
    import matplotlib.pyplot as plt
    fig = plt.figure()

    fig.set_figheight(12)
    fig.set_figwidth(20)
    
    objects = []
    for pid in range(1,NofPa+1):
        objects.append('$P_%s$'%pid)
    

    y_pos = np.arange(len(objects))
    
    plt.bar(y_pos, SobolIndices['ST'], align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Total Sobol indices, $S^T$')
    plt.title('Borehole Model')
    
    fig.savefig('TotalSensitivity.png')   # save the figure to file
    plt.close(fig)

#-------------------------------------------------------------------------------------------------------   
def plot_Histogram(Post_MC_Vector,Post_MC_Weights, problem, Measurement, FigName):
    """
    This Function plots the histogram of posterior parameter sets.
    """
    
    print("Post_MC_Vector:\n", Post_MC_Vector)
    header1 = ['P_%s'%(i+1) for i in range(problem.NofPa)]

    df = pd.DataFrame(Post_MC_Vector, columns = problem.ParameterNames)
    
    ########################################################
    #                PairPlot with Seaborn
    ########################################################        
    def histMoments(sns_plot,df,NofPa):
        for idx, x1 in enumerate(df):
            ax = sns_plot.fig.axes[idx*NofPa+idx]
            anchored_text = AnchoredText("$\mu$=%.4g \n $\sigma$=%.4g \n $S$=%.4f \n $k$=%.4f" %(np.mean(df[x1]), np.std(df[x1]), stats.skew(df[x1]), stats.kurtosis(df[x1])), loc=2)
            ax.add_artist(anchored_text)    
        
    
    g = sns.PairGrid(df, palette=["red"])
    g.map_upper(sns.kdeplot, cmap="Blues_d",shade=False)
    sns_plot = g.map_diag(sns.distplot, hist=True, kde=False, color="b")
    histMoments(sns_plot,df,problem.NofPa)
    #g.map_diag(corrfunc)
    g.map_lower(sns.kdeplot, cmap="Blues_d",shade=False)


    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Outputs')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    sns_plot.savefig(FigName +'.png')
    
    # Save POSTERIOR parameter sets with their weights
    np.savetxt(FigName+'.txt', np.hstack((Post_MC_Vector, Post_MC_Weights)), delimiter=',',header= " ".join(header1)+ "   Weight")
    
    os.chdir('..')
    
    # Histogram of the model output for posterior parameters (Post_MC_Vector)
    
    TotalOutput = Ishigami.Isgigami(Post_MC_Vector)
    
    f, ax_hist = plt.subplots(1, sharex=True)
    sns.distplot(TotalOutput[0], hist = False, ax = ax_hist, label="Model Outputs")
    ax_hist.axvline(np.mean(TotalOutput[0]) , color='r', linestyle='--')
    
    # Histogram of the observation
    mu, sigma = Measurement.Observations[0][0], Measurement.MeasurementError[0] # mean and standard deviation
    y = np.random.normal(mu, sigma, 1000)
    sns.distplot(y, hist = False, ax = ax_hist, color = 'g', label="Observation")
    ax_hist.axvline(np.mean(y), color='g', linestyle='--')
    plt.legend()
    f.show()

    #plt.savefig(Name +'_Outputs'+'.png')   # save the figure to file

#-------------------------------------------------------------------------------------------------------    
def SobolIndicesSALib(SurrogateParams,problem):
    # Generate samples MCSize*(2Nofpa+2) / calc_second_order=False >> MCSize*(Nofpa+2)
 
    Samples = saltelli.sample(problem.ProblemDict, problem.MCSize, calc_second_order = False)
    
    # Save the parameter sets (Samples) in a text file
    Header = ['x_%s, '%(i+1) for i in range(problem.ProblemDict['num_vars'])]
    np.savetxt('Samples.txt', Samples, delimiter = ',', header = " ".join(Header))
    
    # Run model (example)
    #TotalOutput = BoreholeModel.BoreholeModel(Samples)
    TotalOutput = Ishigami.Isgigami(Samples)

    # Compute the Sobol indices
    SobolIndices = sobol.analyze(problem.ProblemDict, TotalOutput.flatten(), print_to_console=True, calc_second_order=False)
    
    print("SobolTotal:",SobolIndices['ST'])
    #==================================================================
    #============     Visualisation of Sobol Indices       ============     
    #==================================================================
    objects = []
    for pid in range(1,problem.NofPa+1):
        objects.append('$P_%s$'%pid)
    

    y_pos = np.arange(len(objects))
    
    plt.bar(y_pos, SobolIndices['ST'], align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Total Sobol indices, $S^T$')
#    plt.ylabel('First-order Sobol indices, $S_1$')
    plt.title('Parameters')
    
    plt.show()
    
    return SobolIndices

#-------------------------------------------------------------------------------------------------------
def SobolIndicesAPC(SurrogateParams,problem,ItrNr,PolynomialDegree,TotalRSCoeffs):
    #TODO: Generalize it for AaPCE
    print(">>> Post-Processing on Arbitraty Polynomial Chaos<<< \n")
    #Sobol Indices: Computationnal of Location and Var
    P=len(PolynomialDegree)
    #=========================================================================
    #==============     Computation of Sobol Indices   =======================
    #=========================================================================
    for VarIdx in range(problem.NofVar):
        Coeffs = TotalRSCoeffs[VarIdx].flatten()
        if ItrNr !=0:
            P=len(PolynomialDegree[VarIdx])
            PolynomialDegree = PolynomialDegree[VarIdx]
        #print("Coeffs for Sobol:\n", Coeffs)
        
        # ------------------- Total Sobol Indices -------------------
        TermsVar=[]
    
        for j in range(0,problem.NofPa):
            location=np.where(PolynomialDegree[:,j] != 0)[0]#[0]
            #print("location:\n", PolynomialDegree[:,j], location)
            TermsVar.append(np.sum(Coeffs[location]**2)/np.sum(np.square(Coeffs[1:])))
        SobolTotal = TermsVar
    
    
    print("SobolTotal:",SobolTotal)
    
    #=========================================================================
    #========== Visualisation of Total Sobol Indices   =======================
    #=========================================================================
    objects = []
    for pid in range(1,problem.NofPa+1):
        objects.append('$P_%s$'%pid)
    

    y_pos = np.arange(len(objects))
    
    plt.bar(y_pos, SobolTotal, align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Total Sobol indices, $S^T$')
    plt.title('Parameters')
    
    plt.show()
    
    return SobolTotal

#-------------------------------------------------------------------------------------------------------
def RS_calculation_aPC(SurrogateParams,Measurement,problem,ItrNr, Model_Output, OptimalCollocationPoints, PolynomialDegrees, PolynomialBasisFileName):
    """
    calculates the response surface for each measurement location
    So each row of the RSTotal matrix is the coefficients of
    one response surface for a specific measurement location

    NofMeasurements: Number of measurement locations
    NofPa: Number of sensitive parameters
    k= will be the output of the TELEMAC runs for each location

    attributes: array1, system_type, NofMeasurements SurrogateParams.MultiplierPCM , SurrogateParams.NoOfIteration
    """
    P = math.factorial(problem.NofPa+SurrogateParams.PceDegree)//(math.factorial(problem.NofPa) * math.factorial(SurrogateParams.PceDegree)) #Total number of terms for aPCE
#    RSTotal = np.zeros((NofVar, P+ItrNr , NofMeasurements))
    RSTotal = [[] for i in range(problem.NofVar)]
    Psi_RS = [[] for i in range(problem.NofVar)]
#    Psi_RS = np.zeros((NofVar, P+ItrNr , P+ItrNr))

    
    #----------------------------------------------------
    # Calculate RSCoeffs & Psi for all outputs (variants)
    #----------------------------------------------------
    for VarIdx in range(problem.NofVar):
        # Handle the first RS calculation
        if ItrNr !=0: 
            NewPolynomialDegrees = PolynomialDegrees[VarIdx] #[i] This will be used when each measurement point has its own Polynomial Degrees
        else:        
            NewPolynomialDegrees = PolynomialDegrees
            
            
        RSTotal[VarIdx] , Psi_RS[VarIdx]  = aPC_Sensitivity.RS_Builder(SurrogateParams, problem, Model_Output[VarIdx],OptimalCollocationPoints,
                                                                       NewPolynomialDegrees, PolynomialBasisFileName)

    return RSTotal, Psi_RS

#-------------------------------------------------------------------------------------------------------
def Likelihood(SurrogateParams, problem,ItrNr,Psi_W_old,Parameterset, RSTotal, Measurement,PolynomialBasisFileName,PolynomialDegrees):
    """
    This function calculates the weights of the each parameter set based on the difference of
    its output of RS and the corresponding observation value.
    
    RSTotal = Coeffs
    NofMeasurements: Number of the measurements
    MCsize: Number of the parameter sets
    """
    
    P = math.factorial(problem.NofPa+SurrogateParams.PceDegree )//(math.factorial(problem.NofPa) * math.factorial(SurrogateParams.PceDegree)) #Total number of terms
    ParametersetSize = Parameterset.shape[0]

    Weights=np.zeros((ParametersetSize, problem.NofVar)) 
    #ObservationSize = Observations.shape[1]
    OutputRS=np.empty((0, Measurement.NofMeasurements))
    #Psi_W = np.zeros((NofVar, ParametersetSize , P+ItrNr))
    Psi_W = [[] for i in range(problem.NofVar)]
    #Loop over the variants (Data Sets)
    for VarIdx in tqdm(range(problem.NofVar), ascii=True, desc='Computing likelihoods'):
        
        # ----------- Create Psi Matrix for all parameter sets ----------------
        
        if ItrNr == 0:
            start_time = time.time()
            Psi_W[VarIdx] = aPC_Sensitivity.Psi_Creater(SurrogateParams,problem,Parameterset, PolynomialBasisFileName,PolynomialDegrees)
            elapsed_time = time.time() - start_time
            print("elapsed_time for likelihood computations:", elapsed_time)
        
        else:
            if Psi_W_old is None:
                Psi_W_Old =None
            else:
                Psi_W_Old=Psi_W_old[VarIdx]
                
            Psi_W[VarIdx] = aPC_Sensitivity.Psi_Creator_Weight(SurrogateParams ,problem, Psi_W_Old,Parameterset, PolynomialBasisFileName,PolynomialDegrees[VarIdx],Measurement.NofMeasurements)
            
        #Calculation of the outputs of Response surfaces and their mean and Stdev
        OutputRS = np.dot(Psi_W[VarIdx], RSTotal[VarIdx])  

        # ========================================================================
        Measurement_Error=np.zeros((Measurement.NofMeasurements, Measurement.NofMeasurements), float)
        np.fill_diagonal(Measurement_Error, Measurement.MeasurementError[VarIdx]**2)
        
        #------------------------------------------------------------------------
        Deviation=np.zeros((ParametersetSize,Measurement.NofMeasurements))
        for i in range(ParametersetSize):
            Deviation[i] = Measurement.Observations[VarIdx] - OutputRS[i]
            
        # Computation of Weight according to the Deviation
        for i in range(ParametersetSize):
            Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Measurement_Error)), Deviation[i,:][:,None])))/(((2*np.pi)**(Measurement.NofMeasurements/2)) * np.sqrt(np.linalg.det(Measurement_Error)))
           
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    Weights = np.prod( Weights, axis=1)

    return Weights[:,None], Psi_W
    
#-------------------------------------------------------------------------------------------------------
def CorrectedLeaveoutError(TotalPsi, Coeffs, TotalModelOutputs, Measurement, ItrNr):
    """
    This is based of the following paper:
        ""Blatman, G., & Sudret, B. (2011). Adaptive sparse polynomial chaos expansion based on least angle regression. 
        Journal of Computational Physics, 230(6), 2345-2367.""
    Psi: the orthogonal polynomials of the response surface
    """
    
    h_i = []
    Q2 = [[] for i in range(len(Measurement.Variants))]

    for VarIdx in range(len(Measurement.Variants)):
        Psi = TotalPsi[VarIdx] 
        P = Psi.shape[1]  # NrTerm of aPCEs 
        N = Psi.shape[0]  # NrEvaluation
        ModelOutputs = TotalModelOutputs[VarIdx]
        
        # Build the projection matrix
        M = np.dot(Psi.T, Psi)
       
        #ProjectionMatrix = np.identity(N) - np.dot(np.dot(Psi,np.linalg.inv(M)),Psi.T) # Wrong Formula for projection matrix (residual operator)
        ProjectionMatrix = np.dot(np.dot(Psi,np.linalg.inv(M)),Psi.T)
        #print("ProjectionMatrix:\n",ProjectionMatrix)
        
        for j in range(N):
            h_i.append(ProjectionMatrix[j,j])

        # Loop over all measurement points and save the Q_2  
        for PointIdx in range(Measurement.NofMeasurements):

            # Calculate Error Loocv for each measurement point
            PredictedResidual = np.zeros((N,1))
            for i in range(N):
                # Response surface coeffs when removing i-th observation (Coeffs correspoding to the experimental design)   
                inv_PsiPsi = np.linalg.pinv(np.dot(np.delete(Psi, (i), axis=0).T, np.delete(Psi, (i), axis=0)))    
                expCoeffs = np.dot(np.dot(inv_PsiPsi,np.delete(Psi, (i), axis=0).T), np.delete(ModelOutputs, (i), axis=0))
                
                # Calculate the perdicted residual without considering the ith dataset
                PredictedResidual[i] =  (ModelOutputs[i] - np.dot(Psi[i], expCoeffs)) // h_i[i]
                

            # PRESS (Predicted Residual Sum of Squares / Jacknife error)
            ErrLoo = np.sum(np.square(PredictedResidual)) / N
            Var = np.var(ModelOutputs, ddof = 1)
            
            # Corrected Error for over-determined system
            if N != P:
                # Chapelle et al. (2002)
#                Cemp = np.dot(Psi.T,Psi)/ N
#                T = (N/(N-P)) * (1 + np.trace(np.linalg.inv(Cemp))/N)
                # Adjusted emperical error; Blatman (2010)
                T = (N - 1) // (N - P - 1) 

                CorrectedErrLoo = ErrLoo * T
                EpsilonLOO = CorrectedErrLoo / Var
            else:
                EpsilonLOO = ErrLoo / Var

            Q_2 = 1 - EpsilonLOO
            Q2[VarIdx].append(Q_2)    
    print("This is the Q_2 only for the first measurement point:", Q2)
    return Q2

#-------------------------------------------------------------------------------------------------------
def selection_max_weight(Parameterset, CollocationPoints, Weight, OldOutput, Measurement, NoItr, problem):
        """ This function does the following operations:
        1) Computes the weights of the optimal integration points in each step
        2) Sorts the parametersets based on the weights calculated in Step1
        3) Computes the weights of the parameter sets
        4) Selects the parameterset with the best weight
        5) Checks if already exist in the optimal integration points (CollocationPoints)
        6) Run the model for the new collocation point

        Output:
        Updated integration points for solving an overdetermined system of equations
        """

        print(80 * '-')
        print('---> Selection of the Parameter Set with Maximum Weight \n')
        
        Parametersets_weight = np.hstack((Parameterset, Weight))
        sorted_Parametersets_weight = Parametersets_weight[np.argsort(Parametersets_weight[:, problem.NofPa])][::-1] # NofPa+2: Sort based on the weights
        
        # Check if the parameter set already exists in the collocation points.
        i = 0
        while i < len(Parameterset):
            NewCollocationPoint = sorted_Parametersets_weight[i, 0:problem.NofPa]
            
            # If doesn't exist, break; else take the next one. 
            if any(item == False for item in np.isin(NewCollocationPoint , CollocationPoints)):
                break
            else:
                print('-' * 50)
                print('The previous collocation point is selected again!!')
                print('Do not panic! We can try to select the next parameter set as new collocation point')
                print('-' * 50)
                NewCollocationPoint=sorted_Parametersets_weight[i, 0:problem.NofPa]
                i += 1
            

        print('-' * 50)
        print("NewCollocationPoint:", NewCollocationPoint)
        print('-' * 50)
        
        O_new=np.vstack((CollocationPoints, NewCollocationPoint))
        
        # Run the original model for the new collocation point
        #Output = BoreholeModel.BoreholeModel(NewCollocationPoint[:,None].T)
        Output = Ishigami.Isgigami(NewCollocationPoint[:,None].T)
        
        NewTotalOutput=np.zeros((problem.NofVar,len(O_new),Measurement.NofMeasurements))
        for VarIdx in range(problem.NofVar):
                NewTotalOutput[VarIdx,:,:] = np.vstack((OldOutput[VarIdx] , Output[VarIdx]))
        
        return O_new, NewTotalOutput

#------------------------------------------------------------------------------
def filtration_prior(Parameterset, Weights, problem):
    """Rejection Sampling: filtration of prior distribution via uniform"""
    ii=0
    unif=np.random.rand(1, problem.MCSize)
    Parameterset_weight=np.hstack((Parameterset, Weights))
    Post_MC_Vector=np.empty((0, problem.NofPa+1))
    
    for i in range(len(Weights)):
        if Weights[i, :]/np.max(Weights) > unif[:, i]:
            ii += 1
            Post_MC_Vector = np.vstack((Post_MC_Vector, Parameterset_weight[i, :]))
    
    return Post_MC_Vector[:, 0:problem.NofPa], Post_MC_Vector[:, problem.NofPa][:,None]

#------------------------------------------------------------------------------
def BME_Corr_Weight(SurrogateParams, problem,ItrNr,Parametersets, RSTotal, OutputOrig, Measurement,PolynomialBasisFileName,PolynomialDegrees):
    """
    Calculates the correction factor for BMEs.
    """
    P = math.factorial(problem.NofPa+SurrogateParams.PceDegree)//(math.factorial(problem.NofPa) * math.factorial(SurrogateParams.PceDegree)) #Total number of terms
    ParametersetSize = Parametersets.shape[0]
    OutputRS = np.empty((0, Measurement.NofMeasurements))
    BME_RM_ModelWeight = np.zeros((ParametersetSize, problem.NofVar)) 
    BME_RM_DataWeight = np.zeros((ParametersetSize, problem.NofVar)) 
    BME_Corr = np.zeros((1,problem.NofVar))
    #z=np.ones((ParametersetSize, 1))
    #a=np.hstack((z, Parameterset))
    
    #Scaling Factor
    Normalization=10**(100) # ???


    for VarIdx in tqdm(range(problem.NofVar), ascii=True, desc='Computing Correction Factor for BMEs'):
        # Create Psi Matrix
        
        if ItrNr == 0:
            NewPolynomialDegree = PolynomialDegrees
        else:
            NewPolynomialDegree = PolynomialDegrees[VarIdx]
        
        #Psi = aPC_Sensitivity.Psi_Creator_Weight(MainDir,N,d,newItrNr,None,Parametersets, PolynomialBasisFileName,NewPolynomialDegree,NofMeasurements)
        Psi = aPC_Sensitivity.Psi_Creater(SurrogateParams,problem,Parametersets, PolynomialBasisFileName,NewPolynomialDegree)
        
        #Calculation of the outputs of Response surfaces
        OutputRS = np.dot(Psi, RSTotal[VarIdx])
        
        # Deviation Computations
        RM_Model_Deviation = np.zeros((ParametersetSize,Measurement.NofMeasurements)) 
        RM_Data_Deviation = np.zeros((ParametersetSize,Measurement.NofMeasurements)) 
        for i in range(ParametersetSize):
            RM_Model_Deviation[i] = OutputOrig[VarIdx,i] - OutputRS[i, :] # Reduce model- Full Model
            RM_Data_Deviation[i] = Measurement.Observations[VarIdx] - OutputRS[i, :] # Reduce model- Measurement Data
        
        # Initialization  of Co-Variance Matrix
        # For BME_RM_ModelWeight
        if Measurement.NofMeasurements == 1:
            RM_Model_Error = np.zeros((Measurement.NofMeasurements, Measurement.NofMeasurements), float)
            np.fill_diagonal(RM_Model_Error, np.cov(RM_Model_Deviation.T))
        else: 
            RM_Model_Error = np.cov(RM_Model_Deviation)
        
        # For BME_RM_DataWeight
        Measurement_Error=np.zeros((Measurement.NofMeasurements, Measurement.NofMeasurements), float)
        np.fill_diagonal(Measurement_Error, Measurement.MeasurementError[VarIdx]**2)
 
        
        # Computation of Weight according to the deviations
        for i in range(ParametersetSize):
            #Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(CovarianceMatrix)), Deviation[i,:])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(CovarianceMatrix)))
 
            BME_RM_ModelWeight[i,VarIdx] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** Measurement.NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Model_Deviation[i], np.linalg.pinv(RM_Model_Error)), RM_Model_Deviation[i]))
            BME_RM_DataWeight[i,VarIdx] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** Measurement.NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Data_Deviation[i], np.linalg.pinv(Measurement_Error)), RM_Data_Deviation[i]))

        BME_Corr[0,VarIdx] = 0
        for i in range(ParametersetSize):
            BME_Corr[0,VarIdx] = BME_Corr[0,VarIdx] + BME_RM_ModelWeight[i,VarIdx] * BME_RM_DataWeight[i,VarIdx] / np.sum(BME_RM_DataWeight[:,VarIdx])
    
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    BME_Corr_Factor = np.prod(BME_Corr,axis=1)

    return BME_Corr_Factor

#------------------------------------------------------------------------------
    
def ResponseSurfaceaPC(SurrogateParams,problem, Measurement):
    
    P = math.factorial(problem.NofPa+ SurrogateParams.PceDegree)//(math.factorial(problem.NofPa) * math.factorial(SurrogateParams.PceDegree))
    # Initialize a matrix to store the Q2 values of LOOCV for each iterations
    CLOOCV = np.zeros((SurrogateParams.NoOfIteration+1,problem.NofVar,Measurement.NofMeasurements))
    ItrNr = 0
    
    # Here, the distributions of the inputDistributions,OptimalCollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName
    inputDistributions,OptimalCollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName = aPC_Sensitivity.CollocationPoints(SurrogateParams, problem)
    
    # Run the origina model at collocation points
    #TotalOutput = BoreholeModel.BoreholeModel(OptimalCollocationPointsBase)
    TotalOutput = Ishigami.Isgigami(OptimalCollocationPointsBase)

    # Calculate the coefficients for the initial surrogate model
    RSCoeffs, Psi = RS_calculation_aPC(SurrogateParams, Measurement, problem,ItrNr, TotalOutput, OptimalCollocationPointsBase, PolynomialDegrees, PolynomialBasisFileName)

    #=====================================================================
    #================== Post-processing of the aPCE ======================
    #=====================================================================
    print("\nRSOutput:\n", np.dot(Psi[0],RSCoeffs[0])) 
    print("\nTotalOutput:\n", TotalOutput.flatten())  
    print("\nDiff:\n", np.mean(TotalOutput.flatten() - np.dot(Psi[0],RSCoeffs[0])))    
    
    
    print('\n---> Computation of output statistics: mean and variance...\n')
    #Analytical form of mean and variance 
    RSOutputMean = RSCoeffs[0][0,0]#[0,0,0]
    RSOutputVar = np.sum(RSCoeffs[0][1:P,0]**2)
    print("\nMean, Variance:", RSOutputMean, RSOutputVar)
    
    print('\n---> Computation of the accuracy of the response surface...\n')
    
    R2 = metrics.r2_score(TotalOutput.flatten(), np.dot(Psi[0],RSCoeffs[0]).flatten())

    RSME = sqrt(metrics.mean_squared_error(TotalOutput.flatten(), np.dot(Psi[0],RSCoeffs[0]).flatten()))
    print("\nRSME, R2:",RSME, R2)
    # Leave-out-one cross validation of the response surface
    CLOOCV[ItrNr,:,:] = CorrectedLeaveoutError(Psi, RSCoeffs, TotalOutput,Measurement , ItrNr)
    
    print('\n---> Computation of the sobol indices from the PCE coefficients...\n')
    # Sobol indices based on aPCE coeffs
    SobolIndicesAPC(SurrogateParams ,problem,ItrNr,PolynomialDegrees,RSCoeffs)
    SobolIndicesSALib(SurrogateParams , problem)
    
    print('\n---> Computation of the likelihoods using the PCE model...\n')
    # Compute likelihoods
    Likelihoods, Psi_W = Likelihood(SurrogateParams,problem,ItrNr,None,inputDistributions, RSCoeffs, Measurement,PolynomialBasisFileName,PolynomialDegrees)
    
    # Compute BME
    BME = np.mean(Likelihoods)
    
    # BME Correction
    Corr_Weight = BME_Corr_Weight(SurrogateParams,problem,ItrNr,OptimalCollocationPointsBase,RSCoeffs,
                                  TotalOutput, Measurement, PolynomialBasisFileName, PolynomialDegrees)   
    corrFactorBME = np.mean(Corr_Weight)
    print("corrFactorBME:", corrFactorBME) 
    #=====================================================================
    #==================     Bayesian Updating     ========================   
    #=====================================================================
    BayesianEvidence=np.empty((0, 1))
    
    for ItrNr in range(SurrogateParams.NoOfIteration+1):
        if ItrNr == 0:
           
           NewPolynomialDegrees = PolynomialDegrees
           NewCollocationPoints=OptimalCollocationPointsBase
           NewTotalOutput = TotalOutput
           NewLikelihoods = Likelihoods
           BayesianEvidence=np.append(BayesianEvidence,  BME * corrFactorBME)
        
        else:
            # Select and run the original model for the parameter set with the highest likelihood
            NewCollocationPoints, NewTotalOutput = selection_max_weight(inputDistributions, NewCollocationPoints, NewLikelihoods, NewTotalOutput, Measurement, ItrNr, problem)
            
            # Select new PolynomialDgrees based on BME
            NewPolynomialDegrees = aPC_Sensitivity.NewTerm_Selection(SurrogateParams,ItrNr,problem,Measurement,NewCollocationPoints,
                                                                     NewTotalOutput,NewPolynomialDegrees,PolynomialBasisFileName)
                                                
            print("\nNewPolynomialDegrees:\n", NewPolynomialDegrees)
            
            # Update the coeffs of the response surface
            
            RSCoeffs, Psi = RS_calculation_aPC(SurrogateParams,Measurement,problem,ItrNr, NewTotalOutput, NewCollocationPoints,
                                               NewPolynomialDegrees, PolynomialBasisFileName)
            
            # Sobol indices based on aPCE coeffs
            SobolIndicesAPC(SurrogateParams,problem,ItrNr,NewPolynomialDegrees,RSCoeffs)
            
            R2 = metrics.r2_score(NewTotalOutput.flatten(), np.dot(Psi[0],RSCoeffs[0]).flatten())

            RSME = sqrt(metrics.mean_squared_error(NewTotalOutput.flatten(), np.dot(Psi[0],RSCoeffs[0]).flatten()))
            print("\nRSME, R2 of Itr %i:"%ItrNr ,RSME, R2)
            
            # Leave-out-one cross validation of the response surface
            CLOOCV[ItrNr,:,:] = CorrectedLeaveoutError(Psi, RSCoeffs, NewTotalOutput, Measurement, ItrNr)
            print("\nCLOOCV:\n", CLOOCV)
            
            
            Likelihoods, Psi_W = Likelihood(SurrogateParams, problem, ItrNr, Psi_W, inputDistributions, RSCoeffs,
                                            Measurement, PolynomialBasisFileName,NewPolynomialDegrees)

            # BME Calculation
            BME = np.mean(Likelihoods) # Bayesian Evidence
            
            print("\nBME:" , BME)
            
            # BME Correction
            Corr_Weight = BME_Corr_Weight(SurrogateParams, problem, ItrNr, NewCollocationPoints, RSCoeffs,
                                          NewTotalOutput, Measurement, PolynomialBasisFileName, NewPolynomialDegrees)
    
            corrFactorBME = np.mean(Corr_Weight)
            BayesianEvidence = np.append(BayesianEvidence, BME * corrFactorBME)
            
    print("\nBayesian Evidence", BayesianEvidence)   
    # =========================================================================
    # =================  Prior & Posterior Distributions ======================
    # =========================================================================
    # Rejection Sampling: filtration of prior distribution via uniform to get posterior
    Post_MC_Vector,Post_MC_Weights = filtration_prior(inputDistributions, Likelihoods, problem)
    print('-' * 50)

    print('\n---> Histogram of Posteriors for calibration stage\n')
    plot_Histogram(Post_MC_Vector, Post_MC_Weights, problem, Measurement, 'Histogram_Posterior_calib')
    
    
    return RSCoeffs, Psi, Likelihoods

###################################################################################################
####################################### MAIN PROGRAM ##############################################
###################################################################################################
            

if __name__ == "__main__":

    #------------- CLASSES -------------
    class ParameterClass:
         def __init__(self):
             self.MainDir = os.getcwd()+"/"
             self.Variants = ['f(x)'] 
             self.NofVar = len(self.Variants) # Nr of SRQs
             self.ParameterNames = ['X1','X2','X3']
             self.NofPa = len(self.ParameterNames)
             self.Distributions = ['unif','unif','unif']
             self.Parameters = []
             self.MCSize = 1000
             self.ProblemDict = {}
    
    class MeasurementClass:
        def __init__(self):
            self.Observations = np.array([[1.68507124]]) 
            self.MeasurementError =  np.array([0.025])
            self.NofMeasurements = 1 # Nr or measurement points
    
    class SurrogateParamsClass:
        def __init__(self):
            self.PceDegree = 7
            self.MultiplierPCM = 2 
            self.NoOfIteration = 5
    
    #=====================================================
    #==================   INPUTS  ========================  
    #=====================================================
    
    #------------- Uncertain Parameters Inputs -------------
    problem = ParameterClass()
    
    problem.MainDir = os.getcwd()+"/"
    problem.ParameterNames = ['X1','X2','X3']
    problem.Distributions = ['unif','unif','unif']
    
    p1Min, p1Max = -np.pi , np.pi
    p2Min, p2Max = -np.pi , np.pi
    p3Min, p3Max = -np.pi , np.pi

    problem.Parameters = [[p1Min,p1Max],[p2Min,p2Max],[p3Min,p3Max]]

    problem.MCSize = 5000 #1000000, 500
    problem.ProblemDict = {'num_vars': problem.NofPa,'names': problem.ParameterNames,
                           'bounds': problem.Parameters,'dists': problem.Distributions}
    
    #------------- Measurement Inputs -------------        
    Measurement = MeasurementClass()
    Measurement.Observations = np.array([[1.68507124]]) #[2.33903783,  2.89448343, -1.66080837]
    Measurement.MeasurementError = np.array([0.025])
    Measurement.NofMeasurements = 1
    Measurement.Variants = ['f(x)'] 
    
    #------------- Surrogate model Inputs -------------
    SurrogateParams = SurrogateParamsClass()
    SurrogateParams.PceDegree = 7 # Degree of aPCE
    SurrogateParams.MultiplierPCM = 1 # Multiplier for collocation points (K>=1)
    SurrogateParams.NoOfIteration = 1 # Iteration number
    
    # The main function
    RSCoeffs , Psi , Likelihoods = ResponseSurfaceaPC(SurrogateParams, problem, Measurement)
