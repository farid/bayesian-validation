# -*- coding: utf-8 -*-
"""
Created on Tue Dec 27 12:12:08 2016
This program runs TELEMAC 2D & Sisyphe to get the outputs with distinguished name.


@author: Farid Mohammadi
"""

import aPC
import matplotlib
from decimal import Decimal
#matplotlib.use('Agg')
import subprocess
import os
import sys
import shutil
import numpy as np
import matplotlib.pyplot as plt
import fileinput
import timeit
import matplotlib.tri as tri
import time
import re
import numpy.lib.recfunctions as rfn
tic=timeit.default_timer()


float_formatter = lambda x: "%.5f" % x
np.set_printoptions(formatter={'float_kind':float_formatter})


def FilePath(InputFile):
    return os.path.abspath(InputFile)

def MeasurementMatrix(MeasurementFile, TimestepMatrix):
    """Required for reading the measurement files. e.g. '1.22mps_5x5_30_40_exp_processed.xlsx'. """

    data_profile = np.genfromtxt(MeasurementFile,names=None, delimiter=";")
    ProfileREF = np.array(data_profile).T

    st = [round(i,4) for i in set(TimestepMatrix)]
    indices=[i for i, e in enumerate(ProfileREF[0]) if round(e,4) in st]
    MeasurementMatrix=ProfileREF[:,indices]
    
    return MeasurementMatrix

def OutputMatrix(File,nameList,Factors):
    """Required for reading the output files. e.g. 'davarzani2014a_1.22mps_1_staggered-storage.csv'. """
    
#    data_Output = np.genfromtxt(Files[0], names=True, delimiter=";")
#    if len(data_Output) == 0:
#        return data_Output
#    else:
#        TimestepMatrix = data_Output[0]
#        Output = np.zeros((len(nameList),len(TimestepMatrix)))
#        
#        st = [round(i,4) for i in set(TimestepMatrix)]
#        
#        
#        for i, File in enumerate(Files):
#            data_Output = np.genfromtxt(File, names=True, delimiter=";")
#            for idx,name in enumerate(nameList):
#                #Output=np.vstack((Output,data_Output[name]*Factors[idx]))
#                if i != 0 :
#                    indices=[j for j, e in enumerate(data_Output[0]) if round(e,4) in st]
#                
#                Output[idx] = data_Output[name][indices]*Factors[idx]
#        print "Output Model:", Output
#        return Output
    data_Output = np.genfromtxt(File, names=True, delimiter=";")

    Output=np.zeros((len(nameList),len(data_Output)))

    if len(data_Output) !=0:    
        for idx,name in enumerate(nameList):
            Output[idx] = data_Output[name]*Factors[idx]
        return Output
    else:
        return Output

def UpdateParamter(File_path, lookup, nValue ):
    """Replacement of a parameter in the input file. *.input"""
    File_in=open(File_path,'r')
    filedata=File_in.readlines()
    for j in range(len(lookup)):
        for line in filedata:
            if lookup[j] in line:
                oLine=line
                i=filedata.index(oLine)
                # Scientific Notation for "Permeability"
                if not lookup[j] == "Permeability":
                    filedata[i]=lookup[j] +' = %f ' %nValue[j] +'#'+line.split('#')[1] +'\n'
                else:
                    filedata[i]=lookup[j] +' = %.2e ' %Decimal(nValue[j]) +'#'+line.split('#')[1] +'\n'
        File_out=open(File_path,'w')
        File_out.writelines(filedata)
        File_out.close()
        File_in.close()
    return

def UpdateShellScript(File_path, lookup, nValue):
    """Replacement of simulation directory (simdir), input, Output.Name in shell script."""
#    File_in=open(File_path,'r')
#    filedata=File_in.readlines()
#
#    for j in range(len(lookup)):
#        cnt=0
#        for line in filedata:
#            if lookup[j] in line and cnt==0:
#                oLine=line
#                i=filedata.index(oLine)
#                if "-" in lookup[j]: # for pvd filename only for sensor 3 TODO: Add sensor 8 as well.
#                    filedata[i]="      "+lookup[j] +'%s \\\n' %nValue[j]
#                elif not lookup[j] == "Output.Name":
#                    filedata[i]=lookup[j] +'=%s \n' %nValue[j]
#                else:
#                    filedata[i]='  -'+lookup[j] +' \\"'+ nValue[j] + '\\" \\\n'
#                cnt=+1
#        File_out=open(File_path,'w')
#        File_out.writelines(filedata)
#        File_out.close()
#        File_in.close()
    # Read in the file
    with open(File_path, 'r') as file :
      filedata = file.read()
    
    # Replace the target string
    for i in range(len(lookup)):
        filedata = filedata.replace(lookup[i], nValue[i])
    
    # Write the file out again
    with open(File_path, 'w') as file:
      file.write(filedata)
      
    return

def ModelExecutor(ShellFile):
    """ ModelExecutor takes the program and configuration file path
    to run the given model. """


    Process=subprocess.call(['./%s' %ShellFile])

    print '\nMessage 1:'
    print '\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process
    return

#----------------------------------------------
#      Function
#----------------------------------------------


def Function(ShellFile, OutputFile,nameList,Factors,TotalTimeStep):
    """ Function """

    print '\n','-'*70
    print 'Model is running...'
    

    #check if result has finished
    while True:
         time.sleep(5)
         files = os.listdir(".")
         if (OutputFile) in files:
#             SimulationsDataMatrix=OutputMatrix(OutputFile,nameList,Factors)
#             print "Waiting for the simulation to be finished!"#, len(SimulationsDataMatrix[0])
#
#             if len(SimulationsDataMatrix[0]) == TotalTimeStep:
#                 break
             break
         else:
             # Model RUN
             ModelExecutor(ShellFile)
        
    print 'After Simulation'
    
    SimulationsDataMatrix=OutputMatrix(OutputFile,nameList,Factors)
    
    return SimulationsDataMatrix



def ModelRun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterName_,CollocationPoints,Run_No,TotalTimeStep):
    """
    #TelemacRun
    This function gets the following inputs and gives the total evolution at all points.
    MainDir: Main Directory
    ShellFile: Original shell file
    InputFile: Input file e.g. .input
    OutputFile: Output file(s) name(s)
    CollocationPoints: The parameter values to be replaced in InputFile (davarzani2014a_1.22mps.input)
    SF_No: The ICF value of each sediment model in the Telemac

    Run_No: sequence of run (from Response Surface script) --> For storing purpose

    """
    if Run_No ==-2:
        NewInputfile=InputFile.split('.input')[0]+"_calib"
        NewShellfile=ShellFile.split('.sh')[0]+"_calib.sh" 
        NewOutputName=InputFile.split('.input')[0]+"_calib" 
        pvd_Name=NewOutputName+"_staggered-pm"
        
    elif Run_No ==-1:
        NewInputfile=InputFile.split('.input')[0]+"_valid"
        NewShellfile=ShellFile.split('.sh')[0]+"_valid.sh" 
        NewOutputName=InputFile.split('.input')[0]+"_valid"
        pvd_Name=NewOutputName+"_staggered-pm"
    else:
        NewInputfile=InputFile.split('.input')[0]+"_%d" %(Run_No+1)
        NewShellfile=ShellFile.split('.sh')[0]+"_%d.sh" %(Run_No+1)
        NewOutputName=InputFile.split('.input')[0]+"_%d" %(Run_No+1)
        pvd_Name=NewOutputName+"_staggered-pm"
 

    if Run_No == -2:
        newpath = (( ModelName + r'_Run_Calib'))
    elif Run_No == -1:
        newpath = (( ModelName + r'_Run_Valid'))
    else:
        newpath = (( ModelName + r'_Run_%s') %(Run_No+1))
    if not os.path.exists(newpath): os.makedirs(newpath)

    shutil.copy2(MainDir+InputFile, newpath)  # Input file of the model
    shutil.copy2(MainDir+ShellFile, newpath)  # Shell file

    os.chdir(newpath)
    os.rename(InputFile,NewInputfile+'.input')
    os.rename(ShellFile,NewShellfile)
    #----------------------------------------------
    #       Input Parameters
    #----------------------------------------------
   
    #Shell file will be modified here:
    
    NewShellKeywords = [newpath , NewInputfile, NewOutputName, pvd_Name]
    UpdateShellScript(NewShellfile, ShellKeywords , NewShellKeywords)

    # Parametrs will be updated in Input file here:
    UpdateParamter(NewInputfile+'.input',ParameterName_, CollocationPoints)

    #--------- Simulation Starts here --------------------
    Output=Function(NewShellfile, OutputFile,nameList,Factors,TotalTimeStep)

    #----------------------------------------------
    #       Storing Outputs --> to be used for the Response Surface Code
    #----------------------------------------------

    #Evolution=Sim_Evolution(EvolutionFile)
    #TotalOutput=np.vstack((TotalOutput, Output))

    os.chdir("..")


    return Output

#-----------------------------------------------------------------------
#       Storing Observations--> to be used for the Response Surface Code
#----------------------------------------------------------------------



#
#if __name__ == "__main__":
#
#    MainDir= "/temp/farid/LH2/Scripts/NewCodes_04_2018/"
#    InputFile="davarzani2014a_1.22mps.input"
#    ShellFile="experiments_davarzani2014a_constTWalls2d_Farid.sh"
#    MeasurementFile = "Measurement_Calib"
#    OutputFile = "Davarzani2014a_Variant1_Run_1/davarzani2014a_1.22mps_1_staggered-storage.csv"
#    TotalTimeStep=96
#    ShellKeywords_ = ['simdir','input','Output.Name']
#    #OutputFile="davarzani2014a_1.22mps_staggered-storage.csv"
#
#    #CollocationPoints = aPC.CollocationPointsBase
#    NofPa=3 #5
#    Run_No=2
#    TimestepMatrix = np.arange(0,4,0.003472222222222*2)
#    MeasurementMatrix = MeasurementMatrix(MainDir+MeasurementFile,TimestepMatrix )
#    nameList = ['Times','WaterMassLosskgextFac','EvaporationRatemmd']
#
#    OutputMatrix = OutputMatrix(MainDir+OutputFile,nameList,[1.15740740740741e-05, 0.09, 1]) #1/86400 = 1.15740740740741e-05



#    TotalOutput=np.empty((0, TotalTimeStep)) # Time Steps of 3600 sec
#
#    ##### Serial
#    for RunNr in range(NofPa+1):
#        OutputFile="davarzani2014a_1.22mps_%s_staggered-storage.csv" %(RunNr+1)
#    #
#    #        Outputs=ModelRun(MainDir,ShellFile,InputFile,OutputFile, CollocationPoints[RunNr],NofPa ,RunNr,TotalTimeStep)
#    #        TotalOutput=np.vstack((TotalOutput, Outputs))
#
#    #### Multiprocessing
#    import multiprocessing
#    jobs = []
#    out_q = multiprocessing.Queue()
#
#    def Modelrun(MainDir,ShellFile,InputFile,OutputFile,O_int,NofPa ,m,TotalTimeStep,TotalOutput, out_q): #Telemacrun
#        Output=ModelRun(MainDir,ShellFile,InputFile,OutputFile,O_int[m],NofPa ,m,TotalTimeStep,TotalOutput)   #TRFormula[index] = defines each model alternative
#        out_q.put(Output)
#        print "out_q:", out_q
#        return out_q
#
#    for m in range(NofPa+1):
#        OutputFile="davarzani2014a_1.22mps_%s_staggered-storage.csv" %(m+1)
#        print "OutputFile Name:",OutputFile
#        #Evolution=Telemac_Run_Rhine_loop.TelemacRun(O_int[m],NofPa,TRFormula[index] ,m)
#        #TotalEvolutions=np.vstack((TotalEvolutions, Evolution))
#        p = multiprocessing.Process(target=Modelrun, args=(MainDir,ShellFile,InputFile,OutputFile,CollocationPoints,NofPa ,m,TotalTimeStep,TotalOutput, out_q,))
#        jobs.append(p)
#        #Evolution_[m]=np.append(Evolution_[m], p)
#        p.start()
#
#    #------- Try to prepare one matrix with the total model results -------------
#    print "TotalOutput is being prepared!!!",out_q.get()
#    for NofE in range(NofPa+1):
#        TotalOutput=np.vstack((TotalOutput, out_q.get())) # It extracts only the EvaporationRate from the output csv file
#
#    #result = []
#    for job in jobs:
#        job.join() #wait for this process to complete
#
#    print "TimeStep, WaterMassLoss, EvaporationRate:", TotalOutput
#    #print "ElapsedTime:",ElapsedTime
#    #print '\n','-'*70,'\n',"My task has been finished.\n",'-'*70,'\n'
