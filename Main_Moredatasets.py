# -*- coding: utf-8 -*-
"""
Validation Benchmark

Description: TODO

+++++++++++++++++++ Response Surface +++++++++++++++++++
This program reads the input points of parameter p_1 to p_8 form text file 'input1.txt'and generates uniform random values between min and max.
It tries to calculate the coefficent of the response surface:
              Rs=a_o + a_1 x p_1 + a_2 x p_2+ a_3 x p_3+ a_4 x p_4+ a_5 x p_5
The original function is:
              f(p_1 ... p_5) comes from Model Outputs
N= number of iterations to improve the Response surfaces
NofMeasurements=111 (The Rhine Model)
ModelRuns=Nofpa+N


+++++++++++++++++++ BME Calculation +++++++++++++++++++


Measurement Error shall be set with caution.
@author: Farid Mohammadi, M.Sc.
Created on Wed Apr 04 2018
"""
#import Uniform_dis_pars
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.size'] = 14
#matplotlib.rcParams['font.family'] = 'serif' #'Calibri'
import aPC
#import Optimal_integration_points_5pa
import numpy as np
from scipy.linalg import lu
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import Model_Run_exp
import os
import multiprocessing
#import shutil
from scipy import stats
from scipy.stats import norm
import math
#from sklearn.preprocessing import normalize
import timeit
import time
from datetime import timedelta
tic=timeit.default_timer()
#------------------------------------------------------------------------------
def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays to get all combination.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out
#------------------------------------------------------------------------------
def FilePath(InputFile):
    return os.path.abspath(InputFile)
#------------------------------------------------------------------------------
def UpdateResultsName(File_path, lookup, nValue ):
    """Replacement of tel.res and sis.res names"""
    File_in=open(File_path,'r')
    filedata=File_in.readlines()
    for j in range(len(lookup)):
        for line in filedata:
                if lookup[j] in line:
                    oLine=line
                    i=filedata.index(oLine)
                    filedata[i]=lookup[j] +'\t = %s \n' %nValue[j]
        File_out=open(File_path,'w')
        File_out.writelines(filedata)
        File_out.close()
        File_in.close()
    return
#------------------------------------------------------------------------------
def NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos, NofMeasurement,NofItr):
#    if NofItr == 1:
#        NewPolynomialDegrees = np.zeros((NofVar,NofMeasurement,len(PolynomialDegrees)+NofItr,NofPa)) #[[] for i in range(NofMeasurement)]
    NewPolynomialDegrees = [[[] for i in range(NofMeasurement)] for j in range(NofVar)]

    for VarIdx in range(NofVar):
        for itr in range(NofItr):
            for idx in range(NofMeasurement):
                if itr ==0:
                    NewPolynomialDegrees[VarIdx][idx]=np.vstack((PolynomialDegrees,NewCombos[itr][VarIdx,idx])) #[itr][idx]
                else:
                    NewPolynomialDegrees[VarIdx][idx]=np.vstack((NewPolynomialDegrees[VarIdx][idx],NewCombos[itr][VarIdx,idx]))
    print "NewPolynomialDegrees:\n", NewPolynomialDegrees
    return NewPolynomialDegrees
#------------------------------------------------------------------------------
def solving_determined_system(array1, array2):
    """
    This fuction solves the determined system of equations.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    Z=np.linalg.lstsq(a, b, rcond=-1)
    R=Z[0]

    return R

#------------------------------------------------------------------------------
def solving_overdetermined_system(array1, array2):
    """
    This fuction solves the overdetermined system of equations using Least Square Method.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    R=np.linalg.lstsq(a,b)[0]

    return R

#------------------------------------------------------------------------------
def RS_calculation_aPC(MainDir,NofVar,NofPa, d,ItrNr, Model_Output, OptimalCollocationPointsBase,modelError, PolynomialDegrees, PolynomialBasisFileName, NofMeasurements):
    """
    calculates the response surface for each measurement location
    So each row of the RSTotal matrix is the coefficients of
    one response surface for a specific measurement location

    NofMeasurements: Number of measurement locations
    NofPa: Number of sensitive parameters
    k= will be the output of the TELEMAC runs for each location

    attributes: array1, system_type, NofMeasurements
    """
    P = math.factorial(NofPa+d)/(math.factorial(NofPa) * math.factorial(d)) #Total number of terms
    RSTotal=np.zeros((NofVar, P+ItrNr , NofMeasurements))
    Psi_RS = np.zeros((NofVar, P+ItrNr , P+ItrNr))
    #NewCombos = np.zeros((NofVar, NofMeasurements, NofPa),dtype=int) #[[]for i in range(NofMeasurements)]
    
#    #----------------------------------------------------
#    # Calculate RSCoeffs & NewCombos for datasets(variants)
#    #----------------------------------------------------
#    for VarIdx in range(NofVar):
#        
#        # Calculate Coeffs of RS for each measurement points
#        for i in range(NofMeasurements): 
#            
#            # Handle the first RS calculation
#            if ItrNr !=0: 
#                NewPolynomialDegrees=PolynomialDegrees[VarIdx] #[i] This will be used when each measurement point has its own Polynomial Degrees
##            elif ItrNr ==1:
##                Coeffs_Old = Coeffs_old[VarIdx,:,i]
##                NewPolynomialDegrees=PolynomialDegrees
#            else:
#            
#                NewPolynomialDegrees=PolynomialDegrees
#                
#            #print "Coeffs_Old:", Coeffs_Old
#            #print "NewPolynomialDegrees", NewPolynomialDegrees
#            #Coeffs, Psi_RS[VarIdx], NewTermCombo= aPC.RS_Builder(MainDir,NofPa, d,ItrNr, Model_Output[VarIdx,:,i], OptimalCollocationPointsBase,Coeffs_Old, NewPolynomialDegrees, PolynomialBasisFileName)
#            print "Model_Output:\n", Model_Output[VarIdx,:,i]
#            Coeffs = aPC.RS_Builder(MainDir,NofPa, d,ItrNr, Model_Output[VarIdx,:,i], OptimalCollocationPointsBase, NewPolynomialDegrees, PolynomialBasisFileName)
#            # Save the NewTermCombo for each Measurement point and Iterations
#            #NewCombos[i]= NewTermCombo
#            #if ItrNr !=0: 
#                #NewCombos[VarIdx,i,:] = NewTermCombo
#            # Save coefficients in the RSTotal array
#            RSTotal[VarIdx,:,i] = Coeffs
    #----------------------------------------------------
    # Calculate RSCoeffs & NewCombos for datasets(variants)
    #----------------------------------------------------
    for VarIdx in range(NofVar):
        # Handle the first RS calculation
        if ItrNr !=0: 
            NewPolynomialDegrees=PolynomialDegrees[VarIdx] #[i] This will be used when each measurement point has its own Polynomial Degrees
        else:        
            NewPolynomialDegrees=PolynomialDegrees
        #print "Model Output to calculate Coeffs:\n", Model_Output[VarIdx]
        RSTotal[VarIdx,:,:] = aPC.RS_Builder(MainDir,NofPa, d,ItrNr, Model_Output[VarIdx], OptimalCollocationPointsBase,modelError, NewPolynomialDegrees, PolynomialBasisFileName)
        
    return RSTotal#, NewCombos


#------------------------------------------------------------------------------
def weights(N,d,NofVar,ItrNr,Psi_W_old,Parameterset, RSTotal, Observations, MeasurementError, NofMeasurements, MCsize,PolynomialBasisFileName,PolynomialDegrees):
    """
    This function calculates the weights of the each parameter set based on the difference of
    its output of RS and the corresponding observation value.
    
    
    RSTotal = Coeffs
    NofMeasurements: Number of the measurements
    MCsize: Number of the parameter sets
    """
    P = math.factorial(NofPa+d)/(math.factorial(NofPa) * math.factorial(d)) #Total number of terms
    ParametersetSize = Parameterset.shape[0]
#    Parameterset = Parameterset[:,:-1] # to split the modelErrorDistribution
#    modelErrorDistribution = Parameterset[:,-1]
    Weights=np.zeros((ParametersetSize, NofVar)) 
    ObservationSize = Observations.shape[1]
    OutputRS=np.empty((0, NofMeasurements))
    Psi_W = np.zeros((NofVar, ParametersetSize , P+ItrNr))
    
    #Loop over the variants (Data Sets)
    for VarIdx in range(NofVar):
        # Create Psi Matrix
        #start = time.time()
        if ItrNr == 0:
            Psi_W[VarIdx], NewCombos=aPC.Psi_Creater(MainDir,N,d,ItrNr,Parameterset, PolynomialBasisFileName,PolynomialDegrees)
        else:
            if Psi_W_old is None:
                Psi_W_Old =None
            else:
                Psi_W_Old=Psi_W_old[VarIdx]
            Psi_W[VarIdx]=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr,Psi_W_Old,Parameterset, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        #end = time.time()
        #print ("Elapsed time after Psi:", end - start)
        
        #Calculation of the outputs of Response surfaces and their mean and Stdev
        #print "Psi_W shape:", Psi_W[VarIdx].shape
        #print "RS Coeffs:",RSTotal[VarIdx].shape
        OutputRS = np.dot(Psi_W[VarIdx], RSTotal[VarIdx])
        
        # Calculate mean and standard deviation
        #mean_OutputRS=np.mean(OutputRS, axis=0)
        #stdev_OutputRS=np.std(OutputRS, axis=0)
        
        # ========================================================================
        Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
        np.fill_diagonal(Measurement_Error, MeasurementError[VarIdx]**2)
        
        #------------------------------------------------------------------------
        if ObservationSize != ParametersetSize:
            Deviation=np.zeros((ParametersetSize,NofMeasurements)) 
    
            for i in range(ParametersetSize):
                Deviation[i] = Observations[VarIdx+1] - OutputRS[i, :]
        
        else: # for calculation of Weight_RM
            Deviation = Observations[VarIdx+1] - OutputRS
            
        #print "Observation:",Observations
        #print "OutputRS:",OutputRS[0, :]
        #print "Deviation:",Deviation[0]
        
        # Computation of Weight according to the Deviation
        
        for i in range(ParametersetSize):
            Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Measurement_Error)), Deviation[i,:][:,None])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(Measurement_Error)))
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    #print "Weights:\n",Weights
    Weights = np.prod( Weights,axis=1)
    #print "Weights:\n",Weights
    return Weights[:,None], Psi_W#, mean_OutputRS, stdev_OutputRS

#------------------------------------------------------------------------------
def BME_Corr_Weight(N,d,NofVar,ItrNr,Parametersets, RSTotal, OutputOrig, MeasurementError, NofMeasurements, MCsize,PolynomialBasisFileName,PolynomialDegrees):
    """
    Calculates the correction factor for BMEs.
    """
    ParametersetSize = Parametersets.shape[0]
    OutputRS=np.empty((0, NofMeasurements))
    Weights=np.zeros((ParametersetSize, NofVar)) 
    #z=np.ones((ParametersetSize, 1))
    #a=np.hstack((z, Parameterset))
    for VarIdx in range(NofVar):
        # Create Psi Matrix
        start = time.time()
        #Psi,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr,Parametersets,RSTotal, PolynomialBasisFileName,PolynomialDegrees)
        Psi=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr,None,Parametersets, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        end = time.time()
        print ("Elapsed time after Psi for BME_Corr_Weight:", end - start)
        
        #Calculation of the outputs of Response surfaces and their mean and Stdev
        OutputRS = np.dot(Psi, RSTotal[VarIdx])
        #mean_OutputRS=np.mean(OutputRS, axis=0)
        #stdev_OutputRS=np.std(OutputRS, axis=0)
        
        # ========================================================================
        Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
        np.fill_diagonal(Measurement_Error, MeasurementError[VarIdx]**2)
        
        #------------------------------------------------------------------------
        Deviation=np.zeros((ParametersetSize,NofMeasurements)) 
        for i in range(ParametersetSize):
            Deviation[i] = OutputOrig[VarIdx,i,:] - OutputRS[i, :]
            
        #print "OutputOrig:",OutputOrig
        #print "OutputRS:",OutputRS[0, :]
        #print "Deviation:",Deviation[0]
        # Computation of Weight according to the Deviation
        
        for i in range(ParametersetSize):
            Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Measurement_Error)), Deviation[i,:])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(Measurement_Error)))
        
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    Weights = np.prod( Weights,axis=1)
    return Weights#, mean_OutputRS, stdev_OutputRS



#------------------------------------------------------------------------------
def filtration_prior(Parameterset, Weights ,MCsize, NofPa):
    """Rejection Sampling: filtration of prior distribution via uniform"""
    ii=0
    unif=np.random.rand(1, MCsize)
    Parameterset_weight=np.hstack((Parameterset, Weights))
    Post_MC_Vector=np.empty((0, NofPa+1))
    for i in range(len(Weights)):
        if Weights[i, :]/np.max(Weights) > unif[:, i]:
            ii += 1
            Post_Vector = Parameterset_weight[i, :]
            Post_MC_Vector=np.vstack((Post_MC_Vector, Post_Vector))
    
    return Post_MC_Vector[:, 0:NofPa], Post_MC_Vector[:, NofPa][:,None]

#------------------------------------------------------------------------------
def Mean_Stdev(MainDir,N,d,NofVar,ItrNr,Psi_W,Parametersets, RSTotal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements):
    """
    Calculates the mean and standard deviation of the SQRs using the reduced model
    NofMeasurements: Number of the measurements 
    """    
    #Psi,NewTermCombo  = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parametersets,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    mean_OutputRS = [[] for i in range(NofVar)]
    stdev_OutputRS = [[] for i in range(NofVar)]
    for VarIdx in range(NofVar):
        Psi=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr,Psi_W,Parametersets, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        
        # Calculate the respose surface's output
        OutputRS = np.dot(Psi, RSTotal[VarIdx])
    
        #------- Calculation of mean and Stdev of the outputs of Response surfaces----------------
        mean_OutputRS[VarIdx]=np.mean(OutputRS, axis=0)
        stdev_OutputRS[VarIdx]=np.std(OutputRS, axis=0)

    return mean_OutputRS, stdev_OutputRS

#------------------------------------------------------------------------------
def plot_weights(Parameterset, Weights,ParameterNames,Parameters, Name,RSTotal,Nr_Iteration,NofVar):
    """
    This Function plots each parameter versus its weight

    """
    #print "plot_weights, Parameterset:\n", Parameterset.shape
    #print "plot_weights, Weights:\n", Weights.shape
    fig = plt.figure()

    fig.set_figheight(12)
    fig.set_figwidth(20)

    Colors = ['b.','g.','r.','c.','m.', 'y.', 'k.']
    header1 = []
    header2 = []
    #header2 = ["c_0"]
    for coeffs in range(len(RSTotal[0,:,0])):
        header2.append('c_%s'%(coeffs)) #+1
        
    # Decide on shape of the figure
    if len(ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
    
    
    # Loop over the paramters and plot their weights
    for i,ParameterName in enumerate(ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-6) #(1, 2*i-5)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        ax1.plot(Parameterset[:, i], Weights, Colors[i])
        plt.xlim(Parameters[i][0], Parameters[i][1])
        header1.append('P_%s'%(i+1))
        #header2.append('c_%s'%(i+1))
        plt.xlabel('p$_%s$'%(i+1))
        plt.ylabel('Weights')
        plt.title(ParameterName)

    plt.tight_layout()
    
    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Outputs') 
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(Name +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save parameter sets with their weights
    np.savetxt(Name+'.txt', np.hstack((Parameterset, Weights)), delimiter=',', header= " ".join(header1)+ "   Weight")
    # Save Response surfaces in a text file
    for VarIdx in range(NofVar):
        if Nr_Iteration != None:
            np.savetxt('RSTotal_Itr%s_Var%s.txt'%(Nr_Iteration,VarIdx+1), RSTotal[VarIdx].T, delimiter=',',  header=" ".join(header2))
    
    os.chdir('..')
    
#---------------------------------------------------------------------------
def plot_Histogram(Post_MC_Vector,Post_MC_Weights,ParameterNames, Parameters, Name):
    """
    This Function plots the histogram of posterior parameter sets.
    """
    fig = plt.figure()

    fig.set_figheight(12) #20
    fig.set_figwidth(20)
  
    header1 = []
    Colors = ['b','g','r','c','m']

    # Decide on shape of the figure
    if len(ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
        
    # Loop over the paramters and plot their histograms        
    for i,ParameterName in enumerate(ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-5)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        x1=Post_MC_Vector[:, i]
        n, bins, patches = ax1.hist(x1, 5, normed=1, facecolor=Colors[i], alpha=0.75)
        
        # ------ Fitting data -----------
        xs1 = np.linspace(Parameters[i][0], Parameters[i][1], 200)
        kde1 = stats.gaussian_kde(x1)
        ax1.plot(xs1, kde1(xs1), Colors[i]+'--', lw=2)
        
        ax1.set_xlabel('Values of p$_%s$'%(i+1))
        ax1.set_ylabel('Density')
        ax1.grid(True)
        anchored_text = AnchoredText("$\mu$=%.4g \n $\sigma$=%.4g \n $S$=%.4f \n $k$=%.4f" %(np.mean(x1), np.std(x1), stats.skew(x1), stats.kurtosis(x1)), loc=2)
        ax1.add_artist(anchored_text)
        header1.append('P_%s'%(i+1))

    #plt.suptitle('Probablity Density Function')

    plt.tight_layout()

    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Outputs')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(Name +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save POSTERIOR parameter sets with their weights
    np.savetxt(Name+'.txt', np.hstack((Post_MC_Vector, Post_MC_Weights)), delimiter=',',header= " ".join(header1)+ "   Weight")

    os.chdir('..')

#-------------------------------------------------------------------------------------------------------
def Variant_plot(MainDir,ModelName,N,d,Variants,ItrNr,Observations, Output, Parameterset, RSTotal,PolynomialBasisFileName,PolynomialDegrees,BestIdxs,NrofSparse,NofMeasurements):
    """ 
    This function plots the SQR's output from the full complexity model and its reduced version (aPCE).
    """
    # Calculate the SQR's output from the reduced version (aPCE)
    #Psi_Calib,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parameterset_Calib,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    P = math.factorial(N+d)/(math.factorial(N) * math.factorial(d))
    #Nr_of_Sparse_Indexes=len(PolynomialDegrees[0])
    RSOutputTotal = np.zeros((len(Variants),NofMeasurements))
    
    X_value = Observations[0]
    
            
    # Loop over all outputs (Variants)
    for VarIdx in range(len(Variants)):
        
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
        
        # Plot the measurement; once for each variant
        ax.plot(X_value, Observations[VarIdx+1], "x", color='k' ,label='Measurements' , lw=2)
        # Plot the output from the original model; once for each variant
        #print "Original model Output %s:"%ItrNr, Output[VarIdx][:NofMeasurements]
        ax.plot(X_value, Output[VarIdx][:NofMeasurements], "o-", color="brown" ,label='Original model' , lw=2)
        
        
        # Plot the RSOutputs; once for each variant
        for sparse_index in range(NrofSparse):            
            if ItrNr ==1: # using initial PolynomialDegrees
                Psi, NewCombos=aPC.Psi_Creater(MainDir,N,d,ItrNr-1,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees)
            elif ItrNr !=1 and NrofSparse==1:
            #if NrofSparse==1:
                Psi=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr-1,None,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
            else:
                Psi=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr-1,None,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees[VarIdx,sparse_index],NofMeasurements)
            
            #print "Psi_Variant_Plot:\n", Psi
            #print "Coeff_Variant_Plot:\n", RSTotal[VarIdx].shape
            #print "RS:\n", np.dot(Psi, RSTotal[VarIdx])
            if ItrNr == 1 or NrofSparse == 1:
                RSOutput = np.dot(Psi, RSTotal[VarIdx])[0]
                RSOutputTotal[VarIdx] = RSOutput
            else:
                RSOutput = np.dot(Psi, RSTotal[VarIdx,sparse_index])[0]
            #print "RSOutput:\n", RSOutput 
            # Plot the output from the surrogate model for all options of sparse polynimial degrees
            if ItrNr!=1 and NrofSparse !=1 and sparse_index == BestIdxs[VarIdx]:
                Color, LW ="g", 3
            else:
                 Color, LW ="navy", 1
            
            ax.plot(X_value, RSOutput, ".-",color=Color , label='Response surface '+str(sparse_index+1), lw=LW)
        
        
        # Axis labels
        plt.xlabel('Time [d]',fontsize=18)
        plt.ylabel(Variants[VarIdx], fontsize=18)
        ax.legend(loc='best', fontsize=18)    
        
        ax.tick_params(axis = 'both', which = 'major', labelsize = 18)
        
        # Save the figure and the BMEs into a folder
        #newpath = (( ModelName + r'_Variant%s_Run_Calib') %(VarID+1))
        # save the figure to file
        newpath = (( ModelName + r'_Run_%s') %(P+ItrNr))
        if not os.path.exists(newpath): os.makedirs(newpath)
        os.chdir(newpath)
        
        fig.savefig(Variants[VarIdx]+'.png')   
        plt.close(fig)
        
        
        os.chdir("..")
    
    if ItrNr == 1 or NrofSparse == 1:
        # Loop over all outputs (Variants)
        for VarIdx in range(len(Variants)):
            fig = plt.figure()
            fig.set_figheight(12) #20
            fig.set_figwidth(20)
            ax = plt.subplot(1,1,1)
            
            # Plot the output from the original model; once for each variant
            predictions = RSOutputTotal[VarIdx]
            NewOutput = Output[VarIdx][:NofMeasurements]
            
            # Create linear regression object
            from sklearn import metrics, linear_model
            regr = linear_model.LinearRegression()
            regr.fit(predictions.reshape(-1, 1) , NewOutput)
            
            # predict y from the data
            x_new = np.linspace(np.min(predictions), np.max(predictions), 100)
            y_new = regr.predict(x_new[:, np.newaxis])
            
            ax.scatter(predictions , NewOutput, edgecolors=(0, 0, 0))
            ax.plot(x_new, y_new, color = 'k')
            
            # Axis labels
            plt.xlabel(Variants[VarIdx] +' by surrogate model',fontsize=18)
            plt.ylabel(Variants[VarIdx] +' by original model', fontsize=18)
            
            
            R2 = metrics.r2_score(NewOutput, predictions)
            RMSE = metrics.mean_squared_error(NewOutput , predictions)

            plt.annotate('RMSE = '+ str(RMSE) + '\n' + '$R^2$ = '+ str(R2), xy=(0.05, 0.85), xycoords='axes fraction')
                     
            #ax.legend(loc='best', fontsize=18)
            # save the figure to file
            newpath = (( ModelName + r'_Run_%s') %(P+ItrNr))
            if not os.path.exists(newpath): os.makedirs(newpath)
            os.chdir(newpath)
            
            fig.savefig(Variants[VarIdx]+'Prediction'+'.png')   
            plt.close(fig)
            
            os.chdir("..")
#-------------------------------------------------------------------------------------------------------


def Calib_Valid_plot(MainDir,N,d,Variants,ItrNr,Observations_calib, Output_calib, Parameterset_Calib, RSTotal, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements,PlotName):
    """ 
    This function plots the SQR's output from the full complexity model and its reduced version (aPCE).
    """
    # Calculate the SQR's output from the reduced version (aPCE)
    #Psi_Calib,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parameterset_Calib,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    # Loop over all outputs (Variants)
    for VarIdx in range(len(Variants)):
        
        Psi_Calib=aPC.Psi_Creator_Weight(MainDir,N,d,ItrNr,None,Parameterset_Calib, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        #print "Psi_Calib:\n", Psi_Calib
        RSOutput_calib = np.dot(Psi_Calib, RSTotal[VarIdx])[0]
        #print "RSOutput_calib:\n", RSOutput_calib 
        X_value = Observations_calib[0] #range(len(RSOutput_calib))
        #print "Observations_calib:\n", Observations_calib
        
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
    
        # Plot the measurement
        ax.plot(X_value, Observations_calib[VarIdx+1], "x", color='k' ,label='Measurements' , lw=2)
        # Plot the output from the original model
        ax.plot(X_value, Output_calib[VarIdx+1][:NofMeasurements], "o-", color="brown" ,label='Original model' , lw=2)
        # Plot the output from the surrogate model
        ax.plot(X_value, RSOutput_calib, ".-",color="navy" , label='Response surface', lw=2)
        # Plot the mean of the surrogate model
        ax.plot(X_value, mean_RS_Cal[VarIdx], "1-",color="green" , label='RS Mean', lw=2)
        
        # Plot confidence interval of reduced model
        Ub=mean_RS_Cal[VarIdx]+2*stdev_RS_Cal[VarIdx]
        Lb=mean_RS_Cal[VarIdx]-2*stdev_RS_Cal[VarIdx]
        ax.fill_between(X_value, Ub, Lb,color="gray", alpha=.5)
        
        # Patch the confidence interval label
        extraString = "$\mu \pm 2\sigma$"
        handles, labels = ax.get_legend_handles_labels()
        handles.append(mpatches.Patch(color='gray', label=extraString))
        ax.legend(handles=handles, loc='best')
        
        
        # Axis labels
        plt.xlabel('Time [d]')
        plt.ylabel(Variants[VarIdx])
        
        
        # Create a directory to save the figure to file
        newpath = (ModelName + '_' + PlotName + '_Outputs')
        if not os.path.exists(newpath): os.makedirs(newpath)
        os.chdir(newpath)
        
        # save the figure to file        
        fig.savefig(Variants[VarIdx]+ '_' + PlotName +'.png')   
        plt.close(fig)
        
        # -------------- Evaluation of surrogate model predictions ------------
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
        
        predictions = RSOutput_calib
        NewOutput = Output_calib[VarIdx+1][:NofMeasurements]
        
        # Create linear regression object
        from sklearn import metrics, linear_model
        regr = linear_model.LinearRegression()
        regr.fit(predictions.reshape(-1, 1) , NewOutput)
        
        # predict y from the data
        x_new = np.linspace(np.min(predictions), np.max(predictions), 100)
        y_new = regr.predict(x_new[:, np.newaxis])
        
        ax.scatter(predictions , NewOutput, edgecolors=(0, 0, 0))
        ax.plot(x_new, y_new, color = 'k')
        
        # Axis labels
        plt.xlabel(Variants[VarIdx] +' by surrogate model',fontsize=18)
        plt.ylabel(Variants[VarIdx] +' by original model', fontsize=18)
        
        
        R2 = metrics.r2_score(NewOutput, predictions)
        RMSE = metrics.mean_squared_error(NewOutput , predictions)

        plt.annotate('RMSE = '+ str(RMSE) + '\n' + '$R^2$ = '+ str(R2), xy=(0.05, 0.85), xycoords='axes fraction')
        
        fig.savefig(Variants[VarIdx]+'Prediction'+ '_' + PlotName +'.png')   
        
        plt.close(fig)
        
        os.chdir("..")
        
#-------------------------------------------------------------------------------------------------------
def Plot_BME(BayesianEvidenceMatrix,logBayesian_EvidenceMatrix, Nr_Iteration,Variants):
    fig = plt.figure()
    N_Var=len(logBayesian_EvidenceMatrix)
    Styles= ['b-','g-','r-','c-','m-','y-']
    Markers = ['o','*','+','s','D','p']
    
    # Plot logBayesian_EvidenceMatrix for all variants
    for NofV in range(N_Var):
        plt.plot(Nr_Iteration, logBayesian_EvidenceMatrix[NofV], Styles[NofV], marker=Markers[NofV], label=Variants[NofV], lw=2)
    
    plt.xlim(np.min(Nr_Iteration), np.max(Nr_Iteration))
    plt.xlabel('Number of Iterations')
    plt.ylabel('log$_{10}$(BME)')
    plt.ylim(np.min(logBayesian_EvidenceMatrix)-1, np.max(logBayesian_EvidenceMatrix)+1)
    plt.title('Bayesian Model Evidence vs. Iterations')
    plt.legend(loc='best')
    plt.grid()
    
    # Save the figure and the BMEs into a folder
    newpath = ((r'Output_BME'))
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    # save the figure to file
    fig.savefig('Log_BME_vs_Itr.png')   
    plt.close(fig)

    # save BayesianEvidenceMatrix & logBayesian_EvidenceMatrix in a text file
    np.savetxt('BME.txt', BayesianEvidenceMatrix, delimiter=',')
    np.savetxt('Log_BME.txt', logBayesian_EvidenceMatrix, delimiter=',')

    os.chdir('..')

#-------------------------------------------------------------------------------------
#def resampling(x, P_range, MCsize):
def resampling(x, MCsize):
    size=MCsize / (4*len(x))
    resampled=np.empty((size,0))
    for i in range(len(x[1,:])):
        kde = stats.gaussian_kde(x[:, i])
        sample = kde.resample(size)
        #filtered_sample = sample[(sample >= P_range[i, 0]) & (sample <= P_range[i, 1])]
        filtered_sample = sample[(sample >= np.min(x[:, i])) & (sample <= np.max(x[:, i]))]
       # print filtered_sample.size
        while True:
             len(filtered_sample) < size
             kde_new = stats.gaussian_kde(filtered_sample)
             size_new=size-len(filtered_sample)
             sample_new=kde_new.resample(size_new)
             filtered_sample=np.append(filtered_sample,sample_new)
          #   filtered_sample = sample_new[(sample_new >= P_range[i, 0]) & (sample_new <= P_range[i, 1])]
             if len(filtered_sample) == size:
                 break

        resampled=np.hstack((resampled,filtered_sample[:, None]))
        #resampled=np.hstack((resampled,sample.T))
    return resampled

#-------------------------------------------------------------------------------------
def selection_max_weight(ShellKeywords,Parameterset, CollocationPoints, Weight, NewOutput, NofMeasurements,nameList,Factors, NoItr, NofVar, m):
        """ This function does the following operations:
        1) Computes the weights of the optimal integration points in each step
        2) Sorts the parametersets based on the weights calculated in Step1
        3) Computes the weights of the parameter sets
        4) Selects the parameterset with the best weight
        5) Checks if already exist in the optimal integration points (CollocationPoints)
        6) Run the model for the new collocation point

        Output:
        Updated integration points for solving an overdetermined system of equations
        """

        print 80 * '-'
        print('---> Selection of the Parameter Set with Maximum Weight \n')
        
        
        Parametersets_weight=np.hstack((Parameterset, Weight))
        sorted_Parametersets_weight=Parametersets_weight[np.argsort(Parametersets_weight[:, NofPa])][::-1]
        
        # Check if the parameter set already exists in the collocation points.
        i = 0
        while i < len(Parameterset):
            NewCollocationPoint=sorted_Parametersets_weight[i, 0:NofPa]
            NewModelError = sorted_Parametersets_weight[i, NofPa]
            print "NewModelError:\n" ,NewModelError 
            # If doesn't exist, break; else take the next one. 
            if any(item == False for item in np.isin(NewCollocationPoint , CollocationPoints)):
                break
            else:
                print '-' * 50
                print 'The previous collocation point is selected again!!'
                print 'Do not panic! We can try to select the next parameter set as new collocation point'
                print '-' * 50
                NewCollocationPoint=sorted_Parametersets_weight[i, 0:NofPa]
                i += 1
            

        print '-' * 50
        print "NewCollocationPoint:", NewCollocationPoint
        print '-' * 50
        
        O_new=np.vstack((CollocationPoints, NewCollocationPoint))
        
        Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[m],nameList,Factors,ParameterNames,NewCollocationPoint,m,NofMeasurements)
        NewTotalOutput=np.zeros((NofVar,len(O_new),NofMeasurements))
        for VarIdx in range(NofVar):

            #Output = Output[[VarIdx+1]] # Takes the second column from the output matrix
            
            #if NoItr == 1:
            #    NewTotalOutput[VarIdx,:,:]=np.vstack((TotalOutput[VarIdx] , Output[VarIdx+1]))
            #else:
                NewTotalOutput[VarIdx,:,:]=np.vstack((NewOutput[VarIdx] , Output[VarIdx+1]))
        
        
        return O_new, NewTotalOutput,NewModelError


#-------------------------------------------------------------------------------------
def Modelrun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int ,m,TotalTimeStep, out_q):
    Output = Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int[m],m,TotalTimeStep)   #Variants[index] = defines each model alternative
    out_q.put(Output)
    return out_q

#-------------------------------------------------------------------------------------
def RS_ST(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,ShellFile_Valid,InputFile_Valid,MeasurementFile_Calib,MeasurementFile_Valid,MeasurementError,ModelError,NofVar,NofPa, d, MCsize, problem, ParameterNames,Parameters, Percentage,OutputFile,nameList,Factors,CalibTimeStep,ValidTimeStep): #out_q1,index,NofPa
    """
    This function is the main function. 
    It runs the model with the help of the Model_Run_exp script and creates the response surfaces and does the Bayesian updating. 
    """
    TotalTimeStep = CalibTimeStep + ValidTimeStep
    
#    CovarianceMatrix = [sum(x) for x in zip(ModelError,MeasurementError)]
    P=math.factorial(NofPa+d)/(math.factorial(NofPa) * math.factorial(d)) #Total number of terms
    # Here, the distributions of the input parameters are imported from the :
    inputDistributions,OptimalCollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName = aPC.CollocationPoints(MainDir,NofPa+1, d,0, MCsize,Parameters, Percentage)
    # Pick the collocatation points for Nofpa-1 and the corresponding PolynomialDegrees
    modelError = OptimalCollocationPointsBase[1:,-1]
    OptimalCollocationPointsBase = OptimalCollocationPointsBase[1:,:NofPa]
    PolynomialDegrees = PolynomialDegrees[1:,:NofPa]
    PolynomialBasisFileName.remove('PolynomialBasis_d1_'+str(NofPa+1)+'.txt')
    PolynomialBasisFileName.remove('PolynomialBasis_d2_'+str(NofPa+1)+'.txt')
    
#    print "PolynomialDegrees:\n", PolynomialDegrees
#    print "PolynomialBasisFileName:\n",PolynomialBasisFileName
    
    #Parametersets_dp1,OptimalCollocationPointsBase_dp1,PolynomialDegree_dp1,PolynomialBasisFileName_dp1 = aPC.CollocationPoints(MainDir,NofPa, d+1,0, MCsize,Parameters, Percentage)
    # Here, the combinations of points are imported to from Optimal_integration_points:
    # Collocation Points
    
    O_int = OptimalCollocationPointsBase
    
    print ('The %s optimal collocation points are:\n' %(P)), O_int
    print '-' * 50
    
    #------------------------------------------------------------------------------
    # Here the input of the formulas are given:
    BayesianEvidenceMatrix=np.empty((0, NofItr))  # This should be equal to the iteration number

    # Initialization of arrays to store  simulation outputs
#    TotalOutput_=[[] for i in range(NofVar)]
#    for Var_Nr in range(NofVar):
#        TotalOutput_[Var_Nr]=np.empty((0, TotalTimeStep))
    TotalOutput_=np.zeros((NofVar,P,TotalTimeStep))
    ################ Serial Simulation   #####################
    # Run simulations at the collocation points
#    for m in range(NofPa+1):
#        Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,InputFile,OutputFile[m],O_int[m],NofPa ,m,TotalTimeStep)
#        # save each output in their corresponding array
#        for Var_Nr in range(NofVar):
#            TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
    
    ################ Parallel Simulation   #####################
    jobs = []
    items =[]
    out_q = multiprocessing.Queue()
    
    # Simulation runs for the optimal integration points     
    for m in range(P):
        p = multiprocessing.Process(target=Modelrun, args=(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[m],nameList,Factors,ParameterNames,O_int,m,TotalTimeStep, out_q,)) #index here is 0: Variant 1
        jobs.append(p)
        
    for m,job in enumerate(jobs):
        job.start()
        time.sleep(0.1)
    #------- Try to prepare one matrix with the total model results -------------
#        while not out_q.empty():
#            print "I am still working, Farid!!!"
#            items.append(out_q.get())
             #----------------------
#        try:
#            print "Try & Except!!!"
#            Output = out_q.get(timeout=2)
#            
#            #if Output is not None:
#            print "Output %s:\n"%m, Output
#            TimestepMatrix = Output[0]
#            
#            for Var_Nr in range(NofVar):
#                TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
#            
#            
#            if Output is None:
#                print "Output is None!!!"
#                break
#                # call working fuction here
#            out_q.task_done()
#        except Exception as error:
#            import logging
#            logging.info("Writer: Timeout occurred {}".format(str(error)))
#            pass
    
#    for NofE in range(NofPa+1):
#        print "I am still working, Farid!!!"
#        item.append(out_q.get_nowait())
    
    #------- Organize the total simulation outputs -------------
    for NofE in range(P):        
        items.append(out_q.get())

    # save each output in their corresponding array
    for NofE in range(P):
        for  Var_Nr in range(NofVar):
            #print "TotalOutput_[%s]\n:" %Var_Nr, TotalOutput_[Var_Nr]

            TotalOutput_[Var_Nr,NofE,:]=items[NofE][Var_Nr+1] #ATTENTION: Var_Nr+1: First array is the timestep
            
    TimestepMatrix = items[0][0]
    
    #wait for this process to complete
    for job in jobs:
        job.join() 
    
    # Print Output
    #print "TotalOutput_:\n", TotalOutput_
    #print "TimestepMatrix:\n", TimestepMatrix

    # -------------------- Read Measurements --------------------
    #MeasurementFile = "Measurement"
    Observations_calibration=Model_Run_exp.MeasurementMatrix(MainDir + MeasurementFile_Calib,TimestepMatrix[:CalibTimeStep])
    Observations_validation=Model_Run_exp.MeasurementMatrix(MainDir + MeasurementFile_Valid,TimestepMatrix[CalibTimeStep:TotalTimeStep])#[:,CalibTimeStep:TotalTimeStep]
    #Observations_validation=Model_Run_exp.MeasurementMatrix(MainDir + MeasurementFile_Calib,TimestepMatrix )[:,CalibTimeStep:]
    Observations = np.hstack((Observations_calibration , Observations_validation))
    
    #########################################################################
    ################### Loop over NofVar  ###################################
    #########################################################################
    # Test for just two simulation results WaterMassLoss and Evaporation
    #for index in range(NofVar):

        #TotalOutput = TotalOutput_[index]
    TotalOutput = TotalOutput_
    print "TotalOutput is prepared!!! \n", TotalOutput
    print "TotalOutput's stdv:\n", np.std(TotalOutput[1], axis = 0)
    #===============================================================
    #          Calculation of Response Surface Coefficient 
    #===============================================================
    print('---> Computing the Response Surfaces Coefficients at the calibration for model!\n')

    ItrNr = 0
#    Coeffs_old = []

    #, NewCombos
    R_int = RS_calculation_aPC(MainDir,NofVar,NofPa, d,ItrNr, TotalOutput, OptimalCollocationPointsBase,modelError,PolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)

    print '-' * 50
    # Calculate of Weights according to the initial Response Surfaces
    # Slice the Response Surface for the first 4 days as the calibration period
    RS_int = R_int[:,:,:CalibTimeStep]
    
    Parametersets = inputDistributions[:,:-1] # to split the modelError
    #modelErrorDistribution = Parametersets[:,-1]

    
    Weights_int, Psi_W = weights(NofPa,d,NofVar,ItrNr,None,Parametersets, RS_int, Observations_calibration, MeasurementError, CalibTimeStep, MCsize,PolynomialBasisFileName,PolynomialDegrees)
    
    print('---> Weights of parameters before Iterations: \n')
    plot_weights(inputDistributions, Weights_int, ParameterNames, Parameters, 'Weights_before Iterations', R_int,0,NofVar)

    print '='*50
    # =========================================================================
    # ======================== Bayesian Updating  =============================
    # =========================================================================
    #  Selection of Parameter Set with Maximum Weights &
    #  derivition of new response surface including Parameter Set with Maximum Weights

    #NewCalibrationEvolution=np.empty((0, NofMeasurementsCalibration))
    #NewOutput=np.empty((0, TotalTimeStep))
    #Bayesian_Evidence_[index]=np.empty((0, 1))
    Bayesian_Evidence=np.empty((0, 1))
    NewCombos=[[] for kk in range(NofItr)]

    print('===> THE ITERATION BEGINS. \n')
    # Improvement of Response Surfaces by modification of integration Points
    for i in range(NofItr):
        if i == 0:
           NewPolynomialDegrees = PolynomialDegrees
           allNewPolynomialDegrees = PolynomialDegrees
           BestIdx=[]
           NrofSparse=1
           sparseCoeff=RS_int
           RS=RS_int
           O_new=O_int
           NewOutput=TotalOutput
           Weights=Weights_int
           BME = np.mean(Weights_int)
           Bayesian_Evidence=np.append(Bayesian_Evidence, BME)
           
        else:
           O_new, NewOutput, NewModelError = selection_max_weight(ShellKeywords,Parametersets, O_new, Weights, NewOutput, TotalTimeStep,nameList,Factors,i, NofVar, m+i)
           modelError = np.hstack((modelError,NewModelError))
           # Print the SRQ's of Model and Response Surface
           Variant_plot(MainDir,ModelName,NofPa,d,Variants,i,Observations_calibration, NewOutput[:,-1,:], O_new[-1],RS ,PolynomialBasisFileName,NewPolynomialDegrees,BestIdx,1,CalibTimeStep)
           #Variant_plot(MainDir,ModelName,NofPa,d,Variants,i,Observations_calibration, NewOutput[:,-1,:], O_new[-1], sparseCoeff,PolynomialBasisFileName,allNewPolynomialDegrees,BestIdx,NrofSparse,CalibTimeStep)
           
           # Select new Combos based on BME
           #MCSize_NewCombos = 20
           #NewPolynomialDegrees = aPC.NewTerm_Selection(MainDir,NofVar,NofPa,d,i,MCSize_NewCombos,TotalTimeStep,Parameters, Percentage,Observations_calibration[1],O_new, NewOutput[0],MeasurementError,NewPolynomialDegrees,PolynomialBasisFileName)
           SampleSize = 500
           NewPolynomialDegrees,BestIdx,allNewPolynomialDegrees,Coeffs,NrofSparse = aPC.NewTerm_Selection(MainDir,NofVar,NofPa,d,i,SampleSize,TotalTimeStep,problem,Observations_calibration,O_new,modelError, NewOutput,ModelError,NewPolynomialDegrees,PolynomialBasisFileName)
           print "NewPolynomialDegrees:\n", NewPolynomialDegrees 
           # sparseCoeff to be used to plot all sparce RS
           sparseCoeff = Coeffs[:,:,:,:CalibTimeStep]
           
           # Calculation of the response surface coefficients
           # , NewCombos[i-1]
           R = RS_calculation_aPC(MainDir,NofVar,NofPa, d,i, NewOutput, O_new,modelError, NewPolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)
           # Slice the Response Surface for the CalibTimeStep as the calibration period
           RS = R[:,:,:CalibTimeStep]
           # Comment: This only indicate that the response surface is fine for the last step
           #if i !=1:
           #    Variant_plot(MainDir,ModelName,NofPa,d,Variants,i+1,Observations_calibration, NewOutput[:,-1,:], O_new[-1], RS,PolynomialBasisFileName,NewPolynomialDegrees,BestIdx,1,CalibTimeStep)
          
           # Merge NewCombos with the polynomial degrees
           #NewPolynomialDegrees = NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos,TotalTimeStep,i)
           #print "ItrNr,NewPolynomialDegrees:\n", i, NewPolynomialDegrees
           
           # Weight calculation with updated response surface
           Weights,Psi_W = weights(NofPa,d,NofVar,i,Psi_W, Parametersets, RS, Observations_calibration, MeasurementError, CalibTimeStep, MCsize,PolynomialBasisFileName,NewPolynomialDegrees)
           # Here, the arigmathic mean of the weights of priors will be calculated as model evaluation criterion
           BME=np.mean(Weights) # Bayesian Evidence
           Bayesian_Evidence=np.append(Bayesian_Evidence, BME)
           
           print('---> Plotting Weights after Iterations %s \n'%i)
           plot_weights(inputDistributions, Weights, ParameterNames, Parameters, 'Weights_Iterations_%s' %i, R, i, NofVar)
               
        
    # =========================================================================
    # =================  Prior & Posterior Distributions ======================
    # =========================================================================
    # Rejection Sampling: filtration of prior distribution via uniform to get posterior

    Post_MC_Vector,Post_MC_Weights =filtration_prior(inputDistributions, Weights, MCsize, NofPa+1)
    print '-' * 50

    print '----> Posterior Parameters vs their Weights\n'
    plot_weights(Post_MC_Vector, Post_MC_Weights, ParameterNames, Parameters, 'Posterior_Parameters-vs_Weights_Calib',R, None,NofVar)

    print '-' * 50
    print('---> Histogram of Posteriors for calibration stage\n')
    plot_Histogram(Post_MC_Vector,Post_MC_Weights,ParameterNames,Parameters, 'Histogram_Posterior_calib')
    
    mean_RS_Cal, stdev_RS_Cal = Mean_Stdev(MainDir,NofPa,d,NofVar,i,None,Post_MC_Vector, RS, PolynomialBasisFileName,NewPolynomialDegrees,CalibTimeStep)
    print '-' * 50
    
    # =========================================================================
    # === STOCHASTIC CALIBRATION of the Model using the Best Parameterset =====
    # =========================================================================
    print('---> Stochastic Calibration of Model is being done. \n')
    # Selection of the parameter set with the highest weight
    sorted_Parametersets_weight_calibration = Post_MC_Vector[np.argsort(Post_MC_Weights)][::-1]
    BestParameterset_calibration = sorted_Parametersets_weight_calibration[0] #[0, 0:NofPa]
    print "Parameterset selected for the stochastic calibration of model: \n", BestParameterset_calibration
    
    # Run model variant for the stochastic calibration model
    #Model_Run_exp.TelemacRun_Calibration(BestParameterset_calibration,R,NofPa, mean_RS_Cal, stdev_RS_Cal, Variants[index])
    # Run_No = -2    >>>>> Calibration
    Output_Calibration=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[-2],nameList,Factors,ParameterNames,BestParameterset_calibration[0], -2,TotalTimeStep)
    
    Calib_Valid_plot(MainDir,NofPa,d,Variants,i,Observations_calibration, Output_Calibration, BestParameterset_calibration, RS, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,NewPolynomialDegrees,CalibTimeStep, "Calib")
    print('---> Calibration of Model has finished. \n')
    print '========================================================================='

    # =========================================================================
    # ====== Using Posterior Parametersets for STOCHASTIC VALIDATION ==========
    # =========================================================================
    print('===> Validation Process for the validation locations.\n')
    # The posterior matrix of calibration is used as the prior parameters of the validation
    Prior_for_Validation_original = Post_MC_Vector #[:, 0:NofPa]

    #============ Resampling of the prior of validation parameters ===============
    # We do resampling for the case in which not enough parametersets remain after filtering
    #print "Prior_for_Validation_original:\n", Prior_for_Validation_original
    if len(Prior_for_Validation_original) < 20:
        print "Resampling has been performed!!!"
        Prior_for_Validation=resampling(Prior_for_Validation_original, MCsize)
        print "Prior_for_Validation:\n", Prior_for_Validation
    else:
        Prior_for_Validation = Prior_for_Validation_original
    #==============================================================================
    # Slice the Response Surface for the first 4 days as the calibration period
    RSValid = R[:,:,CalibTimeStep:TotalTimeStep]
    
    Weights_Validation, Psi = weights(NofPa,d,NofVar,i,None,Prior_for_Validation, RSValid, Observations_validation, MeasurementError, ValidTimeStep, len(Prior_for_Validation),PolynomialBasisFileName,NewPolynomialDegrees)
    #Prior_Validation = np.hstack((Prior_for_Validation, Weights_Validation))

    plot_weights(Prior_for_Validation, Weights_Validation, ParameterNames, Parameters, 'Prior_Weights_Valid', R, None,NofVar)

    Post_MC_Validation, Post_MC_Validation_Weights = filtration_prior(Prior_for_Validation, Weights_Validation, len(Prior_for_Validation), NofPa+1)

    print('---> Histogram of Posteriors for calibration stage\n')

    plot_Histogram(Post_MC_Validation,Post_MC_Validation_Weights,ParameterNames, Parameters, 'Posterior_Histogram_Valid')

    # ========================================================================
    # ====== Storing Mechanism of Bayesian Evidence
    # ========================================================================

    BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Bayesian_Evidence))
    #print (r'The Bayesian evidence for this model is %.5f.' % Bayesian_evidence)

    # =========================================================================
    # ====== STOCHASTIC VALIDATION of the Model using the Best Parameterset ===
    # =========================================================================
    print('---> Validation of Model is being done. \n')
    # Selection of the parameter set with the highest weight
    sorted_Parametersets_weight_validation = Post_MC_Validation[np.argsort(Post_MC_Validation_Weights)][::-1]
    BestParameterset_validation = sorted_Parametersets_weight_validation[0] #[0, 0:NofPa]
    
    mean_RS_Val, stdev_RS_Val = Mean_Stdev(MainDir,NofPa,d,NofVar,i,None,Post_MC_Validation, R, PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep)
    print "Parameterset selected for the stochastic validation of model: \n", BestParameterset_validation
    #Model_Run_exp.TelemacRun_Validation(BestParameterset_validation,R,NofPa, mean_OutputRS_v, stdev_OutputRS_v, Variants[index])
    
    # Run_No = -1    >>>>> Validation
    #Output_Validation=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Valid,ShellKeywords,InputFile_Valid,OutputFile[-1],nameList,Factors,ParameterNames,BestParameterset_validation[0], -1,TotalTimeStep)
    
    Output_Validation = Model_Run_exp.ModelRun(MainDir, ModelName, ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[-1],nameList,Factors,ParameterNames,BestParameterset_validation[0], -1,TotalTimeStep)
    
    #Plot the validation
    Calib_Valid_plot(MainDir,NofPa,d,Variants,i,Observations, Output_Validation, BestParameterset_validation, R, mean_RS_Val, stdev_RS_Val,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep, "Valid")
    
    print "Output_Validation:\n", Output_Validation
    print('---> Validation of Variant has finished. \n')

    #print('============================= Next Model Variant ================================== \n')

    
    ###################### BME Correction ##########################
    Corr_Weight = BME_Corr_Weight(NofPa,d,NofVar,i,O_new, R, NewOutput, ModelError, TotalTimeStep, len(O_new),PolynomialBasisFileName,NewPolynomialDegrees)
    #NewOutput: Oputputmatrix of original model runs
    # O_new: Parameter sets saved during the Bayesian updating
    Weight_RM = np.mean(Corr_Weight)
    
    
    return BayesianEvidenceMatrix , Weight_RM


###################################################################################################
####################################### MAIN PROGRAM ##############################################
###################################################################################################

if __name__ == "__main__":

    ModelName = "Davarzani2014a"
    MainDir= "/temp/farid/LH2/Scripts/NewCodes_04_2018/"
    # Name of the headers in output csv files
    nameList = ['Times','WaterMassLosskgextFac','EvaporationRatemmd', 'Sw', 'temperature']
    Factors= [1.15740740740741e-05, 0.09, 1, 1, 1]# 1.15740740740741e-05 = 1/86400
    ShellKeywords = ['simdirpath','inputFile','MYFirstAttempt', 'pvdFile']
    
    # Calibration
    ShellFile_Calib="experiments_davarzani2014a_constTWalls2d_Calib.sh"
    InputFile_Calib="davarzani2014a_1.22mps.input"
    MeasurementFile_Calib = "Measurement_Calib.txt" # 1.22mps >> 0 to 4 days
    #MeasurementFile_Calib = "Measurement_Synthetic"
    # Validation
    ShellFile_Valid="experiments_davarzani2014a_constTWalls2d_Valid.sh"
    InputFile_Valid="davarzani2014a_3.65mps.input"
    MeasurementFile_Valid = "Measurement_Valid.txt" # 1.22mps >> 4 to 8 days
    #MeasurementFile_Valid = "Measurement_Synthetic"    #Synthetic Case

    # =========================================================================
    # ====================== Framework Settings ===============================
    # =========================================================================
    # Model Variants
    NofVar= 4  # Nr. of Variants (Davarzani's Model Outputs: 3)
    #Variants=np.asarray(["e [mmperd]"]) #"cumulative mass Loss [kg]"
    Variants=np.asarray(["cumulative mass Loss [kg]", "e [mmperd]", "Sw[-]", "Temperature[K]"])
    ##########################################################
    #------------- Uncertain Parameters -------------
    # Parameter Characteristics
    NofPa=5 # Number of Uncertantain Parameters
    #P_range = aPC.P_range

#    ParameterNames = ['RegularizationLowSw','ThermalConductivitySolid', 'Permeability','Porosity', 'ModelError']
    ParameterNames = ['VgN', 'Permeability', 'VgAlpha', 'Snr', 'Porosity', 'ModelError']

#    ParameterNames =  ['RegularizationLowSw', 'VelocityUncertaintyFactor', 'MassUncertaintyFactor', 'TemperatureUncertaintyFactor']
#
#    p_1init=0.01 #RegularizationLowSw
#    p_2init=1.0  #VelocityUncertaintyFactor
#    p_3init=1.0 #MassUncertaintyFactor
#    p_4init=1.0 #TemperatureUncertaintyFactor
    #p1Min, p1Max = 0.01, 0.12 # RegularizationLowSw >> ??? - ??? [Mosthaf et al. 2014]
    #p2Min, p2Max = 5.5, 6.2  # ThermalConductivitySolid 5.9>> 4.3 - 6.3 [Mosthaf et al. 2014]
    p1Min, p1Max = 10.0, 20.0   # VgN = 17.8
    p2Min, p2Max = 1.0e-12, 1.5e-10 # Permeability >> 1e-12 - 1e-10 [Mosthaf et al. 2014]
    p3Min, p3Max = 5e-4, 1e-3   # VgAlpha = 5.8e-4
    p4Min, p4Max = 0.005, 0.05   # Snr = 0.01
    p5Min, p5Max = 0.3, 0.35   # Porosity >> 0.3 - 0.5 [Mosthaf et al. 2014]
    p6Min, p6Max = -0.001, 0.001   # Estimate Error >>> Additional unknown
    
    Parameters = [[p1Min,p1Max],[p2Min,p2Max],[p3Min,p3Max],[p4Min,p4Max],[p5Min,p5Max],[p6Min,p6Max]]
    
    
    
    # Define the model inputs
    problem = {
        'num_vars': NofPa+1,
        'names': ParameterNames,
        'bounds': Parameters
    }
    
    #Parameters = [p_1init,p_2init,p_3init,p_4init]
    Percentage = 2 #5 Percentage of uncertainty analysis

    d = 1 #d-order polynomial
    MCsize = 15000 #15000,150000 # Sample size
    #TotalTimeStep = 96 # Nr of timesteps of the Model
    CalibTimeStep = 96
    ValidTimeStep = 96
    MeasurementError = [0.15 , 2.0, 2.05, 2.5] # [0.15 , 1.0, 1.25, 2.5]  This should be given by the error quantification 
    ModelError = [1.15 , 1.5, 1.0, 1.0]
        
    NofItr = 6 #6 NI+1 NI: Nr of iterations
    
    
    ##########################################################
    # Creating names of output files
    OutputFile=[]
    for RunNr in range(NofPa+NofItr):
        #OutputFile.append(InputFile_Calib.split('.input')[0]+"_%s_staggered-storage.csv" %(RunNr+1))
        OutputFile.append(InputFile_Calib.split('.input')[0]+"_%s.csv" %(RunNr+1))
    
    OutputFile.append(InputFile_Calib.split('.input')[0]+"_calib.csv") # Calibration
    
    OutputFile.append(InputFile_Calib.split('.input')[0]+"_valid.csv") # Validation
    #OutputFile.append(InputFile_Valid.split('.input')[0]+"_valid.csv") # Validation

    # =========================================================================
    # ===== Starting the jobs on cluster via multiprocessing (Entire Script)===
    # =========================================================================
#    out_qq = multiprocessing.Queue()
#    for index in range(NofSF):
#        jobst = []
#
#        task = multiprocessing.Process(target=RS_ST, args=(MainDir,ShellFile,InputFile,OutputFile,O_int,NofPa,TotalTimeStep, out_qq,))
#        jobst.append(task)
#
#        task.start()
#
#    for NofE in range(NofSF): # NodSF: Nr of different model variants
#        BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, out_qq.get()))
#
#    for pr in jobst:
#        pr.join()

#    print '-----------------------------------------------------------------------'
#    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix
    # =========================================================================
    # ==================  Starting the jobs in serie ==========================
    # =========================================================================
    #BayesianEvidenceMatrix=np.empty((0, NofItr))

    # Start the calculations
    #BayesianEvidenceMatrix =RS_ST(MainDir,ModelName,ShellFile,InputFile,OutputFile,O_int,NofPa,NofVar,TotalTimeStep)
    BayesianEvidenceMatrix , Weight_RM=RS_ST(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,ShellFile_Valid,InputFile_Valid,MeasurementFile_Calib,MeasurementFile_Valid,MeasurementError,ModelError,NofVar,NofPa, d, MCsize,problem,ParameterNames,Parameters, Percentage, OutputFile,nameList,Factors,CalibTimeStep,ValidTimeStep)
    # Save the BME in an array
    #BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Out))

    print '-----------------------------------------------------------------------'
    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix

    # =========================================================================
    # ==================  Normalization and Plotting the BMEs =================
    # =========================================================================
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    def normalize_column(A):
        normed_A=np.empty((len(A), 0))
        for col in range(len(A[0])):
             B = (A[:,col] / np.sum(A[:,col]))
             normed_A=np.hstack((normed_A,B[:,None]))
        return normed_A
    normed_BayesianEvidenceMatrix=normalize_column(BayesianEvidenceMatrix)

    # Normalization of BMEs
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    log_normed_BayesianEvidenceMatrix=np.log10(normed_BayesianEvidenceMatrix)
    log_BayesianEvidenceMatrix = np.log10(BayesianEvidenceMatrix)
    
    # Plot the BMEs
    print('---> Plotting the BME vs Iterations \n')
    Nr_Iteration=np.asarray(range(NofItr))
    
    formatting_function = np.vectorize(lambda f: format(f, '8.5E'))
    print "BayesianEvidenceMatrix: \n", formatting_function(BayesianEvidenceMatrix)
    print "log_normed_BayesianEvidenceMatrix: \n", formatting_function(log_normed_BayesianEvidenceMatrix)
    print "Weight_RM:\n", formatting_function(Weight_RM)
    
    Plot_BME(BayesianEvidenceMatrix,log_BayesianEvidenceMatrix, Nr_Iteration, Variants)
    
    # Report the total time
    toc=timeit.default_timer()
    elapsedtime= toc - tic
    ElapsedTime=str(timedelta(seconds=elapsedtime))

    print "Elapsed Time:",ElapsedTime
    print('My work is over. \n')
