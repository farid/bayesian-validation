# -*- coding: utf-8 -*-
"""
Validation Benchmark

Description:

+++++++++++++++++++ Response Surface +++++++++++++++++++
This program reads the input points of parameter p_1 to p_8 form text file 'input1.txt'and generates uniform random values between min and max.
It tries to calculate the coefficent of the response surface:
              Rs=a_o + a_1 x p_1 + a_2 x p_2+ a_3 x p_3+ a_4 x p_4+ a_5 x p_5
The original function is:
              f(p_1 ... p_5) comes from Model Outputs
N= number of iterations to improve the Response surfaces
Nofpa=13
NofMeasurements=111 (The Rhine Model)
McSize=10000 (--> It should be the same in Optimal_integration_points_8pa.py)
ModelRuns=Nofpa+N
NofFrictionCof=2

+++++++++++++++++++ BME Calculation +++++++++++++++++++


Measurement Error shall be set with caution.
@author: Farid Mohammadi, M.Sc.
Created on Wed Apr 04 2018
"""
#import Uniform_dis_pars
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.size'] = 12
matplotlib.rcParams['font.family'] = 'Calibri' #'serif'
import aPC
#import Optimal_integration_points_5pa
import numpy as np
from scipy.linalg import lu
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import Model_Run_exp
import os
import multiprocessing
#import shutil
from scipy import stats
from scipy.stats import norm
#from sklearn.preprocessing import normalize
import timeit
import time
from datetime import timedelta
tic=timeit.default_timer()
#------------------------------------------------------------------------------
def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays to get all combination.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out
#------------------------------------------------------------------------------
def FilePath(InputFile):
    return os.path.abspath(InputFile)
#------------------------------------------------------------------------------
def UpdateResultsName(File_path, lookup, nValue ):
    """Replacement of tel.res and sis.res names"""
    File_in=open(File_path,'r')
    filedata=File_in.readlines()
    for j in range(len(lookup)):
        for line in filedata:
                if lookup[j] in line:
                    oLine=line
                    i=filedata.index(oLine)
                    filedata[i]=lookup[j] +'\t = %s \n' %nValue[j]
        File_out=open(File_path,'w')
        File_out.writelines(filedata)
        File_out.close()
        File_in.close()
    return
#------------------------------------------------------------------------------
def NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos, NofMeasurement,NofItr):
    #NewPolynomialDegrees = [[] for i in range(len(NewCombos))]
    NewPolynomialDegrees = [[] for i in range(NofMeasurement)]
    for itr in range(NofItr):
        for idx in range(NofMeasurement):
            if itr ==0:
                NewPolynomialDegrees[idx]=np.vstack((PolynomialDegrees,NewCombos[itr][idx]))
            else:
                NewPolynomialDegrees[idx]=np.vstack((NewPolynomialDegrees[idx],NewCombos[itr][idx]))
    return NewPolynomialDegrees
#------------------------------------------------------------------------------
def solving_determined_system(array1, array2):
    """
    This fuction solves the determined system of equations.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    Z=np.linalg.lstsq(a, b, rcond=-1)
    R=Z[0]

    return R

#------------------------------------------------------------------------------
def solving_overdetermined_system(array1, array2):
    """
    This fuction solves the overdetermined system of equations using Least Square Method.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    R=np.linalg.lstsq(a,b)[0]

    return R

#------------------------------------------------------------------------------
def RS_calculation_aPC(MainDir,NofPa, d,ItrNr, Model_Output, OptimalCollocationPointsBase,Coeffs_old, PolynomialDegrees, PolynomialBasisFileName, NofMeasurements):
    """
    calculates the response surface for each measurement location
    So each row of the RSTotal matrix is the coefficients of
    one response surface for a specific measurement location

    NofMeasurements: Number of measurement locations
    NofPa: Number of sensitive parameters
    k= will be the output of the TELEMAC runs for each location

    attributes: array1, system_type, NofMeasurements
    """
    RSTotal=np.zeros((NofPa+1+ItrNr , NofMeasurements))
    NewCombos = [[]for i in range(NofMeasurements)]#np.zeros((NofMeasurements, NofItr))
    #----------------------------------------------------
    # Calculate Coeffs of RS for each measurement points
    #----------------------------------------------------
    for i in range(NofMeasurements): 
        
        # Handle the first RS calculation
        if ItrNr >1: #len(Coeffs_old) != 0:
            Coeffs_Old = Coeffs_old[:,i]
            NewPolynomialDegrees=PolynomialDegrees[i]
        elif ItrNr ==1:
            Coeffs_Old = Coeffs_old[:,i]
            NewPolynomialDegrees=PolynomialDegrees
        else:
            Coeffs_Old = None
            NewPolynomialDegrees=PolynomialDegrees
            
        #print "Coeffs_Old:", Coeffs_Old
        #print "NewPolynomialDegrees", NewPolynomialDegrees
        Coeffs, Psi, NewTermCombo= aPC.RS_Builder(MainDir,NofPa, d,ItrNr, Model_Output[:,i], OptimalCollocationPointsBase,Coeffs_Old, NewPolynomialDegrees, PolynomialBasisFileName)

        # Save the NewTermCombo for each Measurement point and Iterations
        NewCombos[i]= NewTermCombo
        
        # Save coefficients in the RSTotal array
        RSTotal[:,i] = Coeffs
        
    
    
    return RSTotal, NewCombos


#------------------------------------------------------------------------------
def weights(N,d,ItrNr,Parameterset, RSTotal, Observations, MeasurementError, NofMeasurements, MCsize,PolynomialBasisFileName,PolynomialDegrees):
    """
    This function calculates the weights of the each parameter set based on the difference of
    its output of RS and the corresponding observation value.
    
    RSTotal = Coeffs
    NofMeasurements: Number of the measurements
    MCsize: Number of the parameter sets
    """
    ParametersetSize = Parameterset.shape[0]
    ObservationSize = Observations.shape[0]
    OutputRS=np.empty((0, NofMeasurements))
    
    # Create Psi Matrix
    start = time.time()
    if ItrNr == 0:
        Psi, NewCombos=aPC.Psi_Creater(MainDir,N,d,ItrNr,Parameterset,RSTotal, PolynomialBasisFileName,PolynomialDegrees)
    else:
        Psi=aPC.Psi_Creater_Weight(MainDir,N,d,ItrNr,Parameterset, PolynomialBasisFileName,PolynomialDegrees,NofMeasurements)
    end = time.time()
    print ("Elapsed time after Psi:", end - start)
    
    #Calculation of the outputs of Response surfaces and their mean and Stdev
    OutputRS = np.dot(Psi, RSTotal)
    
    # Calculate mean and standard deviation
    mean_OutputRS=np.mean(OutputRS, axis=0)
    stdev_OutputRS=np.std(OutputRS, axis=0)
    
    # ========================================================================
    Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
    np.fill_diagonal(Measurement_Error, MeasurementError**2)
    
    #------------------------------------------------------------------------
    if ObservationSize != ParametersetSize:
        Deviation=np.zeros((ParametersetSize,NofMeasurements)) 

        for i in range(ParametersetSize):
            Deviation[i] = Observations - OutputRS[i, :]
    
    else: # for calculation of Weight_RM
        Deviation = Observations - OutputRS
        
    #print "Observation:",Observations
    #print "OutputRS:",OutputRS[0, :]
    #print "Deviation:",Deviation[0]
    # Computation of Weight according to the Deviation
    Weights=np.zeros((ParametersetSize, 1)) 
    for i in range(ParametersetSize):
        Weights[i]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Measurement_Error)), Deviation[i,:])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(Measurement_Error)))

    return Weights, mean_OutputRS, stdev_OutputRS

#------------------------------------------------------------------------------
def BME_Corr_Weight(N,d,ItrNr,Parametersets, RSTotal, OutputOrig, MeasurementError, NofMeasurements, MCsize,PolynomialBasisFileName,PolynomialDegrees):
    """
    Calculates the correction factor for BMEs.
    """
    ParametersetSize = Parametersets.shape[0]
    OutputRS=np.empty((0, NofMeasurements))

    #z=np.ones((ParametersetSize, 1))
    #a=np.hstack((z, Parameterset))
    # Create Psi Matrix
    start = time.time()
    #Psi,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr,Parametersets,RSTotal, PolynomialBasisFileName,PolynomialDegrees)
    Psi=aPC.Psi_Creater_Weight(MainDir,N,d,ItrNr,Parametersets, PolynomialBasisFileName,PolynomialDegrees,NofMeasurements)
    end = time.time()
    print ("Elapsed time after Psi:", end - start)
    
    #Calculation of the outputs of Response surfaces and their mean and Stdev
    OutputRS = np.dot(Psi, RSTotal)
    mean_OutputRS=np.mean(OutputRS, axis=0)
    stdev_OutputRS=np.std(OutputRS, axis=0)
    
    # ========================================================================
    Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
    np.fill_diagonal(Measurement_Error, MeasurementError**2)
    
    #------------------------------------------------------------------------
    Deviation=np.zeros((ParametersetSize,NofMeasurements)) 
    for i in range(ParametersetSize):
        Deviationofparameterset = OutputOrig - OutputRS[i, :]
        Deviation[i] = Deviationofparameterset 
    #print "Observation:",OutputOrig
    #print "OutputRS:",OutputRS[0, :]
    #print "Deviation:",Deviation[0]
    # Computation of Weight according to the Deviation
    Weights=np.zeros((ParametersetSize, 1)) 
    for i in range(ParametersetSize):
        Weights[i]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Measurement_Error)), Deviation[i,:])))/np.sqrt(2*np.pi*MeasurementError**2)

    return Weights, mean_OutputRS, stdev_OutputRS



#------------------------------------------------------------------------------
def filtration_prior(Parameterset, Weights ,MCsize, NofPa):
    """Rejection Sampling: filtration of prior distribution via uniform"""
    ii=0
    unif=np.random.rand(1, MCsize)
    Parameterset_weight=np.hstack((Parameterset, Weights))
    Post_MC_Vector=np.empty((0, NofPa+1))
    for i in range(len(Weights)):
        if Weights[i, :]/np.max(Weights) > unif[:, i]:
            ii += 1
            Post_Vector = Parameterset_weight[i, :]
            Post_MC_Vector=np.vstack((Post_MC_Vector, Post_Vector))
    
    return Post_MC_Vector[:, 0:NofPa], Post_MC_Vector[:, NofPa][:,None]

#------------------------------------------------------------------------------
def Mean_Stdev(MainDir,N,d,ItrNr,Parametersets, RSTotal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements):
    """
    Calculates the mean and standard deviation of the SQRs using the reduced model
    NofMeasurements: Number of the measurements 
    """    
    #Psi,NewTermCombo  = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parametersets,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    Psi=aPC.Psi_Creater_Weight(MainDir,N,d,ItrNr,Parametersets, PolynomialBasisFileName,PolynomialDegrees,NofMeasurements)
    print "Psi: \n", Psi
    
    OutputRS = np.dot(Psi, RSTotal)

    #------- Calculation of mean and Stdev of the outputs of Response surfaces----------------
    mean_OutputRS=np.mean(OutputRS, axis=0)
    stdev_OutputRS=np.std(OutputRS, axis=0)

    return mean_OutputRS, stdev_OutputRS

#------------------------------------------------------------------------------
def plot_weights(Parameterset, Weights,ParameterNames, Name,RSTotal, Variant_No,Nr_Iteration):
    """
    This Function plots each parameter versus its weight

    """
    fig = plt.figure()

    fig.set_figheight(12)
    fig.set_figwidth(20)

    Colors = ['b.','g.','r.','c.','m.']
    header1 = []
    header2 = []
    #header2 = ["c_0"]
    for coeffs in range(len(RSTotal[:,0])):
        header2.append('c_%s'%(coeffs)) #+1
        
    # Decide on shape of the figure
    if len(ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
    
    # Loop over the paramters and plot their weights
    for i,ParameterName in enumerate(ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-5)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        ax1.plot(Parameterset[:, i], Weights, Colors[i])
        plt.xlim(np.min(Parameterset[:, i]), np.max(Parameterset[:, i]))
        header1.append('P_%s'%(i+1))
        #header2.append('c_%s'%(i+1))
        plt.xlabel('p$_%s$'%(i+1))
        plt.ylabel('Weights')
        plt.title(ParameterName)

    plt.tight_layout()
    
    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Output_'+Variant_No) 
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(Name +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save parameter sets with their weights
    np.savetxt(Name+'.txt', np.hstack((Parameterset, Weights)), delimiter=',', header= " ".join(header1)+ "   Weight")
    # Save Response surfaces in a text file
    if Nr_Iteration != None:
        np.savetxt('RSTotal_Itr%s.txt'%Nr_Iteration, RSTotal.T, delimiter=',',  header=" ".join(header2))
    
    os.chdir('..')
    
#---------------------------------------------------------------------------
def plot_Histogram(Post_MC_Vector,Post_MC_Weights,ParameterNames, Name, Variant_No):
    """
    This Function plots the histogram of posterior parameter sets.
    """
    fig = plt.figure()

    fig.set_figheight(12) #20
    fig.set_figwidth(20)
  
    header1 = []
    Colors = ['b','g','r','c','m']

    # Decide on shape of the figure
    if len(ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
        
    # Loop over the paramters and plot their histograms        
    for i,ParameterName in enumerate(ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-5)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        x1=Post_MC_Vector[:, i]
        n, bins, patches = ax1.hist(x1, 50, normed=1, facecolor=Colors[i], alpha=0.75)
        
        # ------ Fitting data -----------
        xs1 = np.linspace(x1.min(), x1.max(), 200)
        kde1 = stats.gaussian_kde(x1)
        ax1.plot(xs1, kde1(xs1), Colors[i]+'--', lw=2)
        
        ax1.set_xlabel('Values of p$_%s$'%(i+1))
        ax1.set_ylabel('Density')
        ax1.grid(True)
        anchored_text = AnchoredText("$\mu$=%2f \n $\sigma$=%2f \n $S$=%2f \n $k$=%2f" %(np.mean(x1), np.std(x1), stats.skew(x1), stats.kurtosis(x1)), loc=2)
        ax1.add_artist(anchored_text)
        header1.append('P_%s'%(i+1))

    #plt.suptitle('Probablity Density Function')

    plt.tight_layout()

    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Output_'+Variant_No)
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(Name +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save POSTERIOR parameter sets with their weights
    np.savetxt(Name+'.txt', np.hstack((Post_MC_Vector, Post_MC_Weights)), delimiter=',',header= " ".join(header1)+ "   Weight")

    os.chdir('..')
    
#-------------------------------------------------------------------------------------------------------
def Calib_Valid_plot(MainDir,N,d,Variants,NofV,ItrNr,Observations_calib, Output_calib, Parameterset_Calib, RSTotal, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements,PlotName):
    """ 
    This function plots the SQR's output from the full complexity model and its reduced version (aPCE).
    """
    # Calculate the SQR's output from the reduced version (aPCE)
    #Psi_Calib,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parameterset_Calib,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    Psi_Calib=aPC.Psi_Creater_Weight(MainDir,N,d,ItrNr,Parameterset_Calib, PolynomialBasisFileName,PolynomialDegrees,NofMeasurements)
    print "Psi_Calib:\n", Psi_Calib
    RSOutput_calib = np.dot(Psi_Calib, RSTotal)[0]
    print "RSOutput_calib:\n", RSOutput_calib 
    X_value = Observations_calib[0] #range(len(RSOutput_calib))
    print "Observations_calib:\n", Observations_calib
    
    fig = plt.figure()
    fig.set_figheight(12) #20
    fig.set_figwidth(20)
    ax = plt.subplot(1,1,1)

    # Plot the measurement
    ax.plot(X_value, Observations_calib[1], "x", color='k' ,label='Measurements' , lw=2)
    # Plot the output from the original model
    ax.plot(X_value, Output_calib, "o-", color="brown" ,label='Original model' , lw=2)
    # Plot the output from the surrogate model
    ax.plot(X_value, RSOutput_calib, ".-",color="navy" , label='Response surface', lw=2)
    
    # Plot confidence interval of reduced model
    Ub=mean_RS_Cal+2*stdev_RS_Cal
    Lb=mean_RS_Cal-2*stdev_RS_Cal
    ax.fill_between(X_value, Ub, Lb,color="gray", alpha=.5)
    
    # Patch the confidence interval label
    extraString = "$\mu \pm 2\sigma$"
    handles, labels = ax.get_legend_handles_labels()
    handles.append(mpatches.Patch(color='gray', label=extraString))
    ax.legend(handles=handles, loc='best')
    
    
    # Axis labels
    plt.xlabel('Time [d]')
    plt.ylabel(Variants[NofV])
    
    # Save the figure and the BMEs into a folder
    #newpath = (( ModelName + r'_Variant%s_Run_Calib') %(VarID+1))
    # save the figure to file
    if PlotName == "Calib":
        fig.savefig('Calibration_Plot.png')   
    else:
        fig.savefig('Validation_Plot.png')
    plt.close(fig)
    
#-------------------------------------------------------------------------------------------------------
def Plot_BME(BayesianEvidenceMatrix,logBayesian_EvidenceMatrix, Nr_Iteration,Variants):
    fig = plt.figure()
    N_Var=len(logBayesian_EvidenceMatrix)
    Styles= ['b-','g-','r-','c-','m-','y-']
    Markers = ['o','*','+','s','D','p']
    
    # Plot logBayesian_EvidenceMatrix for all variants
    for NofV in range(N_Var):
        plt.plot(Nr_Iteration, logBayesian_EvidenceMatrix[NofV], Styles[NofV], marker=Markers[NofV], label=Variants[NofV], lw=2)
    
    plt.xlim(np.min(Nr_Iteration), np.max(Nr_Iteration))
    plt.xlabel('Number of Iterations')
    plt.ylabel('log$_{10}$(BME)')
    plt.ylim(np.min(logBayesian_EvidenceMatrix)-1, np.max(logBayesian_EvidenceMatrix)+1)
    plt.title('Bayesian Model Evidence vs. Iterations')
    plt.legend(loc='best')
    plt.grid()
    
    # Save the figure and the BMEs into a folder
    newpath = ((r'Output_BME'))
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    # save the figure to file
    fig.savefig('Log_BME_vs_Itr.png')   
    plt.close(fig)

    # save BayesianEvidenceMatrix & logBayesian_EvidenceMatrix in a text file
    np.savetxt('BME.txt', BayesianEvidenceMatrix, delimiter=',')
    np.savetxt('Log_BME.txt', logBayesian_EvidenceMatrix, delimiter=',')

    os.chdir('..')

#-------------------------------------------------------------------------------------
def resampling(x, P_range, MCsize):
    size=MCsize / (4*len(x))
    resampled=np.empty((size,0))
    for i in range(len(x[1,:])):
        kde = stats.gaussian_kde(x[:, i])
        sample = kde.resample(size)
        #filtered_sample = sample[(sample >= P_range[i, 0]) & (sample <= P_range[i, 1])]
        filtered_sample = sample[(sample >= np.min(x[:, i])) & (sample <= np.max(x[:, i]))]
       # print filtered_sample.size
        while True:
             len(filtered_sample) < size
             kde_new = stats.gaussian_kde(filtered_sample)
             size_new=size-len(filtered_sample)
             sample_new=kde_new.resample(size_new)
             filtered_sample=np.append(filtered_sample,sample_new)
          #   filtered_sample = sample_new[(sample_new >= P_range[i, 0]) & (sample_new <= P_range[i, 1])]
             if len(filtered_sample) == size:
                 break

        resampled=np.hstack((resampled,filtered_sample[:, None]))
        #resampled=np.hstack((resampled,sample.T))
    return resampled

#-------------------------------------------------------------------------------------
def selection_max_weight(ShellKeywords,Parameterset, CollocationPoints, Weight, TotalOutput, NewOutput, NofMeasurements,nameList,Factors, NoItr, index, m):
        """ This function does the following operations:
        1) Computes the weights of the optimal integration points in each step
        2) Sorts the parametersets based on the weights calculated in Step1
        3) Computes the weights of the parameter sets
        4) Selects the parameterset with the best weight
        5) Checks if already exist in the optimal integration points (CollocationPoints)
        6) Run the model for the new collocation point

        Output:
        Updated integration points for solving an overdetermined system of equations
        """

        print 80 * '-'
        print('---> Selection of the Parameter Set with Maximum Weight \n')

        Parametersets_weight=np.hstack((Parameterset, Weight))
        sorted_Parametersets_weight=Parametersets_weight[np.argsort(Parametersets_weight[:, NofPa])][::-1]

        if sorted_Parametersets_weight[0, 0:NofPa] in CollocationPoints:
            NewCollocationPoint=sorted_Parametersets_weight[1, 0:NofPa]
        else:
            NewCollocationPoint=sorted_Parametersets_weight[0, 0:NofPa]
        print 40 * '-'
        print "NewCollocationPoint:", NewCollocationPoint
        print 40 * '-'
        
        Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[m],nameList,Factors,ParameterNames,NewCollocationPoint ,index ,m,TotalTimeStep)
        Output = Output[1] # Takes the second column from the output matrix
        
        if NoItr == 1:
            NewOutput=np.vstack((TotalOutput , Output))
        else:
            NewOutput=np.vstack((NewOutput , Output))

        O_new=np.vstack((CollocationPoints, NewCollocationPoint))

        return O_new, NewOutput


#-------------------------------------------------------------------------------------
def Modelrun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int ,index,m,TotalTimeStep, out_q):
    Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int[m], index,m,TotalTimeStep)   #Variants[index] = defines each model alternative
    out_q.put(Output)
    return out_q

#-------------------------------------------------------------------------------------
def RS_ST(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,ShellFile_Valid,InputFile_Valid,MeasurementFile_Calib,MeasurementFile_Valid,NofVar,NofPa, d, MCsize, ParameterNames,Parameters, Percentage,OutputFile,nameList,Factors,TotalTimeStep): #out_q1,index,NofPa
    """
    This function is the main function. 
    It runs the model with the help of the Model_Run_exp script and creates the response surfaces and does the Bayesian updating. 
    """
    
    # Here, the distributions of the input parameters are imported from the :
    Parametersets,OptimalCollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName = aPC.CollocationPoints(MainDir,NofPa, d,0, MCsize,Parameters, Percentage)
    #Parametersets_dp1,OptimalCollocationPointsBase_dp1,PolynomialDegree_dp1,PolynomialBasisFileName_dp1 = aPC.CollocationPoints(MainDir,NofPa, d+1,0, MCsize,Parameters, Percentage)
    
    # Here, the combinations of points are imported to from Optimal_integration_points:
    # Collocation Points
    O_int = OptimalCollocationPointsBase
    
    print ('The %s optimal collocation points are:\n' %(NofPa+1)), O_int
    print '-' * 50
    
    #------------------------------------------------------------------------------
    # Here the input of the formulas are given:
    BayesianEvidenceMatrix=np.empty((0, NofItr))  # This should be equal to the iteration number

    # Initialization of arrays to store  simulation outputs
    TotalOutput_=[[] for i in range(NofVar)]
    for Var_Nr in range(NofVar):
        TotalOutput_[Var_Nr]=np.empty((0, TotalTimeStep))

    ################ Serial Simulation   #####################
    # Run simulations at the collocation points
#    for m in range(NofPa+1):
#        Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,InputFile,OutputFile[m],O_int[m],NofPa ,m,TotalTimeStep)
#        # save each output in their corresponding array
#        for Var_Nr in range(NofVar):
#            TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
    
    ################ Parallel Simulation   #####################
    jobs = []
    items =[]
    out_q = multiprocessing.Queue()
    
    # Simulation runs for the optimal integration points     
    for m in range(NofPa+1):
        p = multiprocessing.Process(target=Modelrun, args=(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[m],nameList,Factors,ParameterNames,O_int, 0 ,m,TotalTimeStep, out_q,)) #index here is 0: Variant 1
        jobs.append(p)
        p.start()
        
    #------- Try to prepare one matrix with the total model results -------------
#        while not out_q.empty():
#            print "I am still working, Farid!!!"
#            items.append(out_q.get())
             #----------------------
#        try:
#            print "Try & Except!!!"
#            Output = out_q.get(timeout=2)
#            
#            #if Output is not None:
#            print "Output %s:\n"%m, Output
#            TimestepMatrix = Output[0]
#            
#            for Var_Nr in range(NofVar):
#                TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
#            
#            
#            if Output is None:
#                print "Output is None!!!"
#                break
#                # call working fuction here
#            out_q.task_done()
#        except Exception as error:
#            import logging
#            logging.info("Writer: Timeout occurred {}".format(str(error)))
#            pass
    
#    for NofE in range(NofPa+1):
#        print "I am still working, Farid!!!"
#        item.append(out_q.get_nowait())
    
    #------- Organize the total simulation outputs -------------
    for NofE in range(NofPa+1):        
        items.append(out_q.get())

    # save each output in their corresponding array
    for NofE in range(NofPa+1):
        for  Var_Nr in range(NofVar):
            TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], items[NofE][Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
            #print "TotalOutput_[%s]\n:" %Var_Nr, TotalOutput_[Var_Nr]
#        #TotalOutput=np.vstack((TotalOutput, out_q.get())) # It extracts only the EvaporationRate from the output csv file
    
    TimestepMatrix = items[0][0]

    #wait for this process to complete
    for job in jobs:
        job.join() 
    
    # Print Output
    #print "TotalOutput_:\n", TotalOutput_
    #print "TimestepMatrix:\n", TimestepMatrix

    # -------------------- Read Measurements --------------------
    #MeasurementFile = "Measurement"
    MeasurementCalib=Model_Run_exp.MeasurementMatrix(MainDir + MeasurementFile_Calib,TimestepMatrix )
    MeasurementValid=Model_Run_exp.MeasurementMatrix(MainDir + MeasurementFile_Valid,TimestepMatrix )
    
    Observations_calibration=MeasurementCalib[1] #Mass loss 
    Observations_validation=MeasurementValid[1] # Mass loss 
    print "Measurement Calib: \n", Observations_calibration
    print "Measurement Valid: \n", Observations_validation
    
    #########################################################################
    ################### Loop over NofVar  ###################################
    #########################################################################
    # Test for just two simulation results WaterMassLoss and Evaporation
    for index in range(NofVar):

        TotalOutput = TotalOutput_[index]
        print "TotalOutput is being prepared!!! \n", TotalOutput
        
        #===============================================================
        #          Calculation of Response Surface Coefficient 
        #===============================================================
        print('---> Computing the Response Surfaces Coefficients at the calibration for model variant %s\n' %Variants[index])
        #R_int=RS_calculation_aPC(O_int, 'determined', TotalOutput, NofPa, NofMeasurements)
        ItrNr = 0
        Coeffs_old = []
        R_int, NewCombos=RS_calculation_aPC(MainDir,NofPa, d,ItrNr, TotalOutput, OptimalCollocationPointsBase,Coeffs_old,PolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)
        
        print '-' * 50
        # Calculate of Weights according to the initial Response Surfaces
        Weights_int, mean_OutputRS, stdev_OutputRS=weights(NofPa,d,ItrNr,Parametersets, R_int, Observations_calibration, MeasurementError, TotalTimeStep, MCsize,PolynomialBasisFileName,PolynomialDegrees)

        print('---> Weights of parameters before Iterations: \n')
        plot_weights(Parametersets, Weights_int, ParameterNames, 'Weights_Variant_%s_before Iterations' %(index+1), R_int,Variants[index],0)

        print '='*50
        # =========================================================================
        # ======================== Bayesian Updating  =============================
        # =========================================================================
        #  Selection of Parameter Set with Maximum Weights &
        #  derivition of new response surface including Parameter Set with Maximum Weights

        #NewCalibrationEvolution=np.empty((0, NofMeasurementsCalibration))
        NewOutput=np.empty((0, TotalTimeStep))
        #Bayesian_Evidence_[index]=np.empty((0, 1))
        Bayesian_Evidence=np.empty((0, 1))
        NewCombos=[[] for kk in range(NofItr)]

        print('===> THE ITERATION BEGINS. \n')
        # Improvement of Response Surfaces by modification of integration Points
        for i in range(NofItr):
            if i == 0:
               NewPolynomialDegrees = PolynomialDegrees
               R=R_int
               O_new=O_int
               Weights=Weights_int
               BME = np.mean(Weights_int)
               Bayesian_Evidence=np.append(Bayesian_Evidence, BME)
               
            else:
               O_new, NewOutput = selection_max_weight(ShellKeywords,Parametersets, O_new, Weights, TotalOutput,NewOutput, TotalTimeStep,nameList,Factors,i, index, m+i)
               # Calculation of the response surface coefficients
               R , NewCombos[i-1]= RS_calculation_aPC(MainDir,NofPa, d,i, NewOutput, O_new,R, NewPolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)
               # Merge NewCombos with the polynomial degrees
               NewPolynomialDegrees =NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos,TotalTimeStep,i)
               #print "ItrNr,NewPolynomialDegrees:\n", i, NewPolynomialDegrees
               # Weight calculation with updated response surface
               Weights, mean_OutputRS, stdev_OutputRS=weights(NofPa,d,i, Parametersets, R, Observations_calibration, MeasurementError, TotalTimeStep, MCsize,PolynomialBasisFileName,NewPolynomialDegrees)
               # Here, the arigmathic mean of the weights of priors will be calculated as model evaluation criterion
               BME=np.mean(Weights) # Bayesian Evidence
               Bayesian_Evidence=np.append(Bayesian_Evidence, BME)
               
               print('---> Plotting Weights after Iterations %s \n'%i)
               plot_weights(Parametersets, Weights, ParameterNames, 'Weights_Variant_%s_Iterations_%s' %(index+1,i), R,Variants[index],i)
               
        
        # =========================================================================
        # =================  Prior & Posterior Distributions ======================
        # =========================================================================
        # Rejection Sampling: filtration of prior distribution via uniform to get posterior

        Post_MC_Vector,Post_MC_Weights =filtration_prior(Parametersets, Weights, MCsize, NofPa)
        print '-' * 50

        print '----> Posterior Parameters vs their Weights\n'
        plot_weights(Post_MC_Vector, Post_MC_Weights, ParameterNames, 'Posterior_Parameters-vs_Weights_%s' %(index+1),R, Variants[index], None)

        print '-' * 50
        print('---> Histogram of Posteriors for calibration stage\n')
        plot_Histogram(Post_MC_Vector,Post_MC_Weights,ParameterNames, 'Histogram_Posterior_parameters_calibration_Variant_%s'%(index+1), Variants[index])
        
        mean_RS_Cal, stdev_RS_Cal = Mean_Stdev(MainDir,NofPa,d,i,Post_MC_Vector, R,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep)
        print '-' * 50
        
        # =========================================================================
        # === STOCHASTIC CALIBRATION of the Model using the Best Parameterset =====
        # =========================================================================
        print('---> Stochastic Calibration of Model %s is being done. \n' %index)
        # Selection of the parameter set with the highest weight
        sorted_Parametersets_weight_calibration = Post_MC_Vector[np.argsort(Post_MC_Weights)][::-1]
        BestParameterset_calibration = sorted_Parametersets_weight_calibration[0] #[0, 0:NofPa]
        print "Parameterset selected for the stochastic calibration of model %s: \n" %index, BestParameterset_calibration
        
        # Run model variant for the stochastic calibration model
        #Model_Run_exp.TelemacRun_Calibration(BestParameterset_calibration,R,NofPa, mean_RS_Cal, stdev_RS_Cal, Variants[index])
        # Run_No = -2    >>>>> Calibration
        Output_Calibration=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,OutputFile[-2],nameList,Factors,ParameterNames,BestParameterset_calibration[0],index, -2,TotalTimeStep)
        
        Calib_Valid_plot(MainDir,NofPa,d,Variants,index,i,MeasurementCalib, Output_Calibration[1], BestParameterset_calibration, R, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep, "Calib")
        print('---> Calibration of Model %s has finished. \n' %index)
        print '========================================================================='

        # =========================================================================
        # ====== Using Posterior Parametersets for STOCHASTIC VALIDATION ==========
        # =========================================================================
        print('===> Validation Process for the validation locations.\n')
        # The posterior matrix of calibration is used as the prior parameters of the validation
        Prior_for_Validation_original = Post_MC_Vector #[:, 0:NofPa]

        #============ Resampling of the prior of validation parameters ===============
        # We don't do resampling for the formula which used in the twin test
        #Prior_for_Validation=resampling(Prior_for_Validation_original, Resampling_size)

        #if index == twinfor_index:
        #    Prior_for_Validation = Prior_for_Validation_original
        #else:
        #    Prior_for_Validation=resampling(Prior_for_Validation_original, P_range, MCsize)

        Prior_for_Validation = Prior_for_Validation_original
        #==============================================================================
        Weights_Validation, mean_OutputRS_v, stdev_OutputRS_v=weights(NofPa,d,i,Prior_for_Validation, R, Observations_validation, MeasurementError, TotalTimeStep, len(Prior_for_Validation),PolynomialBasisFileName,NewPolynomialDegrees)
        Prior_Validation = np.hstack((Prior_for_Validation, Weights_Validation))

        plot_weights(Prior_for_Validation, Weights_Validation, ParameterNames, 'Weights of prior parametersets of validation_%s' %index, R, Variants[index], None)

        Post_MC_Validation,Post_MC_Validation_Weights=filtration_prior(Prior_for_Validation, Weights_Validation, len(Prior_for_Validation),NofPa)

        print('---> Histogram of Posteriors for calibration stage\n')

        plot_Histogram(Post_MC_Validation,Post_MC_Validation_Weights,ParameterNames, 'Histogram of posterior parameters_validation_%s' %(index+1), Variants[index])

        # ========================================================================
        # ====== Storing Mechanism of Bayesian Evidence
        # ========================================================================

        BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Bayesian_Evidence))
        #print (r'The Bayesian evidence for this model is %.5f.' % Bayesian_evidence)

        # =========================================================================
        # ====== STOCHASTIC VALIDATION of the Model using the Best Parameterset ===
        # =========================================================================
        print('---> Validation of Model is being done. \n')
        # Selection of the parameter set with the highest weight
        sorted_Parametersets_weight_validation = Post_MC_Validation[np.argsort(Post_MC_Validation_Weights)][::-1]
        BestParameterset_validation = sorted_Parametersets_weight_validation[0] #[0, 0:NofPa]
        
        mean_RS_Val, stdev_RS_Val = Mean_Stdev(MainDir,NofPa,d,i,Post_MC_Validation, R,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep)
        print "Parameterset selected for the stochastic validation of model %s: \n" %index, BestParameterset_validation
        #Model_Run_exp.TelemacRun_Validation(BestParameterset_validation,R,NofPa, mean_OutputRS_v, stdev_OutputRS_v, Variants[index])
        # Run_No = -1    >>>>> Validation
        Output_Validation=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Valid,ShellKeywords,InputFile_Valid,OutputFile[-1],nameList,Factors,ParameterNames,BestParameterset_validation[0],index, -1,TotalTimeStep)
        #Plot the calidation
        Calib_Valid_plot(MainDir,NofPa,d,Variants,index,i,MeasurementValid, Output_Validation[1], BestParameterset_validation, R, mean_RS_Val, stdev_RS_Val,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep, "Valid")
        print "Output_Validation:\n", Output_Validation
        print('---> Validation of Variant %s has finished. \n' %index)

        print('============================= Next Model Variant ================================== \n')
    
    
    ###################### BME Correction ##########################
    Corr_Weight,mean_OutputRS_v, stdev_OutputRS_v=weights(NofPa,d,i,O_new, R, NewOutput, MeasurementError, TotalTimeStep, len(O_new),PolynomialBasisFileName,NewPolynomialDegrees)
    #NewOutput: Oputputmatrix of original model runs
    # O_new: Parameter sets saved during the Bayesian updating
    Weight_RM = np.mean(Corr_Weight)
    
    
    return BayesianEvidenceMatrix , Weight_RM


###################################################################################################
####################################### MAIN PROGRAM ##############################################
###################################################################################################

if __name__ == "__main__":

    ModelName = "Davarzani2014a"
    MainDir= "/temp/farid/LH2/Scripts/NewCodes_04_2018/"
    
    nameList = ['Times','WaterMassLosskgextFac','EvaporationRatemmd']
    Factors= [1.15740740740741e-05, 0.09, 1]# 1.15740740740741e-05 = 1/86400
    ShellKeywords = ['simdir','input','Output.Name']
    
    # Calibration
    ShellFile_Calib="experiments_davarzani2014a_constTWalls2d_Calib.sh"
    InputFile_Calib="davarzani2014a_1.22mps.input"
    MeasurementFile_Calib = "Measurement_Calib" # 1.22mps
    
    # Validation
    ShellFile_Valid="experiments_davarzani2014a_constTWalls2d_Valid.sh"
    InputFile_Valid="davarzani2014a_3.65mps.input"
    MeasurementFile_Valid = "Measurement_Valid" # 3.65 mps
    #MeasurementFile = "Measurement_Synthetic"    #Synthetic Case

    # =========================================================================
    # ====================== Framework Settings ===============================
    # =========================================================================
    # Model Variants
    NofVar= 1  # Nr. of Variants (Davarzani's Model Outputs: 3)
    #Variants=np.asarray(["Mass Loss [kg]", "Evaporation [mm/d]", "Sw [-]"])
    Variants=np.asarray(["Mass_Loss", "Evaporation", "Sw"])
    ##########################################################
    #------------- Uncertain Parameters -------------
    # Parameter Characteristics
    NofPa=4 #5 Number of Uncertantain Parameters
    #P_range = aPC.P_range

    #ParameterNames = ['SolDependentEnergyTemperature','PlexiglassThermalConductivity', 'Permeability','Porosity', 'Swr']
    #ParameterNames =  ['Porosity', 'Swr'] # Small test
    ParameterNames =  ['RegularizationLowSw', 'VelocityUncertaintyFactor', 'MassUncertaintyFactor', 'TemperatureUncertaintyFactor']

    p_1init=0.01 #RegularizationLowSw
    p_2init=1.0  #VelocityUncertaintyFactor
    p_3init=1.0 #MassUncertaintyFactor
    p_4init=1.0 #TemperatureUncertaintyFactor
    #p_5init=0.08383 #Swr

    Parameters = [p_1init,p_2init,p_3init,p_4init]
    #Parameters = [p_4init,p_5init]
    Percentage = 5 # Percentage of uncertainty analysis

    d=1 #d-order polynomial
    MCsize=200 #15000,150000 # Sample size
    TotalTimeStep = 96 #Nr of timesteps of the Model
    MeasurementError= 0.15 # This should be given by the error quantification 
    NofItr=6 # N+1 N: Nr of iterations
    ##########################################################
    # Creating names of output files
    OutputFile=[]
    for RunNr in range(NofPa+NofItr):
        OutputFile.append(InputFile_Calib.split('.input')[0]+"_%s_staggered-storage.csv" %(RunNr+1))
    OutputFile.append(InputFile_Calib.split('.input')[0]+"_calib_staggered-storage.csv" ) # Calibration
    OutputFile.append(InputFile_Valid.split('.input')[0]+"_valid_staggered-storage.csv" ) # Validation
    
    # =========================================================================
    # ===== Starting the jobs on cluster via multiprocessing (Entire Script)===
    # =========================================================================
#    out_qq = multiprocessing.Queue()
#    for index in range(NofSF):
#        jobst = []
#
#        task = multiprocessing.Process(target=RS_ST, args=(MainDir,ShellFile,InputFile,OutputFile,O_int,NofPa,TotalTimeStep, out_qq,))
#        jobst.append(task)
#
#        task.start()
#
#    for NofE in range(NofSF): # NodSF: Nr of different model variants
#        BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, out_qq.get()))
#
#    for pr in jobst:
#        pr.join()

#    print '-----------------------------------------------------------------------'
#    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix
    # =========================================================================
    # ==================  Starting the jobs in serie ==========================
    # =========================================================================
    #BayesianEvidenceMatrix=np.empty((0, NofItr))

    # Start the calculations
    #BayesianEvidenceMatrix =RS_ST(MainDir,ModelName,ShellFile,InputFile,OutputFile,O_int,NofPa,NofVar,TotalTimeStep)
    BayesianEvidenceMatrix , Weight_RM=RS_ST(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,ShellFile_Valid,InputFile_Valid,MeasurementFile_Calib,MeasurementFile_Valid,NofVar,NofPa, d, MCsize,ParameterNames,Parameters, Percentage, OutputFile,nameList,Factors,TotalTimeStep)
    # Save the BME in an array
    #BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Out))

    print '-----------------------------------------------------------------------'
    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix

    # =========================================================================
    # ==================  Normalization and Plotting the BMEs =================
    # =========================================================================
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    def normalize_column(A):
        normed_A=np.empty((len(A), 0))
        for col in range(len(A[0])):
             B = (A[:,col] / np.sum(A[:,col]))
             normed_A=np.hstack((normed_A,B[:,None]))
        return normed_A
    normed_BayesianEvidenceMatrix=normalize_column(BayesianEvidenceMatrix)

    # Normalization of BMEs
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    log_normed_BayesianEvidenceMatrix=np.log10(normed_BayesianEvidenceMatrix)
    log_BayesianEvidenceMatrix = np.log10(BayesianEvidenceMatrix)
    
    # Plot the BMEs
    print('---> Plotting the BME vs Iterations \n')
    Nr_Iteration=np.asarray(range(NofItr))

    print "BayesianEvidenceMatrix: \n", BayesianEvidenceMatrix
    print "log_normed_BayesianEvidenceMatrix: \n", log_normed_BayesianEvidenceMatrix
    print "Weight_RM:\n", Weight_RM
    
    Plot_BME(BayesianEvidenceMatrix,log_BayesianEvidenceMatrix, Nr_Iteration, Variants)
    
    # Report the total time
    toc=timeit.default_timer()
    elapsedtime= toc - tic
    ElapsedTime=str(timedelta(seconds=elapsedtime))

    print "Elapsed Time:",ElapsedTime
    print('My work is over. \n')
