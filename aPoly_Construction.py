#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
% ========================================================================
% Construction of Data-driven Orthonormal Polynomial Basis
% Author: Dr.-Ing. habil. Sergey Oladyshkin
% Department of Stochastic Simulation and Safety Research for Hydrosystems
% Institute for Modelling Hydraulic and Environmental Systems
% Universitaet Stuttgart, Pfaffenwaldring 5a, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% http://www.iws-ls3.uni-stuttgart.de
% The current program is using definition of arbitrary polynomial chaos expansion (aPC) which is presented in the following manuscript:
% Oladyshkin, S. and W. Nowak. Data-driven uncertainty quantification using the arbitrary polynomial chaos expansion. Reliability Engineering & System Safety, Elsevier, V. 106, P. 179�190, 2012. DOI: 10.1016/j.ress.2012.05.002.
% ========================================================================
"""
import numpy as np
import os

def Function(Data, Degree, Roots_FileName,Poly_FileName):
    """
      Input parameters:
          % Data - raw data array
          % Degree - higher degree of the orthonormal polynomial basis
          % Poly_FileName - file name in which the orthonormal polynomial basis should be storred
    """
    #----- Initialization
    d=Degree #Degree of polinomial expansion
    dd=d+1 #Degree of polinomial for roots defenition
    L_norm=1 # L-norm for polnomial normalization
    NumberOfDataPoints=len(Data)

    print("\n")
    print("---> Construction of Arbitrary Polynomil Basis \n")

    #Forward linear transformation (Avoiding of numerical problem):
    MeanOfData=np.mean(Data)
    VarOfData=np.var(Data)
    Data=Data/MeanOfData

    #--------------------------------------------------------------------------
    #----- Moments compuatation
    #--------------------------------------------------------------------------
    #Moments Computation for Input Data
    M=[]
    for x in range(2*dd+1):
        c=sum(Data **x)/len(Data) # Raw Moments
        M.append(c)

    #--------------------------------------------------------------------------
    #----- Main Loop for Polynomial with degree up to dd
    #--------------------------------------------------------------------------
    PolyCoeff_NonNorm=np.empty((0,1))
    Polynomial=np.zeros((dd+1,dd+1))
    for degree in range(dd+1):
        #print "degree:",degree
        Mm=np.empty((degree+1,degree+1))
        Vc=np.empty((degree+1))
        #PolyCoeff_NonNorm=np.zeros((degree+1,degree+1))

        #Defenition of Moments Matrix Mm
        for i in range(degree+1):
            for j in range(degree+1):
                if (i<degree):
                    #print "i,j,C1:",M[i+j+1]
                    Mm[i,j]=M[i+j]

                elif (i==degree) and (j<degree):
                    #print "i,j,C2:",0
                    Mm[i,j]=0

                elif (i==degree) and (j==degree):
                    #print "i,j,C1:",1
                    Mm[i,j]=1


            #Numerical Optimization for Matrix Solver
            Mm[i,:]=Mm[i,:]/max(abs(Mm[i,:]))

        #Defenition of Right Hand side ortogonality conditions: Vc
        for i in range(degree+1):

            if (i<degree):
                Vc[i]=0

            elif (i==degree):
                Vc[i]=1

        #print "Mm=" , Mm
        #print Vc

        #Solution: Coefficients of Non-Normal Orthogonal Polynomial: Vp Eq.(4)
        inv_Mm = np.linalg.pinv(Mm)
        Vp = np.linalg.solve(Mm, Vc)
        #print "Vp=",Vp
        #PolyCoeff_NonNorm[degree,0:degree]=Vp #PolyCoeff_NonNorm(degree+1,1:degree+1)=Vp'
        if degree == 0:
            PolyCoeff_NonNorm=np.append(PolyCoeff_NonNorm,Vp)

        if degree !=0:
            #PolyCoeff_NonNorm = np.append(PolyCoeff_NonNorm,[0])
            #Vp=np.append(Vp,[0])
            if degree == 1:
                zero=[0]
            else:
                zero=np.zeros((degree,1))
            PolyCoeff_NonNorm = np.hstack((PolyCoeff_NonNorm , zero))

            PolyCoeff_NonNorm=np.vstack((PolyCoeff_NonNorm, Vp))

        #print "PolyCoeff_NonNorm:\n",PolyCoeff_NonNorm

        if 100*abs(sum(abs(np.dot(Mm,Vp)) - abs(Vc))) > 0.5: #PolyCoeff_NonNorm
           print('\n---> Attention: Computational Error too high !')
           print('\n---> Problem: Convergence of Linear Solver')

        # Original Numerical Normalization of Coefficients with Norm and Ortho-normal Basis computation
        #Matrix Storrage Notice: Polynomial(i,j) correspont to coefficient number "j-1" of polinomil degree "i-1"
        P_norm=0
        #Polynomial=np.zeros((degree+1,degree+1))

        for i in range(NumberOfDataPoints):
            Poly=0
            for k in range(degree+1):
                if degree == 0:
                    Poly = Poly + PolyCoeff_NonNorm[k] * (Data[i]**k)
                else:
                    Poly = Poly + PolyCoeff_NonNorm[degree,k] *( Data[i]**k )

            P_norm = P_norm + Poly**2 / NumberOfDataPoints

        P_norm=np.sqrt(P_norm)

        for k in range(degree+1):
            if degree == 0:

                Polynomial[degree,k] = PolyCoeff_NonNorm[k]/P_norm
            else:
                Polynomial[degree,k]=PolyCoeff_NonNorm[degree,k]/P_norm


    #Backward linear transformation to the real data space
    Data=Data * MeanOfData
    for k in range(0,len(Polynomial)):
        Polynomial[:,k] = Polynomial[:,k]/(MeanOfData**(k))

    #print "Polynomial:\n", Polynomial


    #----------------------------------------------------------------------------------------------
    #----- Roots of polinomial and Plot
    #----------------------------------------------------------------------------------------------

    #Roots of polynomial with degree dd=d+1
    Roots=np.roots(Polynomial[dd,:][::-1])
    #print "Roots:", Roots
    # Create a directory
    newpath = (r'Polynomial_Roots')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    # Save Polynomials and Roots
    np.savetxt(Poly_FileName, Polynomial, delimiter=',')
    np.savetxt(Roots_FileName, Roots, delimiter=',')
    os.chdir("..")

####### __name__ == "__main__" #############

#if __name__ == "__main__":
#
#    Data=np.random.lognormal(0.5, 1.0, 1000)
#    Degree = 1
#
#    Poly_FileName="aaa"
#
#    aPoly_Construction(Data, Degree, Poly_FileName)








