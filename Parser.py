#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

@author: Farid Mohammadi, M.Sc.
Email: Farid.mohammadi@iws.uni-stuttgart.de

Created on Fri Nov 23 2018

"""


import sys
try:
    import ConfigParser as configparser
except ImportError:  # Python 3
    import configparser
    
import numpy as np
import ast

def Parser(config):
    
    #-------------------------------------------------
    #------------ [Model Specifications] -------------
    #-------------------------------------------------
    config.ModelName = config.get('Model Specifications', 'ModelName').split(' #')[0]
    config.MainDir = config.get('Model Specifications', 'MainDir').split(' #')[0]
    
    # Name of the headers in output csv files
    #config.nameList = config.get('Model Specifications', 'nameList')
    config.nameList = list(ast.literal_eval(config.get('Model Specifications', 'nameList').split(' #')[0]))

    config.Factors = ast.literal_eval(config.get('Model Specifications', 'Factors').split(' #')[0])
                                      
    config.ShellKeywords = list(ast.literal_eval(config.get('Model Specifications', 'ShellKeywords').split(' #')[0]))
    
    # Calibration
    config.ShellFile_Calib = config.get('Model Specifications',"ShellFile_Calib").split(' #')[0]
    config.InputFile_Calib = config.get('Model Specifications',"InputFile_Calib").split(' #')[0]
    config.MeasurementFile_Calib = config.get('Model Specifications',"MeasurementFile_Calib").split(' #')[0]
                                        
    # Validation
    config.ShellFile_Valid = config.get('Model Specifications',"ShellFile_Valid").split(' #')[0]
    config.InputFile_Valid = config.get('Model Specifications',"InputFile_Valid").split(' #')[0]
    config.MeasurementFile_Valid = config.get('Model Specifications',"MeasurementFile_Valid").split(' #')[0]
    
    #-------------------------------------------------                                        
    #---------------- [Model Outputs] ----------------
    #-------------------------------------------------
    config.NofVar = ast.literal_eval(config.get('Model Outputs', 'NofVar').split(' #')[0])
    
    config.Variants = np.asarray(ast.literal_eval(config.get('Model Outputs', 'Variants').split(' #')[0]))
    
    #-------------------------------------------------                                              
    #------------ [Uncertain Parameters] -------------
    #-------------------------------------------------
    config.NofPa = int(config.get('Uncertain Parameters',"NofPa").split("#")[0])
    
    config.ParameterNames = list(ast.literal_eval(config.get('Uncertain Parameters',"ParameterNames").split(' #')[0]))

    config.Parameters = []
    for idx in range(1,config.NofPa+1):
        pMin, pMax = [float(string) for string in [item for item in config.get('Uncertain Parameters','p'+str(idx)+'Min, p'+str(idx)+'Max').split(" #")[0].strip().split(',')]]
        config.Parameters.append([pMin,pMax])
    
    
    config.Distributions = list(ast.literal_eval(config.get('Uncertain Parameters','Distributions').split(" #")[0]))
    
    # Define the model inputs
    config.problem = {
        'num_vars': config.NofPa,
        'names': config.ParameterNames,
        'bounds': config.Parameters,
        'Dists': config.Distributions
    }
    
    #-------------------------------------------------
    #------------------ [AaPC] -----------------------
    #-------------------------------------------------
    config.d = int(config.get('AaPC','d').split(' #')[0])
    config.NofItr = int(config.get( 'AaPC','NofItr').split(' #')[0])
    config.MCsize =  int(config.get( 'AaPC','MCsize').split(' #')[0])
    
    config.CalibTimeStep = int(config.get( 'AaPC','CalibTimeStep').split(' #')[0])
    config.ValidTimeStep = int(config.get( 'AaPC','ValidTimeStep').split(' #')[0])
    
    config.MeasurementError = ast.literal_eval(config.get( 'AaPC','MeasurementError').split(' #')[0])
#    config.ModelError = ast.literal_eval(config.get( 'AaPC','ModelError').split(' #')[0])
#    
                                         

    return config
    

#if __name__ == '__main__':
#    config = configparser.ConfigParser()
#    try:
#        SysArg = sys.argv[1]
#    except:
#        print("No input file given.")
#        
#    config.read(SysArg)
#    
#    MyClass = Parser(config)
#    
#    print("My class:\n", MyClass.CalibTimeStep)
    