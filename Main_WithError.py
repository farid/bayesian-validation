# -*- coding: utf-8 -*-
"""
Bayesian Validation Framework

DESCRIPTION:
This script runs Bayesian valiation for computatopnal models.
Creates response surfaces and updates in a Bayesian Updating step. 
The model reduction is carried out with an Adaptive arbitrary Polynomial Chaos Expansion (AaPCE).
MC simulation is run using the updated reduced model.
The model is stochastically calibrated using the experimental data set
and the resulting posterior of the framework will be used as prior distribution for the validation step 
using a data set, which was not used in the calibration step.


+++++++++++++++++++ Response Surface +++++++++++++++++++
The response surface will be built using RS_Calculation_aPC function. 
This function also considers the model error as an additional unknown parameter
and tries to emulate the original model's responses. 
For each SRQ, the response surface is constructed and updated using the Bayesian updating. 
To select the new term in the RS, we sparsely select the polynomial degree using a Bayesian 
Model Selection (BMS) approach.


+++++++++++++++++++ BME Calculation +++++++++++++++++++
The BME calculation for the validation metric as well as to select the new Polynomial degree 
with the help of BMS is done by taking arithmatic average over the likelihood in the entire 
parameter space defined as prior distributions for all input parameters.

Measurement Error shall be set with caution.
@author: Farid Mohammadi, M.Sc.
Created on Wed Apr 04 2018
"""
# To supress warnings
import warnings
warnings.filterwarnings("ignore")
#import Uniform_dis_pars
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.size'] = 14
#matplotlib.rcParams['font.family'] = 'serif' #'Calibri'
import aPC
import sys
import Parser
try:
    import ConfigParser as configparser
except ImportError:  # Python 3
    import configparser
#import Optimal_integration_points_5pa
import numpy as np
from scipy.linalg import lu
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import matplotlib.ticker as ticker
import Model_Run_exp
import os
import multiprocessing
#import shutil
from operator import is_not
from functools import partial
from scipy import stats
import scipy as sp
from scipy.stats import norm
import math
#from sklearn.preprocessing import normalize
from tqdm import tqdm
import timeit
import time
from datetime import timedelta
tic=timeit.default_timer()
#------------------------------------------------------------------------------
def cutout(seq, idx):
    """
    Remove element at `idx` from `seq`.
    """
    return np.vstack((seq[:idx] , seq[idx + 1:]))

#------------------------------------------------------------------------------
def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays to get all combination.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out
#------------------------------------------------------------------------------
def FilePath(InputFile):
    return os.path.abspath(InputFile)
#------------------------------------------------------------------------------
def UpdateResultsName(File_path, lookup, nValue ):
    """Replacement of tel.res and sis.res names"""
    File_in=open(File_path,'r')
    filedata=File_in.readlines()
    for j in range(len(lookup)):
        for line in filedata:
                if lookup[j] in line:
                    oLine=line
                    i=filedata.index(oLine)
                    filedata[i]=lookup[j] +'\t = %s \n' %nValue[j]
        File_out=open(File_path,'w')
        File_out.writelines(filedata)
        File_out.close()
        File_in.close()
    return
#------------------------------------------------------------------------------
def NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos, NofMeasurement,NofItr):
#    if NofItr == 1:
#        NewPolynomialDegrees = np.zeros((NofVar,NofMeasurement,len(PolynomialDegrees)+NofItr,NofPa)) #[[] for i in range(NofMeasurement)]
    NewPolynomialDegrees = [[[] for i in range(NofMeasurement)] for j in range(Inputs.NofVar)]

    for VarIdx in range(Inputs.NofVar):
        for itr in range(NofItr):
            for idx in range(NofMeasurement):
                if itr ==0:
                    NewPolynomialDegrees[VarIdx][idx]=np.vstack((PolynomialDegrees,NewCombos[itr][VarIdx,idx])) #[itr][idx]
                else:
                    NewPolynomialDegrees[VarIdx][idx]=np.vstack((NewPolynomialDegrees[VarIdx][idx],NewCombos[itr][VarIdx,idx]))
    print "NewPolynomialDegrees:\n", NewPolynomialDegrees
    return NewPolynomialDegrees
#------------------------------------------------------------------------------
def solving_determined_system(array1, array2):
    """
    This fuction solves the determined system of equations.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    Z=np.linalg.lstsq(a, b, rcond=-1)
    R=Z[0]

    return R

#------------------------------------------------------------------------------
def solving_overdetermined_system(array1, array2):
    """
    This fuction solves the overdetermined system of equations using Least Square Method.
    Parameters
    ----------
    arrays1 : list of array-like
    Selected parameter sets

    arrays2 : list of array-like
    The outputs of fuction

    Returns
    -------
    out : ndarray
    1D array, the coefficients' matrix
    """
    # Here response surface will be computed using least Square Method:
    z=np.ones((len(array1), 1))
    a=np.hstack((z, array1))

    b=np.transpose([array2])

    R=np.linalg.lstsq(a,b)[0]

    return R

#------------------------------------------------------------------------------
def RS_calculation_aPC(Inputs,ItrNr, Model_Output, OptimalCollocationPointsBase, PolynomialDegrees, PolynomialBasisFileName, NofMeasurements):
    """
    calculates the response surface for each measurement location
    So each row of the RSTotal matrix is the coefficients of
    one response surface for a specific measurement location

    NofMeasurements: Number of measurement locations
    NofPa: Number of sensitive parameters
    k= will be the output of the TELEMAC runs for each location

    attributes: array1, system_type, NofMeasurements
    """
    P = math.factorial(Inputs.NofPa+Inputs.d)/(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d)) #Total number of terms
#    RSTotal = np.zeros((NofVar, P+ItrNr , NofMeasurements))
    RSTotal = [[] for i in range(Inputs.NofVar)]
#    Psi_RS = np.zeros((NofVar, P+ItrNr , P+ItrNr))
    PsiRS = [[] for i in range(Inputs.NofVar)]
    #NewCombos = np.zeros((NofVar, NofMeasurements, NofPa),dtype=int) #[[]for i in range(NofMeasurements)]
    
    #----------------------------------------------------
    # Calculate RSCoeffs & NewCombos for datasets(variants)
    #----------------------------------------------------
    for VarIdx in range(Inputs.NofVar):
        
        # Handle the first RS calculation
        if ItrNr !=0: 
            NewPolynomialDegrees=PolynomialDegrees[VarIdx] #[i] This will be used when each measurement point has its own Polynomial Degrees
        else:        
            NewPolynomialDegrees=PolynomialDegrees
        
        RSTotal[VarIdx], PsiRS[VarIdx] = aPC.RS_Builder(Inputs,ItrNr, Model_Output[VarIdx], OptimalCollocationPointsBase, NewPolynomialDegrees, PolynomialBasisFileName)
        
    #return RSTotal, np.array(PsiRS, dtype=object)
    return RSTotal, PsiRS

#------------------------------------------------------------------------------
def PriorLikelihoods(Inputs, ItrNr, Psi_W_old, Parameterset, RSTotal, Observations, NofMeasurements, PolynomialBasisFileName, PolynomialDegrees):
    """
    This function calculates the likelihoods of the each parameter set based on the difference of
    its surrogate outputs and the measurements.
    
    RSTotal = Coeffs
    Parameterset = all the parameter sets drawn from the prior
    Observations = measurement data set
    """
    P = math.factorial(Inputs.NofPa+Inputs.d)/(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d)) #Total number of terms
    
    
    ParametersetSize = Parameterset.shape[0]
    Weights=np.zeros((ParametersetSize, Inputs.NofVar)) 
    ObservationSize = Observations.shape[1]
    OutputRS=np.empty((0, NofMeasurements))
    #Psi_W = np.zeros((NofVar, ParametersetSize , P+ItrNr))
    Psi_W = [[] for i in range(Inputs.NofVar)]
    
    #Loop over the Outputs (Data Sets)
    for VarIdx in range(Inputs.NofVar):
        # Create Psi Matrix
        #start = time.time()
        if ItrNr == 0:
            Psi_W[VarIdx]=aPC.Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d,Parameterset, PolynomialBasisFileName,PolynomialDegrees)
        else:
            if Psi_W_old is None:
                Psi_W_Old =None
            else:
                Psi_W_Old=Psi_W_old[VarIdx]
                
            Psi_W[VarIdx]=aPC.Psi_Creator_Weight(Inputs.MainDir,Inputs.NofPa,Inputs.d,Psi_W_Old,Parameterset, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        #end = time.time()
        #print ("Elapsed time after Psi:", end - start)
        
        #Calculation of RS outputs
        OutputRS = np.dot(Psi_W[VarIdx], RSTotal[VarIdx])
        
        # Calculate mean and standard deviation
        #mean_OutputRS=np.mean(OutputRS, axis=0)
        #stdev_OutputRS=np.std(OutputRS, axis=0)
        
        # Co-Variance matrix R of the Gaussian distribution 
        Covariance = np.zeros((NofMeasurements, NofMeasurements), float)
        np.fill_diagonal(Covariance,  Inputs.MeasurementError[VarIdx]**2)
        
        # Deviation of the RS outputs from the measurements
        if ObservationSize != ParametersetSize:
            Deviation=np.zeros((ParametersetSize,NofMeasurements)) 
    
            for i in range(ParametersetSize):
                Deviation[i] = Observations[VarIdx+1] - OutputRS[i, :]
        
        else: # for calculation of Weight_RM
            Deviation = Observations[VarIdx+1] - OutputRS
            
        # Computation of Weight according to the Deviation      
        for i in range(ParametersetSize):
            Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(Covariance)), Deviation[i,:][:,None])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(Covariance)))
    
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    Weights = np.prod( Weights,axis=1)
    
    return Weights[:,None], Psi_W

#------------------------------------------------------------------------------
def BME_Corr_Weight(Inputs,ItrNr,Parametersets, RSTotal, OutputOrig, Observations, NofMeasurements,PolynomialBasisFileName,PolynomialDegrees):
    """
    Calculates the correction factor for BMEs.
    """
    #P = math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms
    ParametersetSize = Parametersets.shape[0]
    OutputRS = np.empty((0, NofMeasurements))
    BME_RM_ModelWeight = np.zeros((ParametersetSize, Inputs.NofVar)) 
    BME_RM_DataWeight = np.zeros((ParametersetSize, Inputs.NofVar)) 
    BME_Corr = np.zeros((1,Inputs.NofVar))
    #z=np.ones((ParametersetSize, 1))
    #a=np.hstack((z, Parameterset))
    
    #Scaling Factor
    Normalization=10**(100) # ???


    for VarIdx in tqdm(range(Inputs.NofVar), ascii=True, desc='Computing Correction Factor for BMEs'):
        # Create Psi Matrix
        
        if ItrNr == 0:
            NewPolynomialDegree = PolynomialDegrees
        else:
            NewPolynomialDegree = PolynomialDegrees[VarIdx]
        
        #Psi = aPC_Sensitivity.Psi_Creator_Weight(MainDir,N,d,newItrNr,None,Parametersets, PolynomialBasisFileName,NewPolynomialDegree,NofMeasurements)
        Psi = aPC.Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d,Parametersets, PolynomialBasisFileName,NewPolynomialDegree)
        
        #Calculation of the outputs of Response surfaces
        OutputRS = np.dot(Psi, RSTotal[VarIdx])
        
        # Deviation Computations
        RM_Model_Deviation = np.zeros((ParametersetSize,NofMeasurements)) 
        RM_Data_Deviation = np.zeros((ParametersetSize,NofMeasurements)) 
        for i in range(ParametersetSize):
            RM_Model_Deviation[i] = OutputOrig[VarIdx,i][:NofMeasurements] - OutputRS[i, :] # Reduce model- Full Model
            RM_Data_Deviation[i] = Observations[VarIdx+1] - OutputRS[i, :] # Reduce model- Measurement Data
        
        # Initialization  of Co-Variance Matrix
        # For BME_RM_ModelWeight
        if NofMeasurements == 1:
            RM_Model_Error = np.zeros((NofMeasurements, NofMeasurements), float)
            np.fill_diagonal(RM_Model_Error, np.cov(RM_Model_Deviation.T))
        else: 
            RM_Model_Error = np.cov(RM_Model_Deviation.T)
        
        # For BME_RM_DataWeight
        Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
        np.fill_diagonal(Measurement_Error, Inputs.MeasurementError[VarIdx]**2)
 
        
        # Computation of Weight according to the deviations
        for i in range(ParametersetSize):
            #Weights[i,VarIdx]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(CovarianceMatrix)), Deviation[i,:])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(CovarianceMatrix)))
 
            BME_RM_ModelWeight[i,VarIdx] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Model_Deviation[i], np.linalg.pinv(RM_Model_Error)), RM_Model_Deviation[i]))
            BME_RM_DataWeight[i,VarIdx] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Data_Deviation[i], np.linalg.pinv(Measurement_Error)), RM_Data_Deviation[i]))

        BME_Corr[0,VarIdx] = 0
        for i in range(ParametersetSize):
            BME_Corr[0,VarIdx] += BME_RM_ModelWeight[i,VarIdx] * BME_RM_DataWeight[i,VarIdx] / np.sum(BME_RM_DataWeight[:,VarIdx])
    
    # Multivariate Gaussian Likelihood (Assump: Independent Data Sets)
    BME_Corr_Factor = np.prod(BME_Corr,axis=1)

    return BME_Corr_Factor


#------------------------------------------------------------------------------
def filtration_prior(Inputs, Parameterset, Weights, NrofParametersets):
    """Rejection Sampling: filtration of prior distribution via uniform"""
    ii=0
    unif=np.random.rand(1, NrofParametersets)
    Parameterset_weight=np.hstack((Parameterset, Weights))
    Post_MC_Vector=np.empty((0, Inputs.NofPa+1))
    for i in range(len(Weights)):
        if Weights[i, :]/np.max(Weights) > unif[:, i]:
            ii += 1
            Post_Vector = Parameterset_weight[i, :]
            Post_MC_Vector=np.vstack((Post_MC_Vector, Post_Vector))
    
    return Post_MC_Vector[:, 0:Inputs.NofPa], Post_MC_Vector[:,Inputs.NofPa][:,None]

#------------------------------------------------------------------------------
def Mean_Stdev(MainDir,N,d,NofVar,ItrNr,Psi_W,Parametersets, RSTotal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements):
    """
    Calculates the mean and standard deviation of the SQRs using the reduced model
    NofMeasurements: Number of the measurements 
    """    
    P = math.factorial(Inputs.NofPa+d)/(math.factorial(Inputs.NofPa) * math.factorial(d)) #Total number of terms

    #Psi,NewTermCombo  = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parametersets,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    mean_OutputRS = [[] for i in range(NofVar)]
    stdev_OutputRS = [[] for i in range(NofVar)]
    for VarIdx in range(NofVar):
        itrNr = len(PolynomialDegrees[VarIdx]) - P
        Psi=aPC.Psi_Creator_Weight(MainDir,N,d,Psi_W,Parametersets, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        
        # Calculate the respose surface's output
        OutputRS = np.dot(Psi, RSTotal[VarIdx])
    
        #------- Calculation of mean and Stdev of the outputs of Response surfaces----------------
        mean_OutputRS[VarIdx]=np.mean(OutputRS, axis=0)
        stdev_OutputRS[VarIdx]=np.std(OutputRS, axis=0)

    return mean_OutputRS, stdev_OutputRS

#------------------------------------------------------------------------------
def plot_weights(Parameterset, Weights,Inputs, Name,RSTotal,Nr_Iteration):
    """
    This Function plots each parameter versus its weight

    """
    #print "plot_weights, Parameterset:\n", Parameterset.shape
    #print "plot_weights, Weights:\n", Weights.shape
    fig = plt.figure()

    fig.set_figheight(12)
    fig.set_figwidth(20)

    Colors = ['b.','g.','r.','c.','m.', 'y.', 'k.']
    header1 = []
    header2 = []
    #header2 = ["c_0"]
    #for coeffs in range(len(RSTotal[0,:,0])):
    for coeffs in range(len(RSTotal[0][:,0])):
        header2.append('c_%s'%(coeffs)) #+1
        
    # Decide on shape of the figure
    if len(Inputs.ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
    
    
    # Loop over the paramters and plot their weights
    for i,ParameterName in enumerate(Inputs.ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-6) #(1, 2*i-5)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        ax1.plot(Parameterset[:, i], Weights, Colors[i])
        plt.xlim(Inputs.Parameters[i][0], Inputs.Parameters[i][1])
        ax1.xaxis.set_major_locator(ticker.AutoLocator())
        ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        header1.append('P_%s'%(i+1))
        #header2.append('c_%s'%(i+1))
        plt.xlabel('p$_%s$'%(i+1))
        plt.ylabel('Weights')
        plt.title(ParameterName)

    plt.tight_layout()
    
    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Outputs') 
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(Name +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save parameter sets with their weights
    np.savetxt(Name+'.txt', np.hstack((Parameterset, Weights)), delimiter=',', header= " ".join(header1)+ "   Weight")
    # Save Response surfaces in a text file
    for VarIdx in range(Inputs.NofVar):
        if Nr_Iteration != None:
            np.savetxt('RSTotal_Itr%s_Var%s.txt'%(Nr_Iteration,VarIdx+1), RSTotal[VarIdx].T, delimiter=',',  header=" ".join(header2))
    
    os.chdir('..')
    
#---------------------------------------------------------------------------
def plot_Histogram(Post_MC_Vector,Post_MC_Weights,Inputs, FigName):
    """
    This Function plots the histogram of posterior parameter sets.
    """
    fig = plt.figure()

    fig.set_figheight(12) #20
    fig.set_figwidth(20)
  
    header1 = []
    Colors = ['b','g','r','c','m', 'y', 'k']
    
    # Decide on shape of the figure
    if len(Inputs.ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
        
    # Loop over the paramters and plot their histograms        
    for i,ParameterName in enumerate(Inputs.ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-6)
            
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        x1=Post_MC_Vector[:, i]
        n, bins, patches = ax1.hist(x1, 5, density=True, facecolor=Colors[i], alpha=0.75)
        
        # ------ Fitting data -----------
        xs1 = np.linspace(Inputs.Parameters[i][0], Inputs.Parameters[i][1], 200)
        kde1 = stats.gaussian_kde(x1)
        ax1.plot(xs1, kde1(xs1), Colors[i]+'--', lw=2)
        
        ax1.set_xlabel('Values of p$_%s$'%(i+1))
        ax1.set_ylabel('Density')
        ax1.grid(True)
        ax1.xaxis.set_major_locator(ticker.AutoLocator())
        ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        anchored_text = AnchoredText("$\mu$=%.4g \n $\sigma$=%.4g \n $S$=%.4f \n $k$=%.4f" %(np.mean(x1), np.std(x1), stats.skew(x1), stats.kurtosis(x1)), loc=2)
        ax1.add_artist(anchored_text)
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        header1.append('P_%s'%(i+1))
        plt.title(ParameterName)

    #plt.suptitle('Probablity Density Function')

    plt.tight_layout()

    # ---------------- Saving the figure and text files -----------------------
    newpath = (r'Outputs')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    fig.savefig(FigName +'.png')   # save the figure to file
    plt.close(fig)
    
    # Save POSTERIOR parameter sets with their weights
    np.savetxt(FigName+'.txt', np.hstack((Post_MC_Vector, Post_MC_Weights)), delimiter=',',header= " ".join(header1)+ "   Weight")

    os.chdir('..')

#-------------------------------------------------------------------------------------------------------
def CorrectedLeaveoutError(TotalPsi, Coeffs, TotalModelOutputs, NofMeasurements, Variants):
    """
    This is based on the following paper:
        ""Blatman, G., & Sudret, B. (2011). Adaptive sparse polynomial chaos expansion based on least angle regression. 
        Journal of Computational Physics, 230(6), 2345-2367.""
    Psi: the orthogonal polynomials of the response surface
    """
    h_i = []
    Q2 = [[] for i in range(len(Variants))]
    

    for VarIdx in range(len(Variants)):
        Psi = np.array(TotalPsi[VarIdx], dtype = float)
        P = Psi.shape[1]  # NrTerm of aPCEs 
        N = Psi.shape[0]  # NrEvaluation
        ModelOutputs = TotalModelOutputs[VarIdx]
        
        
        # Build the projection matrix
        M = np.dot(Psi.T, Psi)
        
        #ProjectionMatrix = np.identity(N) - np.dot(np.dot(Psi,np.linalg.inv(M)),Psi.T)# Wrong Formula for projection matrix (residual operator)
        ProjectionMatrix = np.dot(np.dot(Psi,np.linalg.inv(M)),Psi.T)
        
        for i in range(N):
            h_i.append(ProjectionMatrix[i,i])
        
        # Loop over all measurement points and save the Q_2  
        for PointIdx in range(NofMeasurements):

            # Calculate Error Loocv for each measurement point
            PredictedResidual = np.zeros((N,1))
            for i in range(N):
                # Response surface coeffs when removing i-th observation (Coeffs correspoding to the experimental design)   
                inv_PsiPsi = np.linalg.pinv(np.dot(np.delete(Psi, (i), axis = 0).T, np.delete(Psi, (i), axis = 0)))    
                expCoeffs = np.dot(np.dot(inv_PsiPsi,np.delete(Psi, (i), axis = 0).T), np.delete(ModelOutputs[:,PointIdx], (i), axis = 0))
                
                # Calculate the perdicted residual without considering the ith dataset
                #PredictedResidual[i] =  (ModelOutputs[i,PointIdx] - np.dot(Coeffs[VarIdx,:,PointIdx], Psi[i].T)) / h_i[i]
                PredictedResidual[i] =  (ModelOutputs[i,PointIdx] - np.dot(Psi[i], expCoeffs ))/ h_i[i]
            
            ErrLoo = np.sum(np.square(PredictedResidual)) / N
            
            Var = np.var(ModelOutputs[:,PointIdx], ddof=1)
            
            # Corrected Error for over-determined system
#            if N != P:
##                Cemp = M // N
##                
##                T = (N/(N-P)) * (1 + np.trace(np.linalg.inv(Cemp))/(N))
#                
#                # Adjusted emperical error; Blatman (2010)
#                T = (N - 1) // (N - P - 1) 
#                
#                CorrectedErrLoo = ErrLoo * T
#                EpsilonLOO = CorrectedErrLoo / Var
#            else:
#                EpsilonLOO = ErrLoo / Var
            EpsilonLOO = ErrLoo / Var
            Q_2 = 1 - EpsilonLOO
            Q2[VarIdx].append(Q_2)
            
    print "This is the Q_2:\n", Q2
    return Q2

#-------------------------------------------------------------------------------------------------------
def Variant_plot(Inputs,ItrNr,Observations, Output, Parameterset, RSTotal,PolynomialBasisFileName,PolynomialDegrees,BestIdxs,NrofSparse):
    """ 
    This function plots the SQR's output from the full complexity model and its reduced version (aPCE).
    """
    # Calculate the SQR's output from the reduced version (aPCE)
    #Psi_Calib,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parameterset_Calib,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    P = math.factorial(Inputs.NofPa+Inputs.d)/(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d))
    #Nr_of_Sparse_Indexes=len(PolynomialDegrees[0])
    RSOutputTotal = np.zeros((len(Inputs.Variants),Inputs.CalibTimeStep))
    
    X_value = Observations[0]
   
            
    # Loop over all outputs (Variants)
    for VarIdx in range(len(Inputs.Variants)):
        
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
        
        # Plot the measurement; once for each variant
        ax.plot(X_value, Observations[VarIdx+1], "x", color='k' ,label='Measurements' , lw=2)
        # Plot the output from the original model; once for each variant
        #print "Original model Output %s:"%ItrNr, Output[VarIdx][:NofMeasurements]
        ax.plot(X_value, Output[VarIdx][:Inputs.CalibTimeStep], "o-", color="brown" ,label='Original model' , lw=2)
        
        
        # Plot the RSOutputs; once for each variant
        for sparse_index in range(NrofSparse):            
            if ItrNr ==1: # using initial PolynomialDegrees
                Psi = aPC.Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees)
            elif ItrNr !=1 and NrofSparse==1:
                Psi=aPC.Psi_Creator_Weight(Inputs.MainDir,Inputs.NofPa,Inputs.d,None,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees[VarIdx],Inputs.CalibTimeStep)
            else:
                Psi=aPC.Psi_Creator_Weight(Inputs.MainDir,Inputs.NofPa,Inputs.d,None,np.asarray([Parameterset]), PolynomialBasisFileName,PolynomialDegrees[VarIdx][sparse_index],Inputs.CalibTimeStep)
            
            # ----- Calculate RSoutput --------
            if ItrNr == 1 or NrofSparse == 1:
                RSOutputTotal[VarIdx] = np.dot(Psi, RSTotal[VarIdx])[0]
            else:
                RSOutputTotal[VarIdx]  = np.dot(Psi, RSTotal[VarIdx][sparse_index])#[0]
            
            #print "RSOutput:\n", RSOutput 
            # Plot the output from the surrogate model for all options of sparse polynimial degrees
            if ItrNr != 1 and NrofSparse != 1 and sparse_index == BestIdxs[VarIdx]:
                Color, LW ="g", 3
            else:
                 Color, LW ="navy", 1
            
            ax.plot(X_value, RSOutputTotal[VarIdx] , ".-",color=Color , label='Response surface '+str(sparse_index+1), lw=LW)
        
        
        # Axis labels
        plt.xlabel('Time [d]',fontsize=18)
        plt.ylabel(Inputs.Variants[VarIdx], fontsize=18)
        ax.legend(loc='best', fontsize=18)    
        
        ax.tick_params(axis = 'both', which = 'major', labelsize = 18)
        
        # Save the figure and the BMEs into a folder
        #newpath = (( ModelName + r'_Variant%s_Run_Calib') %(VarID+1))
        # save the figure to file
        newpath = (( Inputs.ModelName + r'_Run_%s') %(P+ItrNr))
        if not os.path.exists(newpath): os.makedirs(newpath)
        os.chdir(newpath)
        
        fig.savefig(Inputs.Variants[VarIdx]+'.png')   
        plt.close(fig)
        
        
        os.chdir("..")
    
    if ItrNr == 1 or NrofSparse == 1:
        # Loop over all outputs (Variants)
        for VarIdx in range(len(Inputs.Variants)):
            fig = plt.figure()
            fig.set_figheight(12) #20
            fig.set_figwidth(20)
            ax = plt.subplot(1,1,1)
            
            # Plot the output from the original model; once for each variant
            predictions = RSOutputTotal[VarIdx]
            NewOutput = Output[VarIdx][:Inputs.CalibTimeStep]
            
            # Create linear regression object
            from sklearn import metrics, linear_model
            regr = linear_model.LinearRegression()
            regr.fit(predictions.reshape(-1, 1) , NewOutput)
            
            # predict y from the data
            x_new = np.linspace(np.min(predictions), np.max(predictions), 100)
            y_new = regr.predict(x_new[:, np.newaxis])
            
            ax.scatter(predictions , NewOutput, edgecolors=(0, 0, 0))
            ax.plot(x_new, y_new, color = 'k')
            
            # Axis labels
            plt.xlabel(Inputs.Variants[VarIdx] +' by surrogate model',fontsize=18)
            plt.ylabel(Inputs.Variants[VarIdx] +' by original model', fontsize=18)
            
            
            R2 = metrics.r2_score(NewOutput, predictions)
            AdjR2 = 1-(1-R2)*(Inputs.CalibTimeStep-1)/(Inputs.CalibTimeStep-(P+ItrNr)-1)
            RMSE = metrics.mean_squared_error(NewOutput , predictions)

            plt.annotate('RMSE = '+ str(RMSE) + '\n' + 'Adjusted $R^2$ = '+ str(AdjR2), xy=(0.05, 0.85), xycoords='axes fraction')
                     
            #ax.legend(loc='best', fontsize=18)
            # save the figure to file
            newpath = (( Inputs.ModelName + r'_Run_%s') %(P+ItrNr))
            if not os.path.exists(newpath): os.makedirs(newpath)
            os.chdir(newpath)
            
            fig.savefig(Inputs.Variants[VarIdx]+'Prediction'+'.png')   
            plt.close(fig)
            
            os.chdir("..")
#-------------------------------------------------------------------------------------------------------


def Calib_Valid_plot(MainDir,N,d,Variants,ItrNr,Observations_calib, Output_calib, Parameterset_Calib, RSTotal, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements,PlotName):
    """ 
    This function plots the SQR's output from the full complexity model and its reduced version (aPCE).
    """
    P = math.factorial(N+d)/(math.factorial(N) * math.factorial(d))
    # Calculate the SQR's output from the reduced version (aPCE)
    #Psi_Calib,NewTermCombo = aPC.Psi_Creater(MainDir,N,d,ItrNr, Parameterset_Calib,RSTotal,PolynomialBasisFileName,PolynomialDegrees)
    # Loop over all outputs (Variants)
    for VarIdx in range(len(Variants)):
        
        Psi_Calib=aPC.Psi_Creator_Weight(MainDir,N,d,None,Parameterset_Calib, PolynomialBasisFileName,PolynomialDegrees[VarIdx],NofMeasurements)
        #print "Psi_Calib:\n", Psi_Calib
        RSOutput_calib = np.dot(Psi_Calib, RSTotal[VarIdx])[0]
        #print "RSOutput_calib:\n", RSOutput_calib 
        X_value = Observations_calib[0] #range(len(RSOutput_calib))
        #print "Observations_calib:\n", Observations_calib
        
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
    
        # Plot the measurement
        ax.plot(X_value, Observations_calib[VarIdx+1], "x", color='k' ,label='Measurements' , lw=2)
        # Plot the output from the original model
        ax.plot(X_value, Output_calib[VarIdx+1][:NofMeasurements], "o-", color="brown" ,label='Original model' , lw=2)
        # Plot the output from the surrogate model
        ax.plot(X_value, RSOutput_calib, ".-",color="navy" , label='Response surface', lw=2)
        # Plot the mean of the surrogate model
        ax.plot(X_value, mean_RS_Cal[VarIdx], "1-",color="green" , label='RS Mean', lw=2)
        
        # Plot confidence interval of reduced model
        Ub=mean_RS_Cal[VarIdx]+2*stdev_RS_Cal[VarIdx]
        Lb=mean_RS_Cal[VarIdx]-2*stdev_RS_Cal[VarIdx]
        ax.fill_between(X_value, Ub, Lb,color="gray", alpha=.5)
        
        # Patch the confidence interval label
        extraString = "$\mu \pm 2\sigma$"
        handles, labels = ax.get_legend_handles_labels()
        handles.append(mpatches.Patch(color='gray', label=extraString))
        ax.legend(handles=handles, loc='best')
        
        
        # Axis labels
        plt.xlabel('Time [d]')
        plt.ylabel(Variants[VarIdx])
        
        
        # Create a directory to save the figure to file
        newpath = (Inputs.ModelName + '_' + PlotName + '_Outputs')
        if not os.path.exists(newpath): os.makedirs(newpath)
        os.chdir(newpath)
        
        # save the figure to file        
        fig.savefig(Variants[VarIdx]+ '_' + PlotName +'.png')   
        plt.close(fig)
        
        # -------------- Evaluation of surrogate model predictions ------------
        fig = plt.figure()
        fig.set_figheight(12) #20
        fig.set_figwidth(20)
        ax = plt.subplot(1,1,1)
        
        predictions = RSOutput_calib
        NewOutput = Output_calib[VarIdx+1][:NofMeasurements]
        
        # Create linear regression object
        from sklearn import metrics, linear_model
        regr = linear_model.LinearRegression()
        regr.fit(predictions.reshape(-1, 1) , NewOutput)
        
        # predict y from the data
        x_new = np.linspace(np.min(predictions), np.max(predictions), 100)
        y_new = regr.predict(x_new[:, np.newaxis])
        
        ax.scatter(predictions , NewOutput, edgecolors=(0, 0, 0))
        ax.plot(x_new, y_new, color = 'k')
        
        # Axis labels
        plt.xlabel(Variants[VarIdx] +' by surrogate model',fontsize=18)
        plt.ylabel(Variants[VarIdx] +' by original model', fontsize=18)
        
        
        R2 = metrics.r2_score(NewOutput, predictions)
        AdjR2 = 1-(1-R2)*(NofMeasurements-1)/(NofMeasurements-(P+ItrNr)-1)
        RMSE = metrics.mean_squared_error(NewOutput , predictions)

        plt.annotate('RMSE = '+ str(RMSE) + '\n' + 'Adjusted $R^2$ = '+ str(AdjR2), xy=(0.05, 0.85), xycoords='axes fraction')
        
        fig.savefig(Variants[VarIdx]+'Prediction'+ '_' + PlotName +'.png')   
        
        plt.close(fig)
        
        os.chdir("..")
        
#-------------------------------------------------------------------------------------------------------
def Plot_BME(BayesianEvidenceMatrix,logBayesian_EvidenceMatrix, Nr_Iteration,Variants):
    fig = plt.figure()
    N_Var=len(logBayesian_EvidenceMatrix)
    Styles= ['b-','g-','r-','c-','m-','y-']
    Markers = ['o','*','+','s','D','p']
    
    # Plot logBayesian_EvidenceMatrix for all variants
    for NofV in range(N_Var):
        plt.plot(Nr_Iteration, logBayesian_EvidenceMatrix[NofV], Styles[NofV], marker=Markers[NofV], label=Variants[NofV], lw=2)
    
    plt.xlim(np.min(Nr_Iteration), np.max(Nr_Iteration))
    plt.xlabel('Number of Iterations')
    plt.ylabel('log$_{10}$(BME)')
    plt.ylim(np.min(logBayesian_EvidenceMatrix)-1, np.max(logBayesian_EvidenceMatrix)+1)
    plt.title('Bayesian Model Evidence vs. Iterations')
    plt.legend(loc='best')
    plt.grid()
    
    # Save the figure and the BMEs into a folder
    newpath = ((r'Output_BME'))
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    # save the figure to file
    fig.savefig('Log_BME_vs_Itr.png')   
    plt.close(fig)

    # save BayesianEvidenceMatrix & logBayesian_EvidenceMatrix in a text file
    np.savetxt('BME.txt', BayesianEvidenceMatrix, delimiter=',')
    np.savetxt('Log_BME.txt', logBayesian_EvidenceMatrix, delimiter=',')

    os.chdir('..')

#-------------------------------------------------------------------------------------
#def resampling(x, P_range, MCsize):
def resampling(x, MCsize):
    size=MCsize / (4*len(x))
    resampled=np.empty((size,0))
    for i in range(len(x[1,:])):
        kde = stats.gaussian_kde(x[:, i])
        sample = kde.resample(size)
        #filtered_sample = sample[(sample >= P_range[i, 0]) & (sample <= P_range[i, 1])]
        filtered_sample = sample[(sample >= np.min(x[:, i])) & (sample <= np.max(x[:, i]))]
       # print filtered_sample.size
        while True:
             len(filtered_sample) < size
             kde_new = stats.gaussian_kde(filtered_sample)
             size_new=size-len(filtered_sample)
             sample_new=kde_new.resample(size_new)
             filtered_sample=np.append(filtered_sample,sample_new)
          #   filtered_sample = sample_new[(sample_new >= P_range[i, 0]) & (sample_new <= P_range[i, 1])]
             if len(filtered_sample) == size:
                 break

        resampled=np.hstack((resampled,filtered_sample[:, None]))
        #resampled=np.hstack((resampled,sample.T))
    return resampled

#-------------------------------------------------------------------------------------
def selection_max_weight(Inputs,Parameterset, CollocationPoints, Weight, NewOutput, NofMeasurements, NoItr, m):
        """ This function does the following operations:
        1) Computes the weights of the optimal integration points in each step
        2) Sorts the parametersets based on the weights calculated in Step1
        3) Computes the weights of the parameter sets
        4) Selects the parameterset with the best weight
        5) Checks if already exist in the optimal integration points (CollocationPoints)
        6) Run the model for the new collocation point

        Output:
        Updated integration points for solving an overdetermined system of equations
        """

        print 80 * '-'
        print('---> Selection of the Parameter Set with Maximum Weight \n')
        
        Parametersets_weight = np.hstack((Parameterset, Weight))
        sorted_Parametersets_weight = Parametersets_weight[np.argsort(Parametersets_weight[:, Inputs.NofPa])][::-1] # NofPa+2: Sort based on the weights
        
        # Check if the parameter set already exists in the collocation points.
        i = 0
        while i < len(Parameterset):
            NewCollocationPoint = sorted_Parametersets_weight[i, 0:Inputs.NofPa]
            
            # If doesn't exist, break; else take the next one. 
            if any(item == False for item in np.isin(NewCollocationPoint , CollocationPoints)):
                break
            else:
                print '-' * 50
                print 'The previous collocation point is selected again!!'
                print 'Do not panic! We can try to select the next parameter set as new collocation point'
                print '-' * 50
                NewCollocationPoint=sorted_Parametersets_weight[i, 0:Inputs.NofPa]
                i += 1
            

        print '-' * 50
        print "NewCollocationPoint:", NewCollocationPoint
        print '-' * 50
        
        O_new=np.vstack((CollocationPoints, NewCollocationPoint))
        
        Output=Model_Run_exp.ModelRun(Inputs.MainDir,Inputs.ModelName,Inputs.ShellFile_Calib,Inputs.ShellKeywords,Inputs.InputFile_Calib,OutputFile[m],Inputs.nameList,Inputs.Factors,Inputs.ParameterNames,NewCollocationPoint,m,NofMeasurements)
        NewTotalOutput=np.zeros((Inputs.NofVar,len(O_new),NofMeasurements))
        for VarIdx in range(Inputs.NofVar):

            #Output = Output[[VarIdx+1]] # Takes the second column from the output matrix
            
            #if NoItr == 1:
            #    NewTotalOutput[VarIdx,:,:]=np.vstack((TotalOutput[VarIdx] , Output[VarIdx+1]))
            #else:
                NewTotalOutput[VarIdx,:,:]=np.vstack((NewOutput[VarIdx] , Output[VarIdx+1]))
        
        
        return O_new, NewTotalOutput


#-------------------------------------------------------------------------------------
def Modelrun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int ,m,TotalTimeStep, out_q):
    Output = Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,ShellKeywords,InputFile,OutputFile,nameList,Factors,ParameterNames,O_int[m],m,TotalTimeStep)   #Variants[index] = defines each model alternative
    out_q.put(Output)
    return out_q

#-------------------------------------------------------------------------------------
def RS_ST(Inputs,OutputFile): #out_q1,index,NofPa
    """
    This function is the main function. 
    It runs the model with the help of the Model_Run_exp script and creates the response surfaces and does the Bayesian updating. 
    """
    Bayesian_Evidence=np.empty((0, 1))
    TotalTimeStep = Inputs.CalibTimeStep + Inputs.ValidTimeStep
    
#    CovarianceMatrix = [sum(x) for x in zip(ModelError,MeasurementError)]
    P=math.factorial(Inputs.NofPa+Inputs.d)/(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d)) #Total number of terms
    # Here, the distributions of the input parameters are imported from the :
    inputDistributions,OptimalCollocationPoints,PolynomialDegrees,PolynomialBasisFileName = aPC.CollocationPoints(Inputs.MainDir,Inputs.NofPa, Inputs.d,0, Inputs.MCsize,Inputs.Parameters,Inputs.Distributions)
    # Pick the collocatation points for Nofpa-1 and the corresponding PolynomialDegrees

#    OptimalCollocationPointsBase = OptimalCollocationPointsBase[1:,:NofPa]
#    PolynomialDegrees = PolynomialDegrees[1:,:NofPa]
#    PolynomialBasisFileName.remove('PolynomialBasis_d1_'+str(NofPa+1)+'.txt')
#    PolynomialBasisFileName.remove('PolynomialBasis_d2_'+str(NofPa+1)+'.txt')
    
#    print "PolynomialDegrees:\n", PolynomialDegrees
#    print "PolynomialBasisFileName:\n",PolynomialBasisFileName
    
    #Parametersets_dp1,OptimalCollocationPointsBase_dp1,PolynomialDegree_dp1,PolynomialBasisFileName_dp1 = aPC.CollocationPoints(MainDir,NofPa, d+1,0, MCsize,Parameters, Percentage)
    # Here, the combinations of points are imported to from Optimal_integration_points:
    # Collocation Points
    
    print ('The %s optimal collocation points are:\n' %(P)), OptimalCollocationPoints
    print '-' * 50
    
    #------------------------------------------------------------------------------
    # Here the input of the formulas are given:
    BayesianEvidenceMatrix=np.empty((0, Inputs.NofItr+1))  # This should be equal to the iteration number

    # Initialization of arrays to store  simulation outputs
#    TotalOutput_=[[] for i in range(NofVar)]
#    for Var_Nr in range(NofVar):
#        TotalOutput_[Var_Nr]=np.empty((0, TotalTimeStep))
    TotalOutput_=np.zeros((Inputs.NofVar,P,TotalTimeStep))
    ################ Serial Simulation   #####################
    # Run simulations at the collocation points
#    for m in range(NofPa+1):
#        Output=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile,InputFile,OutputFile[m],O_int[m],NofPa ,m,TotalTimeStep)
#        # save each output in their corresponding array
#        for Var_Nr in range(NofVar):
#            TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
    
    ################ Parallel Simulation   #####################
    jobs = []
    items =[]
    out_q = multiprocessing.Queue()
    
    # Simulation runs for the optimal integration points     
    for m in range(P):
        p = multiprocessing.Process(target=Modelrun, args=(Inputs.MainDir,Inputs.ModelName,Inputs.ShellFile_Calib,Inputs.ShellKeywords,Inputs.InputFile_Calib,OutputFile[m],Inputs.nameList,Inputs.Factors,Inputs.ParameterNames,OptimalCollocationPoints,m,TotalTimeStep, out_q,)) #index here is 0: Variant 1
        jobs.append(p)
        
    for m,job in enumerate(jobs):
        job.start()
        time.sleep(0.1)
    #------- Try to prepare one matrix with the total model results -------------
#        while not out_q.empty():
#            print "I am still working, Farid!!!"
#            items.append(out_q.get())
             #----------------------
#        try:
#            print "Try & Except!!!"
#            Output = out_q.get(timeout=2)
#            
#            #if Output is not None:
#            print "Output %s:\n"%m, Output
#            TimestepMatrix = Output[0]
#            
#            for Var_Nr in range(NofVar):
#                TotalOutput_[Var_Nr]=np.vstack((TotalOutput_[Var_Nr], Output[Var_Nr+1])) #ATTENTION: Var_Nr+1: First array is the timestep
#            
#            
#            if Output is None:
#                print "Output is None!!!"
#                break
#                # call working fuction here
#            out_q.task_done()
#        except Exception as error:
#            import logging
#            logging.info("Writer: Timeout occurred {}".format(str(error)))
#            pass
    
#    for NofE in range(NofPa+1):
#        print "I am still working, Farid!!!"
#        item.append(out_q.get_nowait())
    
    #------- Organize the total simulation outputs -------------
    for NofE in range(P):        
        items.append(out_q.get())

    # save each output in their corresponding array
    for NofE in range(P):
        for  Var_Nr in range(Inputs.NofVar):
            #print "TotalOutput_[%s]\n:" %Var_Nr, TotalOutput_[Var_Nr]

            TotalOutput_[Var_Nr,NofE,:]=items[NofE][Var_Nr+1] #ATTENTION: Var_Nr+1: First array is the timestep
            
    TimestepMatrix = items[0][0]
    
    #wait for this process to complete
    for job in jobs:
        job.join() 
    
    # -------------------- Read Measurements --------------------
    Observations_calibration = Model_Run_exp.MeasurementMatrix(Inputs.MainDir + Inputs.MeasurementFile_Calib,TimestepMatrix[:Inputs.CalibTimeStep])
    Observations_validation = Model_Run_exp.MeasurementMatrix(Inputs.MainDir + Inputs.MeasurementFile_Valid,TimestepMatrix[Inputs.CalibTimeStep:TotalTimeStep])#[:,CalibTimeStep:TotalTimeStep]
    Observations = np.hstack((Observations_calibration , Observations_validation))
    
    #########################################################################
    ################### Loop over NofVar  ###################################
    #########################################################################
    #for index in range(NofVar):

        #TotalOutput = TotalOutput_[index]
    TotalOutput = TotalOutput_
    print "TotalOutput is prepared!!! \n", TotalOutput

    #==========================================================================
    #== Calculation of Response Surface Coefficients and prior Likelihoods ====
    #==========================================================================
    print('===> Computing PCE Coefficients of the calibration stage!\n')
    ItrNr = 0

    # Calculate the coefficients for the initial surrogate model
    initRS, Psi_RS = RS_calculation_aPC(Inputs, ItrNr, TotalOutput, OptimalCollocationPoints,PolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)
    print '-' * 50
    
    # Slice the Response Surface for the first 4 days as the calibration period
    SliceRSCoeffs =  lambda x: np.array([[line[:x] for line in element if len(line) > x] for element in initRS]) 
    initCalibRS = SliceRSCoeffs(Inputs.CalibTimeStep)
    
    SliceTotalOutput =  lambda x: np.array([[line[:x] for line in element if len(line) > x] for element in TotalOutput]) 
    initCalibModelOutputs = SliceTotalOutput(Inputs.CalibTimeStep)
    
    # Calculate the determination coefficients via Corrected Leave-one-out Cross Validation
    Q_2 = CorrectedLeaveoutError(Psi_RS, initCalibRS, initCalibModelOutputs, Inputs.CalibTimeStep, Inputs.Variants)
    
    # Calculate and plot the Likelihoods according to the initial Response Surfaces
    InitialLikelihoods, Psi_W = PriorLikelihoods(Inputs, ItrNr,None,inputDistributions, initCalibRS, Observations_calibration,Inputs.CalibTimeStep, PolynomialBasisFileName, PolynomialDegrees)
    BME = np.mean(InitialLikelihoods)

    print('---> Likelihoods of parameters before Iterations. \n')
    plot_weights(inputDistributions, InitialLikelihoods, Inputs, 'Weights_before Iterations', initCalibRS,0)
    
    # BME Correction
    Corr_Weight = BME_Corr_Weight(Inputs, ItrNr, OptimalCollocationPoints, initCalibRS, initCalibModelOutputs,
                                  Observations_calibration,Inputs.CalibTimeStep, PolynomialBasisFileName, PolynomialDegrees)
    
    corrFactorBME = np.mean(Corr_Weight)
    print("Intial corrFactorBME:", corrFactorBME)
    
    Bayesian_Evidence=np.append(Bayesian_Evidence, BME * corrFactorBME)
    
    print '='*50
    # =========================================================================
    # ======================== Bayesian Updating  =============================
    # =========================================================================
    #  Selection of Parameter Set with Maximum Weights &
    #  derivition of new response surface including Parameter Set with Maximum Weights
    print('===> Bayesian Updating of the calibration stage starts here!\n')
    #NewCalibrationEvolution=np.empty((0, NofMeasurementsCalibration))
    #NewOutput=np.empty((0, TotalTimeStep))
    #Bayesian_Evidence_[index]=np.empty((0, 1))
    

    # Improvement of Response Surfaces by modification of integration Points
    for itrIdx in range(Inputs.NofItr+1):
        if itrIdx == 0:
           NewPolynomialDegrees = PolynomialDegrees
           allNewPolynomialDegrees = PolynomialDegrees
           BestIdx=[]
           NrofSparse=1
           sparseCoeff=initCalibRS
           CalibRS=initCalibRS
           NewCollocationPoints = OptimalCollocationPoints
           NewOutput=TotalOutput
           Likelihoods = InitialLikelihoods
           
           
        else:
           NewCollocationPoints, NewOutput = selection_max_weight(Inputs,inputDistributions, NewCollocationPoints, Likelihoods, NewOutput, TotalTimeStep,itrIdx, m+itrIdx)
           
           # Print the SRQ's of Model and Response Surface
           Variant_plot(Inputs,itrIdx,Observations_calibration, NewOutput[:,-1,:], NewCollocationPoints[-1],CalibRS ,PolynomialBasisFileName,NewPolynomialDegrees,BestIdx,1)
           #Variant_plot(Inputs.MainDir,Inputs.ModelName,Inputs.NofPa,Inputs.d,Inputs.Variants,itrIdx,Observations_calibration, NewOutput[:,-1,:], O_new[-1], sparseCoeff,PolynomialBasisFileName,allNewPolynomialDegrees,BestIdx,NrofSparse,Inputs.CalibTimeStep)
           
           
           # Select new Combos based on BME
           #NewPolynomialDegrees = aPC.NewTerm_Selection(MainDir,NofVar,NofPa,d,i,MCSize_NewCombos,TotalTimeStep,Parameters, Percentage,Observations_calibration[1],O_new, NewOutput[0],MeasurementError,NewPolynomialDegrees,PolynomialBasisFileName)
           NewPolynomialDegrees, BestIdx, allNewPolynomialDegrees ,Coeffs,NrofSparse  = aPC.NewTerm_Selection(Inputs,itrIdx,TotalTimeStep,NewCollocationPoints, NewOutput, Observations,NewPolynomialDegrees,PolynomialBasisFileName)
           print "NewPolynomialDegrees:\n", NewPolynomialDegrees 
           
           # sparseCoeff to be used to plot all sparce RS
           #sparseCoeff = Coeffs[:,:,:,:Inputs.CalibTimeStep]
           #sparseCoeff = [[coeffs[:,:Inputs.CalibTimeStep] for i, coeffs in enumerate(Coeffs[varidx])] for varidx in range(len(Coeffs)) ]
           
           # Calculation of the response surface coefficients
           TotalRSCoeffs, Psi_RS = RS_calculation_aPC(Inputs, itrIdx, NewOutput, NewCollocationPoints, NewPolynomialDegrees, PolynomialBasisFileName, TotalTimeStep)
           
           # Slice the Response Surface for the CalibTimeStep as the calibration period
           #RS = R[:,:,:Inputs.CalibTimeStep]
           func =  lambda x: np.array([[line[:x] for line in element if len(line) > x] for element in TotalRSCoeffs]) 
           CalibRS = func(Inputs.CalibTimeStep)
           
           Q_2 = CorrectedLeaveoutError(Psi_RS, CalibRS, NewOutput, Inputs.CalibTimeStep, Inputs.Variants)
           # Comment: This only indicate that the response surface is fine for the last step
           #if i !=1:
           #    Variant_plot(MainDir,ModelName,NofPa,d,Variants,i+1,Observations_calibration, NewOutput[:,-1,:], O_new[-1], RS,PolynomialBasisFileName,NewPolynomialDegrees,BestIdx,1,CalibTimeStep)
          
           # Merge NewCombos with the polynomial degrees
           #NewPolynomialDegrees = NewPolynomialDegreeMerger(PolynomialDegrees,NewCombos,TotalTimeStep,i)
           #print "ItrNr,NewPolynomialDegrees:\n", i, NewPolynomialDegrees
           
           # Weight calculation with updated response surface
           Weights,Psi_W = PriorLikelihoods(Inputs,itrIdx,Psi_W, inputDistributions, CalibRS, Observations_calibration,Inputs.CalibTimeStep,PolynomialBasisFileName,NewPolynomialDegrees)
           # Here, the arigmathic mean of the weights of priors will be calculated as model evaluation criterion
           BME=np.mean(Weights) # Bayesian Evidence
           
           # BME Correction
           Corr_Weight = BME_Corr_Weight(Inputs, itrIdx, NewCollocationPoints, CalibRS, NewOutput, Observations_calibration,
                                         Inputs.CalibTimeStep, PolynomialBasisFileName, NewPolynomialDegrees)
            
           corrFactorBME = np.mean(Corr_Weight)
           print("corrFactorBME:", corrFactorBME)
           Bayesian_Evidence=np.append(Bayesian_Evidence, BME * corrFactorBME)
           
           
           print('---> Plotting Weights after Iterations %s \n'%itrIdx)
           plot_weights(inputDistributions, Weights, Inputs, 'Weights_Iterations_%s' %itrIdx, TotalRSCoeffs, itrIdx)
           #sys.exit("Farid, I terminate the script here according to your request!")
        
    # =========================================================================
    # =================  Prior & Posterior Distributions ======================
    # =========================================================================
    # Rejection Sampling: filtration of prior distribution via uniform to get posterior

    Post_MC_Vector,Post_MC_Weights =filtration_prior(Inputs, inputDistributions, Weights, Inputs.MCsize)
    print '-' * 50

    print '----> Posterior Parameters vs their Weights\n'
    plot_weights(Post_MC_Vector, Post_MC_Weights, Inputs, 'Posterior_Parameters-vs_Weights_Calib',TotalRSCoeffs, None)

    print '-' * 50
    print('---> Histogram of Posteriors for calibration stage\n')
    plot_Histogram(Post_MC_Vector,Post_MC_Weights,Inputs, 'Histogram_Posterior_calib')
    
    mean_RS_Cal, stdev_RS_Cal = Mean_Stdev(Inputs.MainDir,Inputs.NofPa,Inputs.d,Inputs.NofVar,itrIdx,None,Post_MC_Vector, CalibRS, PolynomialBasisFileName,NewPolynomialDegrees,Inputs.CalibTimeStep)
    print '-' * 50
    
    # =========================================================================
    # === STOCHASTIC CALIBRATION of the Model using the Best Parameterset =====
    # =========================================================================
    print('---> Stochastic Calibration of Model is being done. \n')
    # Selection of the parameter set with the highest weight
    sorted_Parametersets_weight_calibration = Post_MC_Vector[np.argsort(Post_MC_Weights)][::-1]
    BestParameterset_calibration = sorted_Parametersets_weight_calibration[0] #[0, 0:NofPa]
    print "Parameterset selected for the stochastic calibration of model: \n", BestParameterset_calibration
    
    # Run model variant for the stochastic calibration model
    #Model_Run_exp.TelemacRun_Calibration(BestParameterset_calibration,R,NofPa, mean_RS_Cal, stdev_RS_Cal, Variants[index])
    # Run_No = -2    >>>>> Calibration
    Output_Calibration = Model_Run_exp.ModelRun(Inputs.MainDir,Inputs.ModelName,Inputs.ShellFile_Calib,Inputs.ShellKeywords,Inputs.InputFile_Calib,OutputFile[-2],Inputs.nameList,Inputs.Factors,Inputs.ParameterNames,BestParameterset_calibration[0], -2,TotalTimeStep)
    
    Calib_Valid_plot(Inputs.MainDir,Inputs.NofPa,Inputs.d,Inputs.Variants,itrIdx,Observations_calibration, Output_Calibration, BestParameterset_calibration, CalibRS, mean_RS_Cal, stdev_RS_Cal,PolynomialBasisFileName,NewPolynomialDegrees,Inputs.CalibTimeStep, "Calib")
    print('---> Calibration of Model has finished. \n')
    print '========================================================================='

    # =========================================================================
    # ====== Using Posterior Parametersets for STOCHASTIC VALIDATION ==========
    # =========================================================================
    print('===> Validation Process for the validation locations.\n')
    # The posterior matrix of calibration is used as the prior parameters of the validation
    Prior_for_Validation_original = Post_MC_Vector #[:, 0:NofPa]

    #============ Resampling of the prior of validation parameters ===============
    # We do resampling for the case in which not enough parametersets remain after filtering
    #print "Prior_for_Validation_original:\n", Prior_for_Validation_original
    if len(Prior_for_Validation_original) < 20:
        print "Resampling has been performed!!!"
        Prior_for_Validation=resampling(Prior_for_Validation_original, Inputs.MCsize)
        print "Prior_for_Validation:\n", Prior_for_Validation
    else:
        Prior_for_Validation = Prior_for_Validation_original
    #==============================================================================
    # Slice the Response Surface for the first 4 days as the validation period
    #RSValid = R[:,:,Inputs.CalibTimeStep:TotalTimeStep]
    # Slice the Response Surface for the first 4 days as the validation period
    funcValid =  lambda x: np.array([[line[x:] for line in element if len(line) > x] for element in TotalRSCoeffs]) 
    RSValid = funcValid(Inputs.CalibTimeStep)
    print("RSValid:\n", RSValid)
    
    Weights_Validation, Psi = PriorLikelihoods(Inputs,itrIdx,None,Prior_for_Validation, RSValid, Observations_validation, Inputs.ValidTimeStep,PolynomialBasisFileName,NewPolynomialDegrees)
    #Prior_Validation = np.hstack((Prior_for_Validation, Weights_Validation))

    plot_weights(Prior_for_Validation, Weights_Validation, Inputs, 'Prior_Weights_Valid', TotalRSCoeffs, None)

    Post_MC_Validation, Post_MC_Validation_Weights = filtration_prior(Prior_for_Validation, Weights_Validation, len(Prior_for_Validation))

    print('---> Histogram of Posteriors for calibration stage\n')

    plot_Histogram(Post_MC_Validation,Post_MC_Validation_Weights,Inputs, 'Posterior_Histogram_Valid')

    # ========================================================================
    # ====== Storing Mechanism of Bayesian Evidence
    # ========================================================================

    BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Bayesian_Evidence))
    #print (r'The Bayesian evidence for this model is %.5f.' % Bayesian_evidence)

    # =========================================================================
    # ====== STOCHASTIC VALIDATION of the Model using the Best Parameterset ===
    # =========================================================================
    print('---> Validation of Model is being done. \n')
    # Selection of the parameter set with the highest weight
    sorted_Parametersets_weight_validation = Post_MC_Validation[np.argsort(Post_MC_Validation_Weights)][::-1]
    BestParameterset_validation = sorted_Parametersets_weight_validation[0] #[0, 0:NofPa]
    
    mean_RS_Val, stdev_RS_Val = Mean_Stdev(Inputs.MainDir,Inputs.NofPa,Inputs.d,Inputs.NofVar,itrIdx,None,Post_MC_Validation, TotalRSCoeffs, PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep)
    print "Parameterset selected for the stochastic validation of model: \n", BestParameterset_validation
    #Model_Run_exp.TelemacRun_Validation(BestParameterset_validation,R,NofPa, mean_OutputRS_v, stdev_OutputRS_v, Variants[index])
    
    # Run_No = -1    >>>>> Validation
    #Output_Validation=Model_Run_exp.ModelRun(MainDir,ModelName,ShellFile_Valid,ShellKeywords,InputFile_Valid,OutputFile[-1],nameList,Factors,ParameterNames,BestParameterset_validation[0], -1,TotalTimeStep)
    
    Output_Validation = Model_Run_exp.ModelRun(Inputs.MainDir, Inputs.ModelName, Inputs.ShellFile_Calib, Inputs.ShellKeywords, Inputs.InputFile_Calib,OutputFile[-1],Inputs.nameList,Inputs.Factors,Inputs.ParameterNames,BestParameterset_validation[0], -1,TotalTimeStep)
    
    #Plot the validation
    Calib_Valid_plot(Inputs.MainDir,Inputs.NofPa,Inputs.d,Inputs.Variants,itrIdx,Observations, Output_Validation, BestParameterset_validation, TotalRSCoeffs, mean_RS_Val, stdev_RS_Val,PolynomialBasisFileName,NewPolynomialDegrees,TotalTimeStep, "Valid")
    
    print "Output_Validation:\n", Output_Validation
    print('---> Validation of Variant has finished. \n')

    #print('============================= Next Model Variant ================================== \n')

    
    ###################### BME Correction ##########################
    #NewOutput: Oputputmatrix of original model runs
    # O_new: Parameter sets saved during the Bayesian updating
    Corr_Weight = BME_Corr_Weight(Inputs.MainDir, Inputs.NofPa, Inputs.d, Inputs.NofVar, itrIdx, NewCollocationPoints,
                                  TotalRSCoeffs, NewOutput,Observations, Inputs.MeasurementError, TotalTimeStep,
                                  len(NewCollocationPoints), PolynomialBasisFileName, NewPolynomialDegrees)
    
    Weight_RM = np.mean(Corr_Weight)
    
    
    return BayesianEvidenceMatrix , Weight_RM


###################################################################################################
####################################### MAIN PROGRAM ##############################################
###################################################################################################

if __name__ == "__main__":
    
    config = configparser.ConfigParser()
    try:
        SysArg = sys.argv[1]
    except:
        print("No input file given.")
        
    config.read(SysArg)
    
    Inputs = Parser.Parser(config)
#    print("Inputs:", Inputs.MeasurementFile_Valid)
#    sys.exit()
    ##########################################################
    # Creating names of output files
    P = math.factorial(Inputs.NofPa+Inputs.d)/(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d))
    OutputFile=[]
    for RunNr in range(P +Inputs.NofItr+1):
        OutputFile.append(Inputs.InputFile_Calib.split('.input')[0]+"_%s.csv" %(RunNr+1))
    
    OutputFile.append(Inputs.InputFile_Calib.split('.input')[0]+"_calib.csv") # Calibration
    
    OutputFile.append(Inputs.InputFile_Calib.split('.input')[0]+"_valid.csv") # Validation
    
    # =========================================================================
    # ===== Starting the jobs on cluster via multiprocessing (Entire Script)===
    # =========================================================================
#    out_qq = multiprocessing.Queue()
#    for index in range(NofSF):
#        jobst = []
#
#        task = multiprocessing.Process(target=RS_ST, args=(MainDir,ShellFile,InputFile,OutputFile,O_int,NofPa,TotalTimeStep, out_qq,))
#        jobst.append(task)
#
#        task.start()
#
#    for NofE in range(NofSF): # NodSF: Nr of different model variants
#        BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, out_qq.get()))
#
#    for pr in jobst:
#        pr.join()

#    print '-----------------------------------------------------------------------'
#    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix
    # =========================================================================
    # ==================  Starting the jobs in serie ==========================
    # =========================================================================
    #BayesianEvidenceMatrix=np.empty((0, NofItr))

    # Start the calculations
    #BayesianEvidenceMatrix =RS_ST(MainDir,ModelName,ShellFile,InputFile,OutputFile,O_int,NofPa,NofVar,TotalTimeStep)
    #BayesianEvidenceMatrix , Weight_RM=RS_ST(MainDir,ModelName,ShellFile_Calib,ShellKeywords,InputFile_Calib,ShellFile_Valid,InputFile_Valid,MeasurementFile_Calib,MeasurementFile_Valid,MeasurementError,ModelError,NofVar,NofPa, d, MCsize,problem,ParameterNames,Parameters, Distributions, OutputFile,nameList,Factors,CalibTimeStep,ValidTimeStep)
    BayesianEvidenceMatrix , Weight_RM = RS_ST(Inputs,OutputFile)

    # Save the BME in an array
    #BayesianEvidenceMatrix=np.vstack((BayesianEvidenceMatrix, Out))

    print '-----------------------------------------------------------------------'
    print 'The Bayesian evidence for the competing models are as follows, respectively:\n', BayesianEvidenceMatrix

    # =========================================================================
    # ==================  Normalization and Plotting the BMEs =================
    # =========================================================================
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    def normalize_column(A):
        normed_A=np.empty((len(A), 0))
        for col in range(len(A[0])):
             B = (A[:,col] / np.sum(A[:,col]))
             normed_A=np.hstack((normed_A,B[:,None]))
        return normed_A
    normed_BayesianEvidenceMatrix=normalize_column(BayesianEvidenceMatrix)

    # Normalization of BMEs
    #normed_BayesianEvidenceMatrix = normalize(BayesianEvidenceMatrix, axis=0, norm='l1')
    log_normed_BayesianEvidenceMatrix=np.log10(normed_BayesianEvidenceMatrix)
    log_BayesianEvidenceMatrix = np.log10(BayesianEvidenceMatrix)
    
    # Plot the BMEs
    print('---> Plotting the BME vs Iterations \n')
    Nr_Iteration=np.asarray(range(Inputs.NofItr+1))
    
    formatting_function = np.vectorize(lambda f: format(f, '8.5E'))
    print "BayesianEvidenceMatrix: \n", formatting_function(BayesianEvidenceMatrix)
    print "log_normed_BayesianEvidenceMatrix: \n", formatting_function(log_normed_BayesianEvidenceMatrix)
    print "Weight_RM:\n", formatting_function(Weight_RM)
    
    Plot_BME(BayesianEvidenceMatrix,log_BayesianEvidenceMatrix, Nr_Iteration, Inputs.Variants)
    
    # Report the total time
    toc=timeit.default_timer()
    elapsedtime= toc - tic
    ElapsedTime=str(timedelta(seconds=elapsedtime))

    print "Elapsed Time:",ElapsedTime
    print('My work is over. \n')
