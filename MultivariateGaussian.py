#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 11:18:08 2018
This script calculates the multivariate gaussian weights for dependent and independent datasets.
Ma, Y-Z., and Aaron Berndsen. "How to combine correlated data sets—A 
    Bayesian hyperparameter matrix method." Astronomy and Computing 5 (2014): 45-56.
Link: https://arxiv.org/pdf/1309.3271.pdf
@author: farid
"""
import numpy as np
import aPC


def Weight_indep(NrDataset,Parameterset,Observations,OutputRS,Hyperparameters):
    """
    OutputRS: Should have a shape of (NrDataset,ParametersetSize,NofMeasurements)
    Observations: contains each SRQ's measurement data in each row
    """
    ParametersetSize = Parameterset.shape[1]
    NofMeasurements = Observations.shape[1]
    Weights=np.zeros((ParametersetSize, NrDataset))

    #Loop over all parameter sets
    for ParIdx in range(ParametersetSize):
        # Calculate Matrix (5) of the paper
        X = np.zeros((NrDataset,NofMeasurements,1))#ParametersetSize
        # Loop over all datasets
        for idx_dset in range(NrDataset):
            # Calculate deviations for each parameterset 
            #deviation_XS = np.zeros((1,NofMeasurements))
            #for i in range(ParametersetSize):
            deviation_XS=Observations[idx_dset] - OutputRS[idx_dset,ParIdx, :] #[idx_dset][i, :]
            
            X[idx_dset]=deviation_XS[:,None]#.T
        
        
        
        #------------ Covariance matrix ------------
        # Initialization of a matrix of NrDataset x NrDataset
        C_hat = [[[] for j in range(NrDataset)] for ii in range(NrDataset)]
        
        for i in range(NrDataset):#row
            for j in range(NrDataset):#column
                C_hat[i][j] = np.dot(X[i],X[j].T)
            

        #------------ Likelihoods' matrix ------------
        
        
        for SetIdx in range(NrDataset):
            C_s=np.diag(np.diag(C_hat[SetIdx][SetIdx])) # Off-diagonal entries set to zero (independent assumption)
            
            #for i in range(ParametersetSize):
            Weights[ParIdx,SetIdx]=(Hyperparameters[SetIdx]**(NofMeasurements/2))* \
                              (np.exp(-0.5 * Hyperparameters[SetIdx]* np.dot(np.dot(X[idx_dset].T, np.linalg.inv(C_s)), X[idx_dset])))/ \
                              (((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(C_s)))
    
            
    Weights = np.prod(Weights,axis=1)
    
    return X , C_hat , Weights

def Weight_dep(NrDataset,Parameterset,Observations,OutputRS,Hyperparameters):
    """
    OutputRS: Should have a shape of (NrDataset,ParametersetSize,NofMeasurements)
    Observations: contains each SRQ's measurement data in each row
    """
    ParametersetSize = Parameterset.shape[1]
    NofMeasurements = Observations.shape[1]
    Weights=np.zeros((ParametersetSize, 1))
    
    a = np.zeros((1, NrDataset))
    for SetIdx in range(NrDataset):
        a[0,SetIdx ]=(Hyperparameters[SetIdx] / (2*np.pi)) **(NofMeasurements/2)
    a = np.prod(a,axis=1)
    #------------ Hyperparameter matrix P ------------
    # Initialization of a matrix of NrDataset x NrDataset
    P = np.zeros((NrDataset,NrDataset))
    for i in range(NrDataset):#row
        for j in range(NrDataset):#column
            P[i,j] = (Hyperparameters[i]**-0.5) * (Hyperparameters[j]**-0.5)
    
    # Kronecker product of P matrix
    J = np.ones((NofMeasurements,NofMeasurements))
    P_Ex = np.kron(P,J)

    # Hadamard inverse of P
    P_HadInv = np.reciprocal(P_Ex)
    #-------------------------------------------------
    
    #Loop over all parameter sets
    for ParIdx in range(ParametersetSize):
        # Calculate Matrix (5) of the paper
        X = np.zeros((NrDataset,NofMeasurements))#ParametersetSize
        # Loop over all datasets
        for idx_dset in range(NrDataset):
            # Calculate deviations for each parameterset 
            #deviation_XS = np.zeros((1,NofMeasurements))
            #for i in range(ParametersetSize):
            X[idx_dset]=(Observations[idx_dset] - OutputRS[idx_dset,ParIdx, :]) / np.sqrt(Hyperparameters[idx_dset])
            
            #X[idx_dset]=deviation_XS#[:,None]#.T
            

        #------------ Covariance matrix ------------
        # Initialization of a matrix of NrDataset x NrDataset
        #C_hat = [[[] for j in range(NrDataset)] for ii in range(NrDataset)]
        #C_hat = np.zeros((NrDataset,NrDataset))
        
        C_hat = np.zeros((NrDataset*NofMeasurements,NrDataset*NofMeasurements))
        for i in range(NrDataset):#row
            for j in range(NrDataset):#column
                #C_hat[i*NofMeasurements:(i+1)*NofMeasurements,j*NofMeasurements:(j+1)*NofMeasurements] = np.dot(X[i][:,None],X[j][:,None].T)#np.dot(X[:,i],X[:,j].T)
                if i==j: # independent assumption
                    print "Shape:", np.dot(X[i][:,None].T,X[j][:,None])
                    C_hat[i*NofMeasurements:(i+1)*NofMeasurements,j*NofMeasurements:(j+1)*NofMeasurements] = np.dot(X[i][:,None].T,X[j][:,None])
                
        #print "Cross-correlation:\n",np.cov(X)#[:,0], X[:,1]
        #C_hat =np.cov(X)
        #print 1/np.linalg.det(C_hat)
        #------------ Likelihoods' matrix ------------
        
        P_HadInv_C_hat=np.multiply(P_HadInv,np.linalg.inv(C_hat)) # non-zero off-diagonal enteries (dependent assumption)
        #print "P_HadInv_C_hat:", P_HadInv_C_hat.shape
        #print "Det(C_hat):", np.linalg.det(C_hat)
        
        #for i in range(ParametersetSize):
        #Weights[ParIdx,0]=a *(np.exp(-0.5 * np.dot(np.dot(X[:,idx_dset].T, P_HadInv_C_hat), X[:,idx_dset])))/ \
        #                      np.sqrt(np.linalg.det(C_hat))
        Weights[ParIdx,0]=a *(np.exp(-0.5 * np.dot(np.dot(X.flatten('F'), P_HadInv_C_hat), X.flatten('F')[:,None])))/ \
                              np.sqrt(np.linalg.det(C_hat))
        
    #Weights = np.prod(Weights,axis=1)
    
    return X , C_hat , Weights









if __name__ == "__main__":
    ##########################################################
    N=3 #5 Number of Uncertantain Parameters
    NrDataset = 2
    MCsize=1500 #1500 
    NofMeasurement = 50
    Hyperparameters = [0.5, 0.9]
    ##########################################################
    # Collocation Points
    Parameters=[0.13,1.16,0.12383] #[0.03,0.116,0.08383]
    Percentage = 2
    
    
    Parameterset = aPC.Parameter_Initialization(N,MCsize, Parameters, Percentage)
    
    
    x = Parameterset[0,:NofMeasurement]
    y = Parameterset[1,:NofMeasurement]
    z = Parameterset[2,:NofMeasurement]
    OutputRS = np.zeros((NrDataset,MCsize,NofMeasurement))
    
    for idx in range(NrDataset):
        for i in range(MCsize):
            OutputRS[idx,i] = np.asarray([(x+y)+np.random.uniform(0,0.19)+(x*2+y)**1+np.random.uniform(0,1)])
    #OutputRS[1] =np.asarray(x**2 + y**2 + 2*x*y + z**2 + 2*x*z +2*y*z)
    
    
    Obs = aPC.Parameter_Initialization(N,NofMeasurement, Parameters, Percentage)
    x = Obs[0]
    y = Obs[1]
    z = Obs[2]
    
    Observations= np.zeros((NrDataset,NofMeasurement))
    Observations[0] = np.asarray([(x**2+y)**2+(x+y**2)**2])
    Observations[1] =np.asarray(x**2 + y**2 + 2*x*y + z**1.2)
    
    #Deviation , C_hat , Weight_indep= Weight_indep(NrDataset,Parameterset,Observations,OutputRS,Hyperparameters)
    
    Deviation , C_hat , Weight_dep= Weight_dep(NrDataset,Parameterset,Observations,OutputRS,Hyperparameters)
    
    
    
            
           
            