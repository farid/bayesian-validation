Validation Benchmark

Description: TODO

+++++++++++++++++++ Response Surface +++++++++++++++++++
This program reads the input points of parameter p_1 to p_8 form text file 'input1.txt'and generates uniform random values between min and max.
It tries to calculate the coefficent of the response surface:
              Rs=a_o + a_1 x p_1 + a_2 x p_2+ a_3 x p_3+ a_4 x p_4+ a_5 x p_5
The original function is:
              f(p_1 ... p_5) comes from Model Outputs
N= number of iterations to improve the Response surfaces
NofMeasurements=111 (The Rhine Model)
ModelRuns=Nofpa+N


+++++++++++++++++++ BME Calculation +++++++++++++++++++
