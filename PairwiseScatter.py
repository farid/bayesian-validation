#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 12:12:41 2018

@author: farid
"""
import seaborn as sns; sns.set(style="ticks", color_codes=True)
import numpy as np
import pandas as pd
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
from scipy import stats



datasets = np.genfromtxt('/home/farid/bayesian-validation/bayesian-validation/SensitivityAnalysis/Outputs/Histogram_Posterior_calib.txt', delimiter=',') #Weights_before Iterations
#print "names:", datasets.dtype.names
#,dtype=None, names = ['P_1','P_2','P_3','P_4','Weight'],

df = pd.DataFrame(datasets[:,:-1], columns = ['$P_1$','$P_2$','$P_3$','$P_4$','$P_5$','$P_6$','$P_7$','$P_8$']) #'Weights'

########################################################
#         Pairwise scatter plot with Pandas
########################################################
scatter_matrix(df, alpha = 0.8, figsize = (10, 10), diagonal = 'kde',  marker='.',
               hist_kwds={'bins': 20}, s=60)

#df.plot(kind='kde', subplots=True,figsize = (10, 10), layout=(4,4), sharex=False)


########################################################
#                PairPlot with Seaborn
########################################################

def histMoments(sns_plot,df,NofPa):
    for idx, x1 in enumerate(df):
        ax = sns_plot.fig.axes[idx*NofPa+idx]
        anchored_text = AnchoredText("$\mu$=%.4g \n $\sigma$=%.4g \n $S$=%.4f \n $k$=%.4f" %(np.mean(df[x1]), np.std(df[x1]), stats.skew(df[x1]), stats.kurtosis(df[x1])), loc=2)
        ax.add_artist(anchored_text)    
    

g = sns.PairGrid(df, palette=["red"])
g.map_upper(plt.scatter, s=20)
axes = g.map_diag(sns.distplot, hist=True, kde=False, color="g")
histMoments(axes,df,8)
#g.map_diag(corrfunc)
g.map_lower(sns.kdeplot, cmap="Blues_d",shade=False)





plt.show()
#sns_plot.savefig("output.png")