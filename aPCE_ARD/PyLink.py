# -*- coding: utf-8 -*-
"""
Created in July 2019
This program runs the model defined by the user.

@author: Farid Mohammadi, M.Sc.
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS),
University of Stuttgart

"""

from decimal import Decimal
import subprocess
import os
import sys
import shutil
import numpy as np
import matplotlib.pyplot as plt
import fileinput
import timeit
import matplotlib.tri as tri
import time
import re
import multiprocessing
import numpy.lib.recfunctions as rfn
tic=timeit.default_timer()


float_formatter = lambda x: "%.5f" % x
np.set_printoptions(formatter={'float_kind':float_formatter})



#def FilePath(InputFile):
#    return os.path.abspath(InputFile)

class ForwardModel:
    def __init__(self):
        self.MainDir = os.getcwd() + '/'
        self.ModelName = None
        self.ShellFile = None
        self.ShellKeywords = None
        self.InputFile = None
        self.ParameterNames = None
        
        self.MeasurementFile = None
        self.OutputFile = None
        self.NameList = None
        self.Outputs = None
        self.NofVar = None
        self.Factors = None
        
        self.OutputMatrix = None
    
    #--------------------------------------------------------------------------------------------------------
    def read_Observation(self, TimestepMatrix):
        """Required for reading the measurement file."""
    
        data_Observation = np.genfromtxt(self.MeasurementFile, names=None, delimiter=";").T
        #ProfileREF = np.array(data_profile).T
    
        st = [round(i,4) for i in set(TimestepMatrix)]
        indices = [i for i, e in enumerate(data_Observation[0]) if round(e,4) in st]
        self.ObservationMatrix = data_Observation[:,indices]
        
        return self.MeasurementMatrix
    
    #--------------------------------------------------------------------------------------------------------
    def read_Output(self, File):
        """Required for reading the output files. """
        
        data_Output = np.genfromtxt(File, names=True, delimiter=";")
    
        Output = np.zeros((len(self.NameList),len(data_Output)))
    
        if len(data_Output) !=0:    
            for idx, name in enumerate(self.NameList):
                Output[idx] = data_Output[name] * self.Factors[idx]
            return Output
        else:
            return Output
    
    
    #--------------------------------------------------------------------------------------------------------
    
    def UpdateParamterNew(self, NewInputfile, CollocationPoints):
        """
        Finds this pattern with <X1> and replace it with the new value from
        the array CollocationPoints.
        """
        filename = NewInputfile #self.InputFile

        #NrofSamples = 1#CollocationPoints.shape[0]
        NofPa = CollocationPoints.shape[0]
        
        text_to_search_list = ['<X%s>'%(i+1) for i in range(NofPa)]
        
        #for sampleIdx in range(NrofSamples):
        replacement_text_list = list(CollocationPoints.astype('str'))
        
        # Read in the file
        with open(filename, 'r') as file :
          filedata = file.read()
        
        # Replace the target string
        for text_to_search, replacement_text in zip(text_to_search_list, replacement_text_list):
            filedata = filedata.replace(text_to_search, replacement_text)
        
        # Write the file out again
        with open(filename, 'w') as file:
          file.write(filedata)
              
    #--------------------------------------------------------------------------------------------------------
    def UpdateShellScript(self, NewShellfile, NewShellKeywords):
        """
        Replacement of simulation directory (simdir), input, Output.Name in shell script.
        """
        lookup = self.ShellKeywords
        
        # Read in the file
        with open(NewShellfile, 'r') as file :
          filedata = file.read()
        
        # Replace the target string
        for i in range(len(lookup)):
            filedata = filedata.replace(lookup[i], NewShellKeywords[i])
        
        # Write the file out again
        with open(NewShellfile, 'w') as file:
          file.write(filedata)
          
        return
    
    #--------------------------------------------------------------------------------------------------------
    def ModelExecutor(self, ShellFile):
        """ ModelExecutor takes the program and configuration file path
        to run the given model. """
    
    
        Process = subprocess.call(['./%s' %ShellFile])
    
        print('\nMessage 1:')
        print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process)
        return
    
    #--------------------------------------------------------------------------------------------------------
    def Function(self,NewShellfile, OutputFile):
        """ Function """
    
        print('\n','-'*70)
        print('Model is running...')
        #nameList = self.NameList
        #Factors = self.Factors
        
        #check if result has finished
        while True:
             time.sleep(3)
             files = os.listdir(".")
             if (OutputFile) in files:
    #             SimulationsDataMatrix=OutputMatrix(OutputFile,nameList,Factors)
    #             print "Waiting for the simulation to be finished!"#, len(SimulationsDataMatrix[0])
    #
    #             if len(SimulationsDataMatrix[0]) == TotalTimeStep:
    #                 break
                 break
             else:
                 # Model RUN
                 self.ModelExecutor(NewShellfile)
            
        #print('After Simulation')
        
        #SimulationsDataMatrix = OutputMatrix(OutputFile,nameList,Factors)
        
        SimulationsDataMatrix = self.read_Output(OutputFile)
        
        return SimulationsDataMatrix
    
    #--------------------------------------------------------------------------------------------------------
    
    def ModelRun(self, OutputFile, CollocationPoints, Run_No):
        """
        #
        This function gets the following inputs and gives the total evolution at all points.
        MainDir: Main Directory
        ShellFile: Original shell file
        InputFile: Input file e.g. .input
        OutputFile: Output file(s) name(s)
        CollocationPoints: The parameter values to be replaced in InputFile (davarzani2014a_1.22mps.input)
    
        Run_No: sequence of run (from Response Surface script) --> For storing purpose
    
        """
        if Run_No ==-2:
            NewInputfile = self.InputFile.split('.input')[0]+"_calib"
            NewShellfile = self.ShellFile.split('.sh')[0]+"_calib.sh" 
            NewOutputName = self.InputFile.split('.input')[0]+"_calib" 
            pvd_Name = NewOutputName+"_staggered-pm"
            
        elif Run_No ==-1:
            NewInputfile = self.InputFile.split('.input')[0]+"_valid"
            NewShellfile = self.ShellFile.split('.sh')[0]+"_valid.sh" 
            NewOutputName = self.InputFile.split('.input')[0]+"_valid"
            pvd_Name = NewOutputName+"_staggered-pm"
        else:
            NewInputfile = self.InputFile.split('.tpl')[0]+"_%d" %(Run_No)+ self.InputFile.split('.tpl')[1]
        
            NewShellfile = self.ShellFile.split('.sh')[0]+"_%d.sh" %(Run_No)
            NewOutputName = self.InputFile.split('.tpl')[0]+"_%d" %(Run_No)+ self.InputFile.split('.tpl')[1]
            pvd_Name = NewOutputName+"_staggered-pm"
     
    
        if Run_No == -2:
            newpath = (( self.ModelName + r'_Run_Calib'))
        elif Run_No == -1:
            newpath = (( self.ModelName + r'_Run_Valid'))
        else:
            newpath = (( self.ModelName + r'_Run_%s') %(Run_No))
        if not os.path.exists(newpath): os.makedirs(newpath)
    
        shutil.copy2(self.MainDir+self.InputFile, newpath)  # Input file of the model
        shutil.copy2(self.MainDir+self.ShellFile, newpath)  # Shell file
        
        os.chdir(newpath)
        os.rename(self.InputFile,NewInputfile)
        os.rename(self.ShellFile,NewShellfile)
        
        #----------------------------------------------
        #       Input Parameters
        #----------------------------------------------
        #Shell file will be modified here:
        
        NewShellKeywords = [newpath , NewInputfile, NewOutputName, pvd_Name]
        self.UpdateShellScript(NewShellfile, NewShellKeywords)
        
        # Update the parametrs in Input file
        self.UpdateParamterNew(NewInputfile,CollocationPoints)
        #--------- Simulation Starts here --------------------
        Output = self.Function(NewShellfile, OutputFile)
    
        os.chdir("..")
    
    
        return Output
    
    #--------------------------------------------------------------------------------------------------------
    
    def Parallel_Modelrun(self, OutputFile, OptimalCollocationPoints, m, out_q):
        Output = self.ModelRun(OutputFile, OptimalCollocationPoints[m], m+1)
        out_q.put(Output)
        return out_q 
    
    #--------------------------------------------------------------------------------------------------------
    
    def ExecuteModel(self, CollocationPoints):
        """
        ################ Parallel Simulation   #####################
        OutputFile : 
            
        This gives a dictionary with time step and all outputs.
        Each key contains an array of the shape P X TotalTimeStep.
        """
        # Initilization
        P = len(CollocationPoints)
        jobs = []
        items =[]
        self.NofVar = len(self.Outputs)
        TotalOutputs = {}
        out_q = multiprocessing.Queue()
        
        OutputFile=[]
        for RunNr in range(len(CollocationPoints)):
            OutputFile.append(self.InputFile.split('.tpl')[0]+"_%s.csv" %(RunNr+1))
        
        # Simulation runs for the optimal integration points     
        for m in range(P):
            p = multiprocessing.Process(target = self.Parallel_Modelrun, args=(OutputFile[m], CollocationPoints, m, out_q,)) #index here is 0: Variant 1
            jobs.append(p)
            
        for m,job in enumerate(jobs):
            job.start()
            time.sleep(0.1)
        
        #------- Store and organize the simulation outputs -------------
        for NofE in range(P):        
            items.append(out_q.get())
        
        # Save time steps
        TimestepMatrix = items[0][0]
        TotalOutputs["Time Steps"] = TimestepMatrix
        
        # save each output in their corresponding array
        for  Var_Nr in range(self.NofVar):
            Outputs = np.array([item[Var_Nr+1] for item in items])#ATTENTION: Var_Nr+1: First array is the timestep
            
            TotalOutputs[self.Outputs[Var_Nr]] = Outputs
        
        # Pass it to the attribute        
        self.OutputMatrix = TotalOutputs
        
        #wait for this process to complete
        for job in jobs:
            job.join() 
    
        return self.OutputMatrix