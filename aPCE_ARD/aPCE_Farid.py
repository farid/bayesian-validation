# To supress warnings
import warnings
warnings.filterwarnings("ignore")
import numpy as np
import math
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
# Set some parameters to apply to all plots. These can be overridden
# in each plot if desired
plt.rcParams.update({'font.size': 12})
plt.rc('figure', figsize = (12, 10))
plt.rc('font', family='serif', serif='Times')
plt.rc('axes', grid = True)
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=12)
plt.rc('ytick', labelsize=12)
plt.rc('axes', labelsize=12)
import scipy as sp
from tqdm import tqdm
import warnings
from itertools import combinations
from scipy import stats
import os
import aPoly_Construction
import sys
from sklearn.linear_model import ARDRegression, LinearRegression
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score



class ForwardModel:

    def __init__(self):
        #TODO: This must be set in the main file. 
        self.pyFile = 'Ishigami'
    
    def CompModel(self, Samples, noise_amount = 0.02):
        Filename = self.pyFile

        Function = getattr(__import__(Filename), Filename)

        y = Function(Samples)
        
        # Generate our data by adding some noise on to the generative function
        noise = np.std(y) * noise_amount
        
        return y + np.random.normal(0, 1, len(Samples[:,0])) * noise
    
class Input:
    def __init__(self):
        self.Marginals = []
    
    def addMarginals(self):
        self.Marginals.append(Marginal())
        
# Nested class
class Marginal:
    def __init__(self):
        self.Name = None
        self.DistType = None
        self.Moments = None
        self.Parameters = None

# Nested class
class ExpDesigns:
    def __init__(self, aPCE):
        self.NrSamples = None
        self.SamplingMethod = 'PCM'
        self.X = None
        self.Y = None
    
    def GetSample(self,aPCE):
        if self.SamplingMethod == 'LHS':
            self.LHSampling(aPCE)
        elif self.SamplingMethod == 'PCM':
            self.PCMSampling(aPCE)
        elif self.SamplingMethod == 'LSCM':
            self.LSCMSampling()
        return self.X
    
    def cartesian(self, arrays, out=None):
        """
        Generate a cartesian product of input arrays to get all combination.
    
        Parameters
        ----------
        arrays : list of array-like
            1-D arrays to form the cartesian product of.
        out : ndarray
            Array to place the cartesian product in.
    
        Returns
        -------
        out : ndarray
            2-D array of shape (M, len(arrays)) containing cartesian products
            formed of input arrays.
    
        Examples
        --------
        >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
        array([[1, 4, 6],
               [1, 4, 7],
               [1, 5, 6],
               [1, 5, 7],
               [2, 4, 6],
               [2, 4, 7],
               [2, 5, 6],
               [2, 5, 7],
               [3, 4, 6],
               [3, 4, 7],
               [3, 5, 6],
               [3, 5, 7]])
    
        """
        arrays = [np.asarray(x) for x in arrays]
        dtype = arrays[0].dtype
    
        n = np.prod([x.size for x in arrays])
        if out is None:
            out = np.zeros([n, len(arrays)], dtype=dtype)
    
        m = n // arrays[0].size
    
        out[:,0] = np.repeat(arrays[0], m)
        if arrays[1:]:
            self.cartesian(arrays[1:], out=out[0:m,1:])
            for j in range(1, arrays[0].size):
                out[j*m:(j+1)*m,1:] = out[0:m,1:]
        return out
   
    def LHSampling(self,aPCE):
        from SALib.sample import latin
        names = [aPCE.Inputs.Marginals[i].Name for i in range(aPCE.NofPa)]
        bounds = [aPCE.Inputs.Marginals[i].Parameters for i in range(aPCE.NofPa)]
        dists = [aPCE.Inputs.Marginals[i].DistType for i in range(aPCE.NofPa)]
        
        ProblemDict = {'num_vars': aPCE.NofPa,'names': names,
                           'bounds': bounds,'dists': dists}

        self.X = latin.sample(ProblemDict,self.NrSamples)  
        
    def PCMSampling(self, aPCE):
        
        print("Probablistic collocation method!")
        Cpoints = np.zeros(shape=[aPCE.MaxPceDegree+1,aPCE.NofPa])
        PolynomialPa = lambda NrofPa: np.load(os.getcwd()+'/Polynomials/'+'PolynomialBasis_Pa'+str(NrofPa+1)+'.npy')
        
        for PaIdx in range(aPCE.NofPa):
            # TODO: collocation Points based on roots for MaxDegree+1
#            for degree in range(aPCE.MaxPceDegree):
#                Roots = np.load(os.getcwd()+'/Polynomials/'+'Roots_degree'+str(Inputs.d+1)+'_Pa'+ str(PaIdx+1) + '.npy')
            Roots = np.trim_zeros(np.roots(PolynomialPa(PaIdx)[aPCE.MaxPceDegree+1][::-1])) # [::-1]
            Cpoints[:,PaIdx] = Roots
        #==============================================================================
        #============= Construction of optimal integration points =====================
        #==============================================================================
        # TODO: CLEANUP NEEDED!!!
        #DigitalUniqueCombinations
        arrays=[]
        for i in range(aPCE.NofPa):
            arrays.append(range(1, aPCE.MaxPceDegree+2))
        DigitalUniqueCombinations = self.cartesian(arrays)
        # TODO: DigitalUniqueCombinationsNew =  np.array(np.meshgrid(arrays)).T.reshape(-1,aPCE.MaxPceDegree+2)
        
        DigitalPointsWeight = np.empty(shape=[0, 1])
        
        for i in range(len(DigitalUniqueCombinations)):
            PointsWeight=sum(DigitalUniqueCombinations[i,:])
            DigitalPointsWeight=np.append(DigitalPointsWeight, [[PointsWeight]], axis=0)
    
        # Sorting of Possible Digital Points Weight
        SortDigitalPointsWeight = np.sort(DigitalPointsWeight, axis=0)
        index_SDPWF=np.lexsort(( np.asarray(range(len(DigitalPointsWeight)))[:, None].T, DigitalPointsWeight.T))
        index_SDPW= index_SDPWF.T
        #index_SDPWF=np.lexsort((np.asarray(range(len(DigitalPointsWeight)))[:, None], DigitalPointsWeight))
        #index_SDPW= index_SDPWF
        SortDigitalUniqueCombinations=DigitalUniqueCombinations[index_SDPW[:, 0],:]
    
        # Ranking relatively mean
        Temp=np.empty(shape=[0, aPCE.MaxPceDegree+1])
        for j in range(aPCE.NofPa):
            s=abs(Cpoints[:, j]-np.mean(aPCE.Input_distributions[j,:]))
            Temp=np.append(Temp, [s], axis=0)
    
        temp=Temp.T
    
        temp_sort, index_CP = np.sort(temp, axis=0), np.argsort(temp, axis=0)
    
    
        #SortCpoints=np.sort(Cpoints, axis=0)
        SortCpoints=np.empty(shape=[0, aPCE.MaxPceDegree+1])
    
        for j in range(aPCE.NofPa):
            SortCp=Cpoints[index_CP[:, j], j]
            SortCpoints=np.vstack((SortCpoints, SortCp))
    
    
        # Mapping of Digital Combination to Cpoint Combination
        SortUniqueCombinations=np.empty(shape=[0, aPCE.NofPa])
        for i in range(len(SortDigitalUniqueCombinations)):
            SortUnComb=[]
            for j in range(aPCE.NofPa):
                SortUC=SortCpoints[j, SortDigitalUniqueCombinations[i, j]-1]
                SortUnComb.append(SortUC)
                SortUnicomb=np.asarray(SortUnComb)
            SortUniqueCombinations=np.vstack((SortUniqueCombinations, SortUnicomb))
    
        #--------------------------------
        # Output the collocation points
        #--------------------------------
        OptimalCollocationPointsBase = SortUniqueCombinations[0:self.NrSamples,:]
        
        print('---> Calculations are successfully completed. \n\n')
        self.X = OptimalCollocationPointsBase
        
    
    def LSCMSampling(self):
        # TODO: Least Square Collocation sampling method
        print("Least Square Collocation method!")
        sys.exit('Stop!')


class aPCE:
    
    def __init__(self, InputClass):

        self.Inputs = InputClass
        self.DisplayFlag = False
        self.NofPa = len(InputClass.Marginals)
        self.MaxPceDegree = None
        self.P = None
        self.DegreeArray = None
        self.RegMethod = "ARD"
        
        self.ExpDesign = None
        self.ModelOutputDict= None
        self.BasisDict = {}
        self.ParameterNames = []
        self.OptimalCollocationPointsBase = []
        self.CoeffsDict = {}
        

    def Parameter_Initialization(self):
        """
        Initialization Uncertain Parameters
        """
        MCSize = 10000
        self.Input_distributions = np.empty((self.NofPa,MCSize))
    
        for i in range(self.NofPa):
            if self.Inputs.Marginals[i].DistType == 'unif':
                self.Input_distributions[i,:] = np.random.uniform(self.Inputs.Marginals[i].Parameters[0], self.Inputs.Marginals[i].Parameters[1], MCSize)
            
            elif self.Inputs.Marginals[i].DistType == 'norm':
                self.Input_distributions[i,:] = np.random.normal(self.Inputs.Marginals[i].Parameters[0], self.Inputs.Marginals[i].Parameters[1], MCSize)
            
            elif self.Inputs.Marginals[i].DistType == 'lognorm':
                self.Input_distributions[i,:] = np.random.lognormal(self.Inputs.Marginals[i].Parameters[0], self.Inputs.Marginals[i].Parameters[1], MCSize)
    
        return self.Input_distributions
    
    #--------------------------------------------------------------------------------------------------------
    def Polynomials_Generator(self):
        
        # Initialization for Uncertanties Parameters
        self.Input_distributions = self.Parameter_Initialization()
        
        for PaIdx in tqdm(range(self.NofPa), ascii=True, desc ="Construction of Arbitrary Polynomial Basis"): 
            aPoly_Construction.Function(self.Input_distributions[PaIdx,:], self.MaxPceDegree, PaIdx)
    
    #--------------------------------------------------------------------------------------------------------
    def PolyBasisIndices(self, degree):
        
        #Total number of termsfrom sklearn.metrics import mean_squared_error, r2_score (Standard truncation scheme)
        P = math.factorial(self.NofPa+self.MaxPceDegree)//(math.factorial(self.NofPa) * math.factorial(self.MaxPceDegree))
    
        PossibleDegree = list(range(0,degree+1,1)) * self.NofPa 
        
        
        UniqueDegreeCombinations = np.unique(list(combinations(PossibleDegree, self.NofPa)),axis=0)
    
        # Possible Degree Weight Computation
        DegreeWeight=np.sum(UniqueDegreeCombinations, axis=1)
        
        #Sorting of Possible Degree Weight
        SortDegreeCombinations = UniqueDegreeCombinations[np.lexsort((range(len(DegreeWeight)),DegreeWeight))]
        
        #Select the first P bases
        self.PolynomialDegrees= SortDegreeCombinations[0:P,:]
        
        return self.PolynomialDegrees
    
    #--------------------------------------------------------------------------------------------------------
    def addExpDesign(self):
        self.ExpDesign = ExpDesigns(self)

    def Exe_ExpDesign(self, Model):
        
        # Get the sample for X (User-defined or use GetSample)
        if self.ExpDesign.SamplingMethod is not 'user':
            self.OptimalCollocationPointsBase = self.ExpDesign.GetSample(self)
        else:
            self.OptimalCollocationPointsBase = self.ExpDesign.X
        #sys.exit('STOP')
        # ---- Create ModelOutput ------
        # TODO: Run the ForwardModel here based on the approach user is defiend.
        if self.ExpDesign.Y is None:
            print('Here the forward model needs to be executed!')
            self.ModelOutputDict = Model.ExecuteModel(self.ExpDesign.X) 
            #self.ModelOutputDict = ForwardModel().CompModel(self.OptimalCollocationPointsBase)
        else:
            self.ModelOutputDict = self.ExpDesign.Y
            
        return self.OptimalCollocationPointsBase
    #--------------------------------------------------------------------------------------------------------
    
    def univ_basis_vals(self, ParamtersetsMatrix):
        """
         A function to evaluate univariate regressors along input directions.
        """
        from numpy.polynomial.polynomial import polyval
        # The number of samples for pre-allocation is deduced from the 
        # number of rows in ParamtersetsMatrix:
        NofSamples = ParamtersetsMatrix.shape[0]
        NofPa = ParamtersetsMatrix.shape[1]
        P = self.MaxPceDegree + 2
        
        cwd = os.getcwd()
        PolynomialPa = lambda NrofPa: np.load(cwd+'/Polynomials/'+'PolynomialBasis_Pa'+str(NrofPa+1)+'.npy')

        univ_kernel = lambda Input, Kernel_Degree, NrofPa: polyval(Input, PolynomialPa(NrofPa)[Kernel_Degree]).T
        
        
        # Calculate the univariate polynomials
        
        # Allocate the output matrix
        univ_vals = np.zeros((NofSamples,NofPa, P))
        
        for parIdx in range(NofPa):
            for deg in range(P):
                # case 'arbitrary'
                univ_vals[:,parIdx,deg] = univ_kernel(ParamtersetsMatrix[:,parIdx], deg, parIdx)
                
        
        #print("univ_vals of P_1:\n", univ_vals[0])
        return univ_vals
    #--------------------------------------------------------------------------------------------------------
    def PCE_create_Psi(self,BasisIndices,univ_p_val):
        """
        This function assemble the design matrix Psi from the given basis index 
        set INDICES and the univariate polynomial evaluations univ_p_val.
        """
        Indices = BasisIndices
        # Initialization and consistency checks
        # number of input variables
        NofPa = univ_p_val.shape[1]
        
        # Size of the experimental design
        NofSamples = univ_p_val.shape[0]
        
        # number of basis elements
        P = Indices.shape[0]
        
        # check that the variables have consistent sizes
        if NofPa != Indices.shape[1]:
            raise ValueError("Error: Index and univ_p_val don't seem to have consistent sizes!!")
        
        # Preallocate the Psi matrix for performance
        Psi = np.ones((NofSamples, P))
        
        # Assemble the matrix
        for m in range(Indices.shape[1]):
            aa = np.where(Indices[:,m]>0)[0]
            try:
                Psi[:,aa] = np.multiply(Psi[:,aa] , np.reshape(univ_p_val[:, m, Indices[aa,m]], Psi[:,aa].shape))
            except:
                raise ValueError
        
        return Psi
    
    #--------------------------------------------------------------------------------------------------------
    def RS_Builder(self, PSI, ModelOutput):
        """

        ==================================================
        Automatic Relevance Determination Regression (ARD)
        ==================================================
        
        Fit regression model with Bayesian Ridge Regression.
        
        See :ref:`bayesian_ridge_regression` for more information on the regressor.
        
        Compared to the OLS (ordinary least squares) estimator, the coefficient
        weights are slightly shifted toward zeros, which stabilises them.
        
        The histogram of the estimated weights is very peaked, as a sparsity-inducing
        prior is implied on the weights.
        
        The estimation of the model is done by iteratively maximizing the
        marginal log-likelihood of the observations.

        Bayesian ARD Regression: 
            https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.ARDRegression.html
        
        Bayesian Ridge Regression:
            https://scikit-learn.org/stable/modules/linear_model.html#bayesian-regression
        
        """
        
        if self.RegMethod == 'BRR':
            clf_poly = linear_model.BayesianRidge()
        
        elif self.RegMethod == 'ARD':
            clf_poly = ARDRegression(n_iter=500, tol=0.0001, threshold_lambda=1e5, compute_score=True)
        
        else:
            print("The regression method you requested has not been implemented yet!")
        
        # Fit
        clf_poly.fit(PSI, ModelOutput)
        
        # Select the nonzero entries of coefficients
        # The first column must be kept (For mean calculations)
        nnz_idx = np.nonzero(clf_poly.coef_)[0]
        if len(nnz_idx) > 0 and nnz_idx[0] != 0:
            nnz_idx = np.insert(np.nonzero(clf_poly.coef_)[0], 0, 0)
        
        # Remove the nonzero entries for Bases and PSI if need be
        if len(nnz_idx) > 0:
            PolynomialDegrees_Sparse = self.PolynomialDegrees[nnz_idx]
            PSI_Sparse = PSI[:,nnz_idx]
        else:
            PolynomialDegrees_Sparse = self.PolynomialDegrees
            PSI_Sparse = PSI
        
        #==============================================================================
        #==== Computation of the expansion coefficients for arbitrary Polynomial Chaos
        #==============================================================================
        PsiTPsi = np.dot(PSI_Sparse.T, PSI_Sparse)
        
        if np.linalg.cond(PsiTPsi) > 1e-12 and np.linalg.cond(PsiTPsi) < 1/sys.float_info.epsilon:
            # faster
            coeffs = sp.linalg.solve(PsiTPsi, np.dot(PSI_Sparse.T,ModelOutput))
        else:
            # stabler
            coeffs = np.dot(np.dot(np.linalg.pinv(PsiTPsi), PSI_Sparse.T), ModelOutput)
        
        
        return PolynomialDegrees_Sparse,PSI_Sparse,coeffs
    
    #--------------------------------------------------------------------------------------------------------
    def adaptiveSparseaPCE(self, CollocationPoints, ModelOutput):
        
        
        # Initialization
        self.DegreeArray = np.array(range(1,self.MaxPceDegree+1))
        
        AllCoeffs = {}
        AllIndices_Sparse = {}
        
        # Overall score indicator: this will be used to compare between different
        # methods
        bestScore = -np.inf
        
        #==============================================================================
        #==== basis adaptive polynomial chaos: repeat the calculation by
        #==== increasing polynomial degree until the target accuracy is reached
        #==============================================================================
        # For each degree check all q-norms and choose the best one
        scores = -np.inf*np.ones(self.DegreeArray.shape[0])
        
        for degIdx in range(len(self.DegreeArray)):
            
            # Generate the polynomial basis indices 
            #print('Generating the polynomial basis indices...\n')
            
            BasisIndices = self.PolyBasisIndices(degree = self.DegreeArray[degIdx])
            
            #print('Polynomial basis indices generation complete!\n\n')
            
            # Evaluate the univariate polynomials on ExpDesig
            #print('Evaluating the univariate polynomials on the experimental design...\n')
            
            univ_p_val = self.univ_basis_vals(CollocationPoints)
            
            #print('Evaluation of the univariate polynomials complete!\n\n')
            
            
            # ---- Calulate the cofficients of aPCE ----------------------
            #print('Calculating the coefficients with the Sparse Bayesian Learning algorithm...\n')
            Psi = self.PCE_create_Psi(BasisIndices,univ_p_val)
            
            
            PolynomialDegrees_Sparse, PSI_Sparse, Coeffs = self.RS_Builder(Psi, ModelOutput)
            AllCoeffs[str(degIdx+1)] = Coeffs
            AllIndices_Sparse[str(degIdx+1)] = PolynomialDegrees_Sparse
            
            # Calculate and save the score of LOOCV
            score = self.CorrectedLeaveoutError(PSI_Sparse, Coeffs, ModelOutput)
            
            # Store the score in the scores list 
            scores[degIdx] = score
        
        # ------------------ Summary of results ------------------
        # Select the one with the best score and save the necessary outputs
        
        coeffs = AllCoeffs[str(np.argmax(scores)+1)]
        PolynomialDegrees = AllIndices_Sparse[str(np.argmax(scores)+1)]
        
        # ------------------ Summary of results ------------------
        if self.DisplayFlag == True:
            print('='*50)
            print(' '* 10 +' Summary of results ')
            print('='*50)
            
            print("scores:\n", scores)
            print("Best score's degree:", np.argmax(scores)+1)
            print("NO. of terms:", len(PolynomialDegrees))
            P = math.factorial(self.NofPa+np.argmax(scores)+1)//(math.factorial(self.NofPa) * math.factorial(np.argmax(scores)+1))
            print("Sparsity index:", round(len(PolynomialDegrees)/P, 3))
            
            print("Best Indices:\n", PolynomialDegrees)
            
            print ('='*80)
        
        
        return coeffs, PolynomialDegrees
    
    #--------------------------------------------------------------------------------------------------------
    def CorrectedLeaveoutError(self, TotalPsi, Coeffs, ModelOutputs):
        """
         calculate the LOO error for the OLS regression on regressor matrix
         PSI that generated the coefficients in COEFFICIENTS. If MODIFIED_FLAG
         is set to true, modified leave one out calculation is returned
         (based on Blatman, 2009 (PhD Thesis), pg. 115-116).
        
         
         modified Leave One Out Estimate is given by (eq. 5.10, pg 116):
           Err_loo = mean((M(x^(i))- metaM(x^(i))/(1-h_i)).^2), with
           h_i = tr(Psi*(PsiTPsi)^-1 *PsiT)_i and
             divided by var(Y) (eq. 5.11) and multiplied by the T coefficient (eq 5.13):
           T(P,NCoeff) = NCoeff/(NCoeff-P) * (1 + tr(PsiTPsi)^-1)
    
        This is based on the following paper:
            ""Blatman, G., & Sudret, B. (2011). Adaptive sparse polynomial chaos expansion based on least angle regression. 
            Journal of Computational Physics, 230(6), 2345-2367.""
    
        Psi: the orthogonal polynomials of the response surface
        """
        
            
        Psi = np.array(TotalPsi, dtype = float)
        
        P = Psi.shape[1]  # NrTerm of aPCEs 
        N = Psi.shape[0]  # NrEvaluation (Size of experimental design)
        
        
        # Build the projection matrix
        PsiTPsi = np.dot(Psi.T, Psi)
        
        if np.linalg.cond(PsiTPsi) > 1e-12 and np.linalg.cond(PsiTPsi) < 1/sys.float_info.epsilon:
            #print("cond(PsiTPsi):\n",np.linalg.cond(PsiTPsi))
            # faster
            #M = PsiTPsi\sparse.eye(PsiTPsi.shape).toarray()
            M = sp.linalg.solve(PsiTPsi, sp.sparse.eye(PsiTPsi.shape[0]).toarray())
        else:
            # stabler
            M = np.linalg.pinv(PsiTPsi)
    
        # h factor (the full matrix is not calculated explicitly, 
        # only the trace is to save memory)
        PsiM = np.dot(Psi, M)
        
        h =  np.sum(np.multiply(PsiM,Psi), axis = 1)
    

        # Calculate Error Loocv for each measurement point
        residual = np.zeros((N,1))
        PredictedResidual = np.zeros((N,1))
        
        for i in range(N):
            # Calculate the perdicted residual without considering the ith dataset
            residual[i] = np.dot(Psi[i], Coeffs) - ModelOutputs[i]
            PredictedResidual[i] = residual[i]  / (1 - h[i])

        # Error 
        Err = np.mean(np.square(PredictedResidual))
        # Varience
        varY = np.var(ModelOutputs)
        
        # if there are NaNs, just return an infinite LOO error (this happens,
        # e.g., when a strongly underdetermined problem is solved)
        ErrLoo = Err / varY
        
        
        normEmpErr = np.linalg.norm(residual)**2/len(residual)/varY
        
        # Corrected Error for over-determined system
        if N != P:
            trM = np.trace(M)
            if trM < 0 or abs(trM) > 1e6:
                trM = np.trace(np.linalg.pinv(np.dot(Psi.T,Psi)))

            if N > P:
                T = N/(N-P) * (1 + trM)
            else:
                #warnings.warn("No. of experimental design samples is higher than No. of PCE coefficients!")
                T = np.inf
            CorrectedErrLoo = ErrLoo * T

        else:

            CorrectedErrLoo = ErrLoo

        Q_2 = 1 - CorrectedErrLoo
    
        return Q_2

    #--------------------------------------------------------------------------------------------------------
    
    def BuildPCE(self, Model):
        """
        This function loops over the outputs and each time step/point to compute the PCE
        coefficients.
        
        """
        # Initialization
        class AutoVivification(dict):
            """Implementation of perl's autovivification feature."""
            def __getitem__(self, item):
                try:
                    return dict.__getitem__(self, item)
                except KeyError:
                    value = self[item] = type(self)()
                    return value

        self.CoeffsDict= AutoVivification()
        self.BasisDict = AutoVivification()
        
        # Generate polynomials coeffs up to self.MaxPceDegree
        self.Polynomials_Generator()
        
        # ---- Experimental Design ------
        # Get the collocation points to run the forward model
        self.Exe_ExpDesign(Model)
        CollocationPoints = self.OptimalCollocationPointsBase
        OutputDict = self.ModelOutputDict.copy()
        
        if 'Time Steps' in OutputDict: del OutputDict['Time Steps']
        
        for key , OutputMatrix in OutputDict.items():
            
            for idx in tqdm(range(OutputMatrix.shape[1]), ascii=True, desc ="Polynomial Coefficient of %s"%key):

                self.CoeffsDict[key]["y_"+str(idx+1)] , self.BasisDict[key]["y_"+str(idx+1)]= self.adaptiveSparseaPCE(CollocationPoints, OutputMatrix[:,idx])
             
        return  self.BasisDict, self.CoeffsDict
             

#--------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------

class PostProcessing:
    def __init__(self, aPCE):
        self.PCEMeans = {}
        self.PCEStd = {}
        self.NrofSamples = None
        self.Samples = []
        self.ModelOutputs = []
        self.PCEOutputs = []
        self.plotFlag = False
        self.sobol_cell = {}
        self.total_sobol = {}
    
    
    #--------------------------------------------------------------------------------------------------------
    def PCEMoments(self, aPCE):
        
        for Outkey, ValuesDict in aPCE.CoeffsDict.items():
            
            PCEMean = np.zeros((len(ValuesDict)))
            PCEStd = np.zeros((len(ValuesDict)))
            
            for Inkey, InIdxValues in ValuesDict.items():
                idx = int(Inkey.split('_')[1]) - 1
                coeffs = aPCE.CoeffsDict[Outkey][Inkey]
                
                # Mean = c_0
                PCEMean[idx] = coeffs[0]
                # Std = sum(coeffs[1:]^2)
                PCEStd[idx] = np.sum(np.square(coeffs[1:]))    
                
            self.PCEMeans[Outkey] = PCEMean
            self.PCEStd[Outkey] = PCEStd
    #--------------------------------------------------------------------------------------------------------
    # Define a function for the line plot
    def MomentLineplot(self, x_data, dict_Means, dict_Stds , x_label, SaveFig = False):
        
        # Create the plot object
        NrofOutputs = len(dict_Means)
        Keys = dict_Means.keys()
        fig, ax = plt.subplots(nrows=NrofOutputs, ncols=2)
        
        # Plot the best fit line, set the linewidth (lw), color and
        # transparency (alpha) of the line
        for idx in range(NrofOutputs):
                mean_data = dict_Means[Keys[idx]]
                std_data = dict_Stds[Keys[idx]]
                ax[idx, 0].plot(x_data, mean_data, lw = 2, color = '#539caf', alpha = 1)
                ax[idx, 1].plot(x_data, std_data, lw = 2, color = '#539caf', alpha = 1)

                # Label the axes and provide a title
                ax[idx, 0].set_xlabel(x_label)
                ax[idx, 1].set_xlabel(x_label)
                ax[idx, 0].set_ylabel(Keys[idx])
        
        # Provide a title
        ax[0, 0].set_title("Mean")
        ax[0, 1].set_title("Std")
        plt.tight_layout()
        
        if SaveFig is True:
            # ---------------- Saving the figure and text files -----------------------
            newpath = (r'Outputs') 
            if not os.path.exists(newpath): os.makedirs(newpath)
            fig.savefig('./Outputs/Mean_Std_PCE.svg')   # save the figure to file
            plt.close(fig)
    #--------------------------------------------------------------------------------------------------------
    
    def ValidMetamodel(self,aPCE):
        self.get_Sample(aPCE)
        
        self.eval_Model()
        self.eval_PCEmodel(aPCE)
        
        if self.plotFlag == True:
            self.plotValidation(aPCE)
    #--------------------------------------------------------------------------------------------------------
    def get_Sample(self,aPCE):
        
        from SALib.sample import saltelli
        names = [aPCE.Inputs.Marginals[i].Name for i in range(aPCE.NofPa)]
        bounds = [aPCE.Inputs.Marginals[i].Parameters for i in range(aPCE.NofPa)]
        dists = [aPCE.Inputs.Marginals[i].DistType for i in range(aPCE.NofPa)]
        
        ProblemDict = {'num_vars': aPCE.NofPa,'names': names,
                           'bounds': bounds,'dists': dists}

        self.Samples = saltelli.sample(ProblemDict, self.NrofSamples, calc_second_order=False)
        
    #--------------------------------------------------------------------------------------------------------
    # Evaluate Model
    def eval_Model(self):
        self.ModelOutputs = ForwardModel().CompModel(self.Samples)
        
    #--------------------------------------------------------------------------------------------------------
    
    def eval_PCEmodel(self, aPCE):
        # TODO: Temporary
        self.get_Sample(aPCE)
        
        self.PCEOutputs = {}
        univ_p_val = aPCE.univ_basis_vals(self.Samples)
        
        for Outkey, ValuesDict in aPCE.CoeffsDict.items():
            
            PCEOutputs = np.zeros((len(self.Samples), len(ValuesDict)))
            
            for Inkey, InIdxValues in ValuesDict.items():
                idx = int(Inkey.split('_')[1]) - 1
                PolynomialDegrees = aPCE.BasisDict[Outkey][Inkey]
                coeffs = aPCE.CoeffsDict[Outkey][Inkey]
            
                PSI_Val = aPCE.PCE_create_Psi(PolynomialDegrees, univ_p_val)
                PCEOutputs[:, idx] = np.dot(PSI_Val, coeffs)
                
            self.PCEOutputs[Outkey] = PCEOutputs
                
        return self.PCEOutputs
    
    #--------------------------------------------------------------------------------------------------------
    def plotValidation(self,aPCE):
        
        plt.figure(figsize=(12, 10))
        # get the samples
        X_Val = self.Samples
        
        
        Y_PC_Val = self.ModelOutputs
        Y_Val = self.PCEOutputs
        
        # Fit the data(train the model)
        regression_model = LinearRegression()
        regression_model.fit(Y_PC_Val[:,None], Y_Val[:,None])
        
        # Predict
        x_new = np.linspace(np.min(Y_PC_Val), np.max(Y_PC_Val), 100)
        y_predicted = regression_model.predict(x_new[:, np.newaxis])
        
        plt.scatter(Y_PC_Val, Y_Val, color='gold', linewidth=2)
        plt.plot(x_new, y_predicted, color = 'k')
        
        # Calculate the adjusted R_squared and RMSE
        NofPredictors = aPCE.PolynomialDegrees.shape[1] # the total number of explanatory variables in the model (not including the constant term)
        TotalSampleSize = X_Val.shape[0] #sample size
        R2 = r2_score(Y_PC_Val[:,None], Y_Val[:,None])
        AdjR2 = 1 - (1 - R2) * (TotalSampleSize - 1)/(TotalSampleSize - NofPredictors - 1)
        RMSE = mean_squared_error(Y_PC_Val[:,None], Y_Val[:,None])
        
        plt.annotate('RMSE = '+ str(round(RMSE, 4)) + '\n' + 'Adjusted $R^2$ = '+ str(round(AdjR2, 4)), xy=(0.05, 0.85), xycoords='axes fraction')
    
        plt.ylabel("Original Model")
        plt.xlabel("PCE Model")
    
        
        
        # "Histogram of the weights"
    #    plt.figure(figsize=(12, 10))
    #    plt.title("Histogram of the weights")
    #    plt.hist(MetaModel.coeffs, bins=10, color='navy', log=True)
    #    plt.ylabel("Features")
    #    plt.xlabel("Values of the weights")
    #    plt.legend(loc=1)
        
        # "Marginal log-likelihood"
    #    plt.figure(figsize=(12, 10))
    #    plt.title("Marginal log-likelihood")
    #    plt.plot(MetaModel.scores_, color='navy', linewidth=2)
    #    plt.ylabel("Score")
    #    plt.xlabel("Iterations")
        
        plt.show()
        
    #--------------------------------------------------------------------------------------------------------
    
    def SobolIndicesPCE(self,aPCE):
        """
        Gives information about the importance of parameters
        More Info: Eq. 27 @ Global sensitivity analysis: A flexible and efficient framework with an example from stochastic hydrogeology 
                    S. Oladyshkin, F.P.J. de Barros, W. Nowak  https://doi.org/10.1016/j.advwatres.2011.11.001
        Taken from UQLAB: uq_PCE_sobol_indices.m
        """
        N = aPCE.NofPa
        Coeffs = aPCE.coeffs
        max_order = aPCE.MaxPceDegree
        PolynomialDegree = aPCE.PolynomialDegrees
        NofMeasurements = 1
        
        # Initialize the (cell) array containing the (total) Sobol indices.
        sobol_cell_array = dict.fromkeys(range(1,max_order+1), []) 
        for i_order in range(1,max_order+1):
            ComboSize = int(math.factorial(N) / (math.factorial(abs(N-i_order)) * math.factorial(i_order)))
            sobol_cell_array[i_order] = np.zeros((ComboSize,NofMeasurements))
            
        total_sobol_array = np.zeros((N,NofMeasurements))
        
        # Initialize the cell to store the names of the variables of the Sobol Idx.
        TotalVariance = np.zeros((NofMeasurements))
    
    
        # Loop over all measurement points and calculate sobol indices
        for PointIdx in range(NofMeasurements): 
            PCECoeffs = Coeffs#[:,PointIdx]
            TotalVariance[PointIdx] = np.sum(np.square(PCECoeffs[1:]))
            
            
            nzidx = np.where(PCECoeffs != 0)[0]
            #  Set all the Sobol indices equal to zero if the presence of a null output.
            if len(nzidx) == 0: 
                # This is buggy.
                for i_order in range(max_order):
                    sobol_cell_array[i_order][:, PointIdx] = 0
            
            # Otherwise compute them by summing well-chosen chaos coefficients
            else:
                nz_basis = PolynomialDegree[nzidx] 
                for i_order in range(1,max_order+1):
                   idx = np.where(np.sum(nz_basis > 0, axis = 1) == i_order) 
                   subbasis = nz_basis[idx]
                   Z = np.array(list(combinations(range(N), i_order))) 
    
                   for q in range(Z.shape[0]): 
                       Zq = Z[q]
                       subsubbasis = subbasis[:, Zq]
                       subidx = np.prod(subsubbasis, axis=1)>0
                       sum_ind = nzidx[idx[0][subidx]]
                       sobol_cell_array[i_order][q, PointIdx] = np.sum(np.square(PCECoeffs[sum_ind])) /TotalVariance[PointIdx]
                       
                   
                #print("sobol_cell_array:\n", sobol_cell_array)
                # Compute the TOTAL Sobol indices.
                for ParIdx in range(N):
                    idx = nz_basis[:, ParIdx] > 0
                    sum_ind = nzidx[idx]
    
                    total_sobol_array[ParIdx,PointIdx] = np.sum(np.square(PCECoeffs[sum_ind])) / TotalVariance[PointIdx]
                #print("total_sobol_array:\n",total_sobol_array)
        
        self.sobol_cell = sobol_cell_array
        self.total_sobol = total_sobol_array
        #=========================================================================
        #======     Visualisation of Sobol Indices
        #=========================================================================
        objects = []
        for pid in range(1,N+1):
            objects.append('$P_%s$'%pid)
        
    
        y_pos = np.arange(len(objects))
        
        plt.figure(figsize=(12, 10))
        
        plt.bar(y_pos, total_sobol_array[:,0], align='center', alpha=0.5)
        for i, v in enumerate(total_sobol_array[:,0]):
            plt.text(i-.1, v+.01, str(round(v, 3)), color='blue', fontweight='bold')
        plt.xticks(y_pos, objects)
        plt.ylabel('Total Sobol indices, $S^T$')
        plt.xlabel('Parameters')
        plt.title('Sensitivity analysis')
         
        plt.show()

        
        return self.sobol_cell, self.total_sobol
    
    
    
    