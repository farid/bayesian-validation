#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 14:27:35 2019

@author: farid
"""
import sys
import os
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import PyLink
import aPCE_Farid





if __name__ == "__main__":
    
    #=====================================================
    #=============   COMPUTATIONAL MODEL  ================
    #=====================================================
    Model = PyLink.ForwardModel()
    
    Model.ModelName = 'Davarzani2014a'
    Model.ShellFile = "experiments_davarzani2014a_constTWalls2d.sh"
    Model.ShellKeywords = ['simdirpath', 'inputFile', 'MYFirstAttempt', 'pvdFile']
    Model.InputFile = "davarzani2014a_1.22mps.tpl.input"
    Model.ParameterNames = ['VgN', 'Permeability', 'VgAlpha', 'Snr', 'Porosity']
    Model.Outputs='cumulative mass Loss [kg]', 'e [mmperd]', 'Sw[-]'
    
    Model.MeasurementFile = 'Measurement_Calib.txt'
    Model.OutputFile = "davarzani2014a_1.22mps_1_staggered-storage.csv"
    Model.NameList = ['Times', 'WaterMassLosskgextFac', 'EvaporationRatemmd', 'Sw', 'temperature']
    Model.Factors = [1.15740740740741e-05, 0.09, 1, 1, 1]
    

#    # TODO: Function/Model wrapper needed!
#    
#    # Define model options
#    myModel = aPCE_Farid.ForwardModel()
#    myModel.pyFile = 'Ishigami'
    
    
    #=====================================================
    #=========   PROBABILISTIC INPUT MODEL  ==============
    #=====================================================
    Inputs = aPCE_Farid.Input()

    Inputs.addMarginals()
    Inputs.Marginals[0].Name = 'VgN'
    Inputs.Marginals[0].DistType = 'unif'
    Inputs.Marginals[0].Parameters = [14.0, 20.0]
    
    Inputs.addMarginals()
    Inputs.Marginals[1].Name = 'Permeability'
    Inputs.Marginals[1].DistType = 'unif'
    Inputs.Marginals[1].Parameters = [1.0e-11, 2.0e-10]
    
    Inputs.addMarginals()
    Inputs.Marginals[2].Name = 'VgAlpha'
    Inputs.Marginals[2].DistType = 'unif'
    Inputs.Marginals[2].Parameters = [5e-4, 1.0e-3]
    
    Inputs.addMarginals()
    Inputs.Marginals[3].Name = 'Snr'
    Inputs.Marginals[3].DistType = 'unif'
    Inputs.Marginals[3].Parameters = [0.0095, 0.0125]
    
    Inputs.addMarginals()
    Inputs.Marginals[4].Name = 'Porosity'
    Inputs.Marginals[4].DistType = 'unif'
    Inputs.Marginals[4].Parameters = [0.3, 0.5]
    
    #=====================================================
    #======  POLYNOMIAL CHAOS EXPANSION METAMODELS  ======
    #=====================================================    
    MetaModel = aPCE_Farid.aPCE(Inputs)
    
    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO)estimator is chosen as the final 
    # metamodel.
    MetaModel.MaxPceDegree = 3
    
    # Select the sparse least-square minimization method for 
    # the PCE coefficients calculation:
    MetaModel.RegMethod = 'ARD' # 1)ARD: Bayesian ARD Regression 2)BRR: Bayesian Ridge Regression
    
    # ------ Experimental Design --------
    # Generate an experimental design of size NrExpDesign based on a latin 
    # hypercube sampling of the input model or user-defined values of X and/or Y:
    MetaModel.addExpDesign()
    MetaModel.ExpDesign.NrSamples = 26
    MetaModel.ExpDesign.SamplingMethod = 'PCM' # 1)LHS 2)PCM 3)LSCM 4)user
    MetaModel.ExpDesign.X = np.load('CollocationPoints.npy')    
    
    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # Adaptive sparse arbitrary polynomial chaos expansion
    BasisDict, CoffesDict = MetaModel.BuildPCE(Model)
    
    
    #=====================================================
    #=========  POST PROCESSING OF METAMODELS  ===========
    #=====================================================
    PostPCE = aPCE_Farid.PostProcessing(MetaModel)

    # Compute the moments
    PostPCE.PCEMoments(MetaModel)
    
    # Plot Mean & Std for all Outputs
    # Call the function to create plot
    PostPCE.MomentLineplot(x_data = MetaModel.ModelOutputDict['Time Steps']
             , dict_Means = PostPCE.PCEMeans
             , dict_Stds = PostPCE.PCEStd
             , x_label = 'Time [day]'
             , SaveFig = True)
    
    # Evaluation of surrogate model predictions 
    PostPCE.NrofSamples = 10 # (N+2) * NrofSamples
    PCEOutputs = PostPCE.eval_PCEmodel(MetaModel)
    


#    PostPCE.plotFlag = True
#    
#    PostPCE.ValidMetamodel(MetaModel)
    
    
#    # Sensitivity analysis with Sobol indices
#    PostPCE.SobolIndicesPCE(MetaModel)
#    
    