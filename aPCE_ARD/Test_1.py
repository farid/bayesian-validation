#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 14:27:35 2019

@author: farid
"""

import aPCE_Farid


import numpy as np


import sys

if __name__ == "__main__":
    
    #=====================================================
    #=============   COMPUTATIONAL MODEL  ================
    #=====================================================
    # TODO: Function/Model wrapper needed!
    
    # Define model options
    myModel = aPCE_Farid.ForwardModel()
    myModel.pyFile = 'Ishigami'
    
    
    #=====================================================
    #=========   PROBABILISTIC INPUT MODEL  ==============
    #=====================================================
    Inputs = aPCE_Farid.Input()
    
    Inputs.addMarginals()
    Inputs.Marginals[0].Name = 'X1'
    Inputs.Marginals[0].DistType = 'unif'
    Inputs.Marginals[0].Parameters = [-np.pi, np.pi]
    
    Inputs.addMarginals()
    Inputs.Marginals[1].Name = 'X2'
    Inputs.Marginals[1].DistType = 'unif'
    Inputs.Marginals[1].Parameters = [-np.pi, np.pi]
    
    Inputs.addMarginals()
    Inputs.Marginals[2].Name = 'X3'
    Inputs.Marginals[2].DistType = 'unif'
    Inputs.Marginals[2].Parameters = [-np.pi, np.pi]
    
    
    #=====================================================
    #======  POLYNOMIAL CHAOS EXPANSION METAMODELS  ======
    #=====================================================    
    MetaModel = aPCE_Farid.aPCE(Inputs)
    
    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO)estimator is chosen as the final 
    # metamodel.
    MetaModel.MaxPceDegree = 10
    
    # Select the sparse least-square minimization method for 
    # the PCE coefficients calculation:
    MetaModel.RegMethod = 'ARD' # 1)ARD: Bayesian ARD Regression 2)BRR: Bayesian Ridge Regression
    
    # ------ Experimental Design --------
    # Generate an experimental design of size NrExpDesign based on a latin 
    # hypercube sampling of the input model:
    MetaModel.addExpDesign()
    MetaModel.ExpDesign.NrSamples = 250
    MetaModel.ExpDesign.SamplingMethod = 'LHS'
    
    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # Adaptive sparse arbitrary polynomial chaos expansion
    MetaModel.adaptiveSparseaPCE()
    
    #sys.exit('Finish.')
    
    #=====================================================
    #=========  POST PROCESSING OF METAMODELS  ===========
    #=====================================================
    PostPCE = aPCE_Farid.PostProcessing(MetaModel)
    
    
    # Sensitivity analysis with Sobol indices
    PostPCE.SobolIndicesPCE(MetaModel)
    
    
    # Evaluation of surrogate model predictions 
    PostPCE.NrofSamples = 50
    PostPCE.plotFlag = True
    
    PostPCE.ValidMetamodel(MetaModel)
    
    
    