# -*- coding: utf-8 -*-
"""
Created on Sun Dec 11 14:14:34 2016
#==================================================================================================================================================
# Construction of Optimal Integration Points Placement
# Analytical expression for second order integration
# Author: Dr.-Ing. habil. Sergey Oladyshkin, Farid Mohammadi
# SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 5a, 70569 Stuttgart
# E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de

# The current program is using definition of aPC which is presented in the following manuscript:
# Oladyshkin, S. and W. Nowak. Data-driven uncertainty quantification using the arbitrary polynomial chaos expansion. Reliability Engineering &
System Safety, Elsevier, V. 106, P. 179–190, 2012. DOI: 10.1016/j.ress.2012.05.002.
#==================================================================================================================================================
"""

import math
import numpy as np
import scipy as sp
import aPoly_Construction
import matplotlib.pyplot as plt
import os
import time
from tqdm import tqdm
import sys
from itertools import combinations
# To supress warnings
import warnings
warnings.filterwarnings("ignore")


#------------------------------------------------------------------------------
def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays to get all combination.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n // arrays[0].size

    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in range(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out
#--------------------------------------------------------------------------------------------------------
    
def DegreeCombinations(N,d):
    
    P=math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms
    #------ Polynomail Degree Computation --------
    
    PossibleDegree = list(range(0,d+1,1)) * N 

    UniqueDegreeCombinations = np.unique(list(combinations(PossibleDegree, N)),axis=0)

    # Possible Degree Weight Computation
    DegreeWeight=np.sum(UniqueDegreeCombinations, axis=1)
    
    SortDegreeCombinations = UniqueDegreeCombinations[np.lexsort((range(len(DegreeWeight)),DegreeWeight))]
    
    return SortDegreeCombinations[:P]
#--------------------------------------------------------------------------------------------------------

def Parameter_Initialization(N, MCsize, Parameters, Distributions):
    """
    Initialization Uncertain Parameters
    """
    Input_distributions=np.empty((N,MCsize))

    for i in range(N):
        if Distributions[i] == 'unif':
            Input_distributions[i,:] = np.random.uniform(Parameters[i][0], Parameters[i][1], MCsize)
        
        elif Distributions[i] == 'norm':
            Input_distributions[i,:] = np.random.normal(Parameters[i][0], Parameters[i][1], MCsize)
        
        elif Distributions[i] == 'lognorm':
            Input_distributions[i,:] = np.random.lognormal(Parameters[i][0], Parameters[i][1], MCsize)
    
    return Input_distributions
#--------------------------------------------------------------------------------------------------------

def CollocationPoints(MainDir,N, d,ItrNr, MCsize, Parameters, Distributions):
    # Initialization for Uncertanties Parameters
    print('\n---> Initialization of Uncertain Parameters ...\n')


    P=math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms

    # Initialization Uncertain Parameters
    Input_distributions = Parameter_Initialization(N ,MCsize, Parameters, Distributions)

    ##########################################################################################
    #Initialization of Collocation points  >>>>> This should be according to aPoly_Construction.m
    cpoints=np.empty(shape=[0, d+1])
    #Computation of Arbitratry Polynomials
    print('\n---> Construction of orhtonormal aPC polynomial basis ...\n')
    RootsFileName = []
    PolynomialBasisFileName=[]
    # store the PolynomialBasis and their Roots for degrees of d and d+1
    for degree in range(d+1):
        for i in range(N):
            RootsFileName.append('Roots_d'+str(degree+1)+'_'+str(i+1)+'.txt')
            PolynomialBasisFileName.append('PolynomialBasis_d'+str(degree+1)+'_'+str(i+1)+'.txt')
            aPoly_Construction.Function(Input_distributions[i,:], degree+1,RootsFileName[(degree)*N+i], PolynomialBasisFileName[(degree)*N+i])

    # 
    for i in range(N):
        Roots = np.loadtxt(MainDir+'Polynomial_Roots/'+RootsFileName[N*(d-1)+i], delimiter=',')
        cpoints=np.vstack([cpoints, Roots])
    Cpoints=cpoints.T

    #==============================================================================
    #============= Construction of optimal integration points =====================
    #==============================================================================
    #DigitalUniqueCombinations
    arrays=[]
    for i in range(N):
        arrays.append(range(1, d+2))
    DigitalUniqueCombinations = cartesian(arrays)
    #TODO: DigitalUniqueCombinations =  np.array(np.meshgrid(arrays)).T.reshape(-1,N)
    
    DigitalPointsWeight=np.empty(shape=[0, 1])
    for i in range(len(DigitalUniqueCombinations)):
        PointsWeight=sum(DigitalUniqueCombinations[i,:])
        DigitalPointsWeight=np.append(DigitalPointsWeight, [[PointsWeight]], axis=0)

    # Sorting of Possible Digital Points Weight
    SortDigitalPointsWeight = np.sort(DigitalPointsWeight, axis=0)
    index_SDPWF=np.lexsort(( np.asarray(range(len(DigitalPointsWeight)))[:, None].T, DigitalPointsWeight.T))
    index_SDPW= index_SDPWF.T
    #index_SDPWF=np.lexsort((np.asarray(range(len(DigitalPointsWeight)))[:, None], DigitalPointsWeight))
    #index_SDPW= index_SDPWF
    SortDigitalUniqueCombinations=DigitalUniqueCombinations[index_SDPW[:, 0],:]

    # Ranking relatively mean
    Temp=np.empty(shape=[0, d+1])
    for j in range(N):
        s=abs(Cpoints[:, j]-np.mean(Input_distributions[j,:])) #[:, j]
        Temp=np.append(Temp, [s], axis=0)

    temp=Temp.T

    temp_sort, index_CP = np.sort(temp, axis=0), np.argsort(temp, axis=0)


    #SortCpoints=np.sort(Cpoints, axis=0)
    SortCpoints=np.empty(shape=[0, d+1])

    for j in range(N):
        SortCp=Cpoints[index_CP[:, j], j]
        SortCpoints=np.vstack((SortCpoints, SortCp))
    #SortCpoints=SortCpoint.T


    # Mapping of Digital Combination to Cpoint Combination
    #SortUniqueCombinations=cartesian((SortCpoints[:, 0], SortCpoints[:, 1], SortCpoints[:, 2], SortCpoints[:, 3], SortCpoints[:, 4], SortCpoints[:, 5], SortCpoints[:, 6], SortCpoints[:, 7]))
    SortUniqueCombinations=np.empty(shape=[0, N])
    for i in range(len(SortDigitalUniqueCombinations)):
        SortUnComb=[]
        for j in range(N):
            SortUC=SortCpoints[j, SortDigitalUniqueCombinations[i, j]-1]
            SortUnComb.append(SortUC)
            SortUnicomb=np.asarray(SortUnComb)
        SortUniqueCombinations=np.vstack((SortUniqueCombinations, SortUnicomb))

    #--------------------------------
    # Selection of P best combination
    #--------------------------------
    CollocationPointsBase=SortUniqueCombinations[0:P,:]   #Determined System
    #CollocationPointsBase=SortUniqueCombinations[0:P+ItrNr,:]  #P+NrItr determined System
    
    #Storring the optimal integration Points
    OptimalCollocationPointsBase=CollocationPointsBase
    
    print('---> Calculations are successfully completed. \n\n')
    #print "Cpoints:", Cpoints
    #print'\n'
    #print "CollocationPointsBase:\n",CollocationPointsBase

    #------ Polynomail Degree Computation --------
    #PossibleDegree=range(0,len(Cpoints[:,0]),1) * N
    #PolynomialDegrees=[[] for i in range(d)]
    
    
    #for degree in range(0, d):
    PossibleDegree  =list(range(0,d+1,1)) * N 
    
    from itertools import combinations
    UniqueDegreeCombinations = np.unique(list(combinations(PossibleDegree, N)),axis=0)

    # Possible Degree Weight Computation
    DegreeWeight=np.sum(UniqueDegreeCombinations, axis=1)
    
    #Sorting of Possible Degree Weight
    #np.hstack((UniqueDegreeCombinations,DegreeWeight[:,None]))
    SortDegreeCombinations = UniqueDegreeCombinations[np.lexsort((range(len(DegreeWeight)),DegreeWeight))]
    
    #Set of MultiDim Collocation Points
    # Original
    PolynomialDegrees= SortDegreeCombinations[0:P,:]
    
    #print "SortDegreeCombinations:",SortDegreeCombinations
    #PolynomialDegree = SortDegreeCombinations[0:P+ItrNr,:] #P+NrItr
    ## Option I
    #PolynomialDegrees[degree] = SortDegreeCombinations#[0:P+ItrNr,:]
    
    # Save Input_distributions,OptimalCollocationPointsBase
    newpath = (r'Polynomial_Roots')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    
    np.savetxt("Input_distributions.txt", Input_distributions, delimiter=',')
    np.savetxt("OptimalCollocationPointsBase.txt", OptimalCollocationPointsBase, delimiter=',')
    os.chdir("..")
    
    return Input_distributions.T,OptimalCollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName
#--------------------------------------------------------------------------------------------------------
def Psi_Creater(MainDir,N,d, ParamtersetsMatrix,PolynomialBasisFileName,PolynomialDegrees):
    
    P = math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms
    P_total=len(ParamtersetsMatrix) #Updating values of P
    
    # TODO: Iteration number
    ItrNr = len(PolynomialDegrees) - P
    
    Psi = np.zeros((P+ItrNr , P_total))
    
    #--------------------------------------------------------
    # Create new entries to matrix PolynomialDegrees 
    # considering sobol index
    #--------------------------------------------------------
    if ItrNr != 0:
    
        #--------------------------------------------------------
        # Create Psi matrix based on PolynomialDegrees 
        #--------------------------------------------------------
        # If there is an old Psi matrix(ItrNr != 0), just expand it instead of doing the calculations from scratch

        # Constructing Psi for d=1
        for i in range(P): 
            for j in range(P_total): 
                Psi[i,j]=1
                for ii in range(N):
                    Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d-1)*N+ii], delimiter=',') # First degree
                    #Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d+1)*N+ii], delimiter=',') # d+1 degree
                    
                    Psi[i,j]=Psi[i,j]*np.polyval(Polynomial[PolynomialDegrees[i,ii], range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
 
        #calculate Psi for new terms
        # Extend Psi according to new Combo
        for NewP in range(P,P+ItrNr): 
            for j in range(P_total): 
                Psi[NewP,j]=1
                for ii in range(N):
                    #Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d-1)*N+ii], delimiter=',') # d degree
                    Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+ PolynomialBasisFileName[(d)*N+ii], delimiter=',') #d+1 degree
                    
                    
                    Psi[NewP,j]=Psi[NewP,j]*np.polyval(Polynomial[PolynomialDegrees[NewP ,ii],range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point

    else:
        # This is the initial Psi
        # Constructing Psi for d=1
        for i in range(P): 
            for j in range(P_total): 
                Psi[i,j]=1
                for ii in range(N):
                    Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d-1)*N+ii], delimiter=',') # d degree
                    #Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[ii], delimiter=',') # d+1 degree
                    
                    Psi[i,j]=Psi[i,j]*np.polyval(Polynomial[PolynomialDegrees[i,ii], range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
                    
                    #Psi[i,j]=Psi[i,j]*np.polyval(Polynomial[PolynomialDegrees[i,ii],range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
        
 
    Psi = Psi.T
    
    return Psi
#--------------------------------------------------------------------------------------------------------

def Psi_Creator_Weight(MainDir,N,d,Psi_W_old, ParamtersetsMatrix,PolynomialBasisFileName,PolynomialDegrees,NofMeasurements):
    
    P = math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms
    P_total=len(ParamtersetsMatrix) #Updating values of P
    
    # TODO: Iteration number
    ItrNr = len(PolynomialDegrees) - P
    
    Psi = np.zeros((P+ItrNr , P_total))

    if Psi_W_old is not None:
        #Use the already calculated Psi for old term
        Psi[:Psi_W_old.shape[1],:Psi_W_old.shape[0]] = Psi_W_old.T
    
        #calculate Psi for new terms
        # Extend Psi according to new Combo
        # Each measurement point has its corresponding PolynomialDegrees
        #for idx in range(NofMeasurements):
        for NewP in range(Psi_W_old.shape[1],P+ItrNr): #range(P,P+ItrNr)
            for j in range(P_total): 
                Psi[NewP,j]=1
                for ii in range(N):
                    #Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d-1)*N+ii], delimiter=',') # d degree
                    Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d)*N+ii], delimiter=',') #d+1 degree
                    #Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+ PolynomialBasisFileName[(d+1)*N+ii], delimiter=',') #d+3 degree
                    
                    Psi[NewP,j]=Psi[NewP,j]*np.polyval(Polynomial[PolynomialDegrees[NewP ,ii],range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
                    
                    # New PolynomialDegrees have normal (not reversed order) The same for all locations
                    #Psi[NewP,j]=Psi[NewP,j]*np.polyval(Polynomial[PolynomialDegrees[NewP ,ii],range(len(Polynomial))],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
    else:
        
        # Each measurement point has its corresponding PolynomialDegrees
        #for idx in range(NofMeasurements):
        # Constructing Psi for d=1
        for i in range(P): 
            for j in range(P_total): 
                Psi[i,j]=1
                for ii in range(N):
                    Polynomial = np.loadtxt(MainDir+'/Polynomial_Roots/'+PolynomialBasisFileName[(d-1)*N+ii], delimiter=',') # d+1 degree

                    #Psi[i,j]=Psi[i,j]*np.polyval(Polynomial[PolynomialDegrees[idx][i,ii], range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point
                    Psi[i,j]=Psi[i,j]*np.polyval(Polynomial[PolynomialDegrees[i,ii], range(len(Polynomial)-1,-1,-1)],ParamtersetsMatrix[j,ii]) #Evaluation in each integration point

    Psi = Psi.T
    
    return Psi
#--------------------------------------------------------------------------------------------------------

def RS_Builder(Inputs,ItrNr, Output, CollocationPointsBase,PolynomialDegrees,PolynomialBasisFileName):
    """
    Coeffs_old: Needed for Sobol index calculation
    """
    P=math.factorial(Inputs.NofPa+Inputs.d)//(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d)) #Total number of terms
    
    #==============================================================================
    #====== Setting up of space/time independent matrix of arbitrary polynomials
    #==============================================================================
    #Initialization of matrix Psi for P-polynomials in P_total collocation points: space/time independent matrix PP*P_total (of polynomials)
    # Solving Eq. (4) in the paper
    Psi = Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d, CollocationPointsBase, PolynomialBasisFileName,PolynomialDegrees)

    #==============================================================================
    #==== Computation of the expansion coefficients for arbitrary Polynomial Chaos
    #==============================================================================

    #print ('\n---> Computation of the expansion coefficients ...\n')
    try:    
        Coeffs = sp.linalg.solve(Psi,Output)
    except:
        #Equi-weighted projection, leading to least square fit of coefficients    
        inv_PsiPsi=np.linalg.pinv(np.dot(Psi.T,Psi))    
        Coeffs= np.dot(np.dot(inv_PsiPsi,Psi.T),Output) #[:,None]

   
    # noisyOutput = Output + modelError
#    print "Model Error to build noisy outputs:", modelError
#    noisyOutput = np.asarray([Output[i] * (1 + modelError[i]) for i in range(len(modelError))])
#    noisyOutput = np.asarray([Output[i] + modelError[i] for i in range(len(modelError))])
    
    # -------------------------------------------------------------
    # Save Input_distributions,OptimalCollocationPointsBase
    newpath = (r'Polynomial_Roots')
    if not os.path.exists(newpath): os.makedirs(newpath)
    os.chdir(newpath)
    
    np.savetxt("Coeffs.txt", Coeffs, delimiter=',')
    os.chdir("..")
    
    return Coeffs, Psi
#--------------------------------------------------------------------------------------------------------
    
def BME_Corr_Weight(MainDir,N,d,NofVar,ItrNr,Parametersets, RSTotal, OutputOrig, Observations,MeasurementError, NofMeasurements, MCsize,PolynomialBasisFileName,SparsePolynomialDegrees):
    """
    Calculates the correction factor for BMEs.
    """
    P = math.factorial(N+d)//(math.factorial(N) * math.factorial(d)) #Total number of terms
    ParametersetSize = Parametersets.shape[0]
    OutputRS = np.empty((0, NofMeasurements))

    #Weights = np.zeros((len(SparsePolynomialDegrees[0]), NofVar, ParametersetSize)) 
    BME_RM_ModelWeight = np.zeros((len(SparsePolynomialDegrees[0]), NofVar, ParametersetSize)) 
    BME_RM_DataWeight = np.zeros((len(SparsePolynomialDegrees[0]), NofVar, ParametersetSize))
    BME_Corr = np.zeros((len(SparsePolynomialDegrees[0]),NofVar))
    #Scaling Factor
    Normalization=10**(100) # ???
    
    #for sparseIdx, sparsePolynomialDegrees in enumerate(SparsePolynomialDegrees):
    for VarIdx in tqdm(range(NofVar), ascii=True, desc ="Calculating the BME correction factors"):
        for sparseIdx in range(len(SparsePolynomialDegrees[VarIdx])):  
            # Create Psi Matrix
            Psi = Psi_Creater(MainDir,N,d,Parametersets, PolynomialBasisFileName,SparsePolynomialDegrees[VarIdx][sparseIdx])
        
            #Calculation of the outputs of Response surfaces
            OutputRS = np.dot(Psi, RSTotal[VarIdx][sparseIdx])
            
           # Deviation Computations
            RM_Model_Deviation = np.zeros((ParametersetSize,NofMeasurements)) 
            RM_Data_Deviation = np.zeros((ParametersetSize,NofMeasurements)) 
            for i in range(ParametersetSize):
                RM_Model_Deviation[i] = OutputOrig[VarIdx][i,:] - OutputRS[i, :] # Reduce model- Full Model
                RM_Data_Deviation[i] = Observations[VarIdx+1] - OutputRS[i, :] # Reduce model- Measurement Data

            # Initialization  of Co-Variance Matrix
            # For BME_RM_ModelWeight
            if NofMeasurements == 1:
                RM_Model_Error = np.zeros((NofMeasurements, NofMeasurements), float)
                np.fill_diagonal(RM_Model_Error, np.cov(RM_Model_Deviation.T))
            else: 
                RM_Model_Error = np.cov(RM_Model_Deviation.T)      

            # For BME_RM_DataWeight
            Measurement_Error=np.zeros((NofMeasurements, NofMeasurements), float)
            np.fill_diagonal(Measurement_Error, MeasurementError[VarIdx]**2)


            # Computation of Weight according to the deviations
            for i in range(ParametersetSize):
                #Weights[sparseIdx, VarIdx ,i] = (np.exp(-0.5 * np.dot(np.dot(Deviation[i,:], np.linalg.inv(CovarianceMatrix)), Deviation[i,:])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(CovarianceMatrix)))
                BME_RM_ModelWeight[sparseIdx, VarIdx ,i] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Model_Deviation[i], np.linalg.pinv(RM_Model_Error)), RM_Model_Deviation[i]))
                BME_RM_DataWeight[sparseIdx, VarIdx ,i] = Normalization * 1/(np.sqrt(2*np.pi*np.exp(1))) ** NofMeasurements * np.exp(-0.5 * np.dot(np.dot(RM_Data_Deviation[i], np.linalg.pinv(Measurement_Error)), RM_Data_Deviation[i]))
            

            for i in range(ParametersetSize):
                BME_Corr[sparseIdx,VarIdx] = BME_Corr[sparseIdx,VarIdx] + BME_RM_ModelWeight[sparseIdx, VarIdx ,i] * BME_RM_DataWeight[sparseIdx, VarIdx ,i] / np.sum(BME_RM_DataWeight[sparseIdx, VarIdx ,:])
                
    return BME_Corr
#------------------------------------------------------------------------------

# TODO: Adapt it to the adaptive aPCE
def Sobol_Indices(N,d,PolynomialDegree,Coeffs):
    """
    Gives information about the importance of parameters
    More Info: Eq. 27 @ Global sensitivity analysis: A flexible and efficient framework with an example from stochastic hydrogeology 
                S. Oladyshkin, F.P.J. de Barros, W. Nowak  https://doi.org/10.1016/j.advwatres.2011.11.001
    """
    #P=math.factorial(N+d)/(math.factorial(N) * math.factorial(d)) #Total number of terms
    
    P=len(PolynomialDegree)
    #P1 = len(Coeffs)

    #=========================================================================
    #======     Computation of Sobol Indices
    #=========================================================================
    #Sobol Indices: Computation of Location and Var
    LocationInfo=np.zeros((P,N))
    TermsVar=[0]
    
    for j in range(1,P):
        location=np.where(PolynomialDegree[j] != 0)[0]
        LocationInfo[j,location]=location+1
        TermsVar.append(np.sum(Coeffs[j]**2)/sum(Coeffs[1:P]**2))
    #print "LocationInfo:\n",LocationInfo
    #print "TermsVar:\n", TermsVar
    #print "LocationInfo:\n", LocationInfo
    #Sobol Indices: Initialization and Definitaion of Dublicates
    TempSobol = [0]#[0]
    DublicatesIndex= [0]#[0]
    for j in range(1,P):    
        TempSobol.append(TermsVar[j]) #Initialization
        for i in range(j,P):
            #Dublicates
            if (set(LocationInfo[j,:])==set(LocationInfo[i,:]) and j!=i):
                TempSobol[j]=TermsVar[j]+TermsVar[i]            
                DublicatesIndex.append(i)
    #print "DublicatesIndex:", DublicatesIndex
    
    #Sobol Indices: Final defenition of Indices
    Sobol = []
    SobolInfo=np.empty((0,N))
    for j in range(1,P):
       if  j not in DublicatesIndex:
           
           Sobol.append(TempSobol[j])
           SobolInfo = np.vstack((SobolInfo, LocationInfo[j,:]))  #SobolInfo[i,:]=LocationInfo[j,:]
     
    #SobolInices[i,1]=Sobol[i]
    #SobolInices[i,1:N+2]=SobolInfo[i,:]    
    #print "Sobol:",Sobol
    #print "SobolInfo:",SobolInfo
    SobolIndices=np.hstack((np.array(Sobol)[:,None], SobolInfo))
    
    
    Sobol_sort, index_Sobol = np.sort(Sobol, axis=0), np.argsort(Sobol, axis=0)         
    #SortSobolInices=SobolInices[index_Sobol[len(index_Sobol):1:-1],:] #Sorted Indecis: all in one
    SortSobolIndices= SobolIndices[np.lexsort((SobolIndices[:, 0], ))[::-1]]
    #print "SortSobolIndices:\n",SortSobolIndices
    
    #Total Sobol Indices
    SobolTotal = np.zeros((1,N))
    
    for ii in range(N): 
        location=np.where(PolynomialDegree[:,ii] != 0)[0] #find(PolynomialDegree(:,ii)~=0);
        SobolTotal[0,ii]= sum(Coeffs[location]**2)/sum(Coeffs[1:len(Coeffs)]**2) #[0]
    
    
    print("SobolTotal:",SobolTotal)
    #=========================================================================
    #======     Visualisation of Sobol Indices
    #=========================================================================
    #objects = ('$P_1$', '$P_2$', '$P_3$')
    objects = []
    for pid in range(1,N+1):
        objects.append('$P_%s$'%pid)
    

    y_pos = np.arange(len(objects))
    
    plt.bar(y_pos, SobolTotal[0], align='center', alpha=0.5)
    plt.xticks(y_pos, objects)
    plt.ylabel('Total Sobol indices, $S^T$')
    plt.title('Parameters')
     
    plt.show()
    
    return SobolTotal #Sobol_sort, SortSobolIndices,SobolTotal
#--------------------------------------------------------------------------------------------------------

def NewTerm_Selection(Inputs,ItrNr, NofMeasurements,CollocationPoints, TotalOutput,Observations,PolynomialDegrees,PolynomialBasisFileName):
    """
    This function calculates BME for all possible degree combinations and gives the degree combination with 
    the highest BME value. 
    """
    print("=" * 50)
    print("NewTerm Selection Starts now!")
    print("=" * 50)
        

    P=math.factorial(Inputs.NofPa+Inputs.d)//(math.factorial(Inputs.NofPa) * math.factorial(Inputs.d)) #Total number of terms
    
    LHSamples = CollocationPoints[-1]
    SampleSize = 1
    
    #==========================================================================
    # ============== Generation of PossibleCombination ========================
    #========================================================================== 
    AllPossibleCombination=DegreeCombinations(Inputs.NofPa,Inputs.d+1) # one degree higher 
    PossibleCombination =[[] for varidx in range(Inputs.NofVar)]
    for VarIdx in range(Inputs.NofVar):
        if ItrNr != 1:
            OldPolynomialDegrees = PolynomialDegrees[VarIdx]
        else:
            OldPolynomialDegrees = PolynomialDegrees
         
        PossibleCombination[VarIdx] = [i for i in np.ndarray.tolist(AllPossibleCombination) if i not in np.ndarray.tolist(OldPolynomialDegrees)]#[:10]
    
    print("\n")
    print("Possible Combination:\n",PossibleCombination)
    Nr_of_Sparse_Indexes = len(PossibleCombination[0]) + 1 # One for no new term scenario
    
    # ----------- Necessary Initializations -----------
    #NewPolynomialDegrees = np.zeros((NofVar,Nr_of_Sparse_Indexes,P+ItrNr,N),dtype=int) #,NofMeasurements
    NewPolynomialDegrees = [[[]for i in range(Nr_of_Sparse_Indexes)] for j in range(Inputs.NofVar) ]
    
    #Coeffs = np.zeros((NofVar,Nr_of_Sparse_Indexes,P+ItrNr,NofMeasurements))
    Coeffs = [[[]for i in range(Nr_of_Sparse_Indexes)] for j in range(Inputs.NofVar) ]
    Weight = np.zeros((Inputs.NofVar,Nr_of_Sparse_Indexes,SampleSize))
    RS_Output = np.zeros((Inputs.NofVar,Nr_of_Sparse_Indexes,SampleSize,NofMeasurements))
    Deviation = np.zeros((SampleSize,NofMeasurements))
    BME = np.zeros((Nr_of_Sparse_Indexes,Inputs.NofVar))
    corrBME = np.zeros((Nr_of_Sparse_Indexes,Inputs.NofVar))
    
    #==========================================================================
    # ====================== No addtional term scenario =======================
    #========================================================================== 
    # The overdetermined system of equations is solved here. 
    for VarIdx in range(Inputs.NofVar):
        # Handle the different Polynomial cases
        if ItrNr != 1:
            OldPolynomialDegrees=PolynomialDegrees[VarIdx]
        else:
            OldPolynomialDegrees = PolynomialDegrees
        # Store the Polynomial degrees
        NewPolynomialDegrees[VarIdx][0] = OldPolynomialDegrees
        # --------- Computation of Psi ---------
        # Create PSI for each Option based on MC matrix  LHSamples[:,:N]
        Psi_MC = Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d, np.array([LHSamples]),PolynomialBasisFileName,OldPolynomialDegrees)        # coeffsNoTerm as the first option
        
        itrNr = P - OldPolynomialDegrees.shape[0] #ItrNr-1 # No extra term
        
        CoeffsNoTerm,Psi_RS = RS_Builder(Inputs,itrNr, TotalOutput[VarIdx], CollocationPoints,OldPolynomialDegrees,PolynomialBasisFileName) #NewPolynomialDegrees[sparse_index,NofM]
        
        # --------- Store coeffs and RSOutput of the No term scenario as the 1st option ---------
        Coeffs[VarIdx][0] = CoeffsNoTerm
        RS_Output[VarIdx,0] = np.dot(Psi_MC, CoeffsNoTerm)    
    
    #==========================================================================
    # ============== scenarios with addtional term scenario ===================
    #========================================================================== 
    # Here, the RS coefficients for each possible combination are calculated.
    for VarIdx in range(Inputs.NofVar):
        for sparse_index in tqdm(range(1, Nr_of_Sparse_Indexes), ascii=True, desc ="Building all RS candidates"):
  
            # --------- Generation of PolynomialDegrees ---------
            if ItrNr != 1:# Handling the first iteration
                OldPolynomialDegrees = PolynomialDegrees[VarIdx]
            else:
                OldPolynomialDegrees = PolynomialDegrees
            
            
            # Modify the ItrNr for the Psi extension when the NO-Term scenario was selected previously.
            if len(OldPolynomialDegrees) != P+ItrNr-1:
                #newItrNr = ItrNr - (P+ItrNr-1 - len(OldPolynomialDegrees))
                newItrNr = 1 + len(OldPolynomialDegrees) - P
            else:
                newItrNr = ItrNr
            
            ##NewPolynomialDegrees[VarIdx,sparse_index,:,:] = np.vstack((OldPolynomialDegrees,PossibleCombination[VarIdx][sparse_index-1]))
         
            NewPolynomialDegrees[VarIdx][sparse_index] = np.vstack((OldPolynomialDegrees,PossibleCombination[VarIdx][sparse_index-1]))
            
            Coeffs[VarIdx][sparse_index],Psi_RS = RS_Builder(Inputs,newItrNr, TotalOutput[VarIdx], CollocationPoints,NewPolynomialDegrees[VarIdx][sparse_index],PolynomialBasisFileName) #NewPolynomialDegrees[sparse_index,NofM]
            
    # Here, the RS_Output for each possible combination are calculated.
    for VarIdx in range(Inputs.NofVar):
        for sparse_index in tqdm(range(1, Nr_of_Sparse_Indexes), ascii=True, desc ="Runing all RS candidates"):
            # --------- Computation of Psi ---------
            Psi_MC = Psi_Creater(Inputs.MainDir,Inputs.NofPa,Inputs.d, np.array([LHSamples]),PolynomialBasisFileName,NewPolynomialDegrees[VarIdx][sparse_index])
            
            # --------- Computation RS  ---------
            #RS_Output[VarIdx,sparse_index,:,:] = np.dot(Psi_MC, Coeffs[VarIdx,sparse_index])
            RS_Output[VarIdx,sparse_index] = np.dot(Psi_MC, Coeffs[VarIdx][sparse_index])
    
    #==========================================================================
    # ==================== Likelihood and BME calculations ====================
    #==========================================================================       
    for VarIdx in range(Inputs.NofVar):
        for sparse_index in range(Nr_of_Sparse_Indexes):        
            for i in range(SampleSize):  # Deviation from the last simulation output  
                Deviation[i] = Observations[VarIdx+1] - RS_Output[VarIdx,sparse_index,i]
                
            # --------- Likelihood ---------
            CovarianceMatrix=np.zeros((NofMeasurements, NofMeasurements), float)
            np.fill_diagonal(CovarianceMatrix, Inputs.MeasurementError[VarIdx]**2)
            
            for i in range(SampleSize):
                Weight[VarIdx,sparse_index,i]=(np.exp(-0.5 * np.dot(np.dot(Deviation[i], np.linalg.inv(CovarianceMatrix)), Deviation[i])))/(((2*np.pi)**(NofMeasurements/2)) * np.sqrt(np.linalg.det(CovarianceMatrix)))
                #Weight[sparse_index,i]=(1/(np.sqrt(2*np.pi)*MeasurementError[VarIdx])**(NofMeasurements))*np.exp(-0.5*sum(Deviation[i]**2/MeasurementError[VarIdx]**2))
                
            # --------- BME for each Sparse_Index ---------
            BME[sparse_index,VarIdx] = np.mean(Weight[VarIdx,sparse_index,:])
    
    #==========================================================================
    #==================== BME correction for all options ======================
    #========================================================================== 
    # For all variants
    corrFactorBME = BME_Corr_Weight(Inputs.MainDir,Inputs.NofPa,Inputs.d,Inputs.NofVar,ItrNr,CollocationPoints,Coeffs,
                              TotalOutput,  Observations, Inputs.MeasurementError, NofMeasurements,
                              len(CollocationPoints), PolynomialBasisFileName, NewPolynomialDegrees)
    
    corrBME = np.multiply(BME,corrFactorBME)
    
    #==========================================================================
    #====================== Computation of the DKL ============================
    #==========================================================================     
    CrossEntropy = np.zeros((Nr_of_Sparse_Indexes,Inputs.NofVar))
    DKL = np.zeros((Nr_of_Sparse_Indexes,Inputs.NofVar))
    
    # Computation of Corrected DKL
    for VarIdx in range(Inputs.NofVar):
        for sparse_index in range(Nr_of_Sparse_Indexes):
            
            IndexMinCrossEntropy = Weight[VarIdx,sparse_index,:] / sum(Weight[VarIdx,sparse_index,:]) * np.log(Weight[VarIdx,sparse_index,:])
    
            #CrossEntropy[sparse_index, VarIdx] = -sum(IndexMinCrossEntropy(math.isnan(IndexMinCrossEntropy))) #Cross Entropy - Uni
            CrossEntropy[sparse_index, VarIdx] = -np.nansum(IndexMinCrossEntropy)
                
            # Kullback-Leiber Convergence 
            DKL[sparse_index,VarIdx ] = - CrossEntropy[sparse_index,VarIdx] - np.log(corrBME[sparse_index,VarIdx]) #Bayesian experimental design
            
    
    #==========================================================================
    #====== Select the best from Possible Combinations for each NofVar ========
    #==========================================================================
    Best_idx=[]
    NEWPolynomialDegrees = [[] for j in range(Inputs.NofVar)]

    for VarIdx in range(Inputs.NofVar):
        #BestSparse_idx = np.argsort(BME[:,VarIdx])[-1] # Best sparse index for BME
        #BestSparse_idx = np.argsort(corrBME[:,VarIdx])[-1] # Best sparse index for corrBME
        #BestSparse_idx = np.argsort(DKL[:,VarIdx])[-1] # Best sparse index for DKL
        BestSparse_idx = np.argsort(DKL[:,VarIdx])[0] # Best sparse index for DKL
        
        
        # Handle the situation where new term is not required
        if BestSparse_idx == 0:
            print "Over-determined system needs to be solved!!!"
            if ItrNr != 1:
                NEWPolynomialDegrees[VarIdx] = PolynomialDegrees[VarIdx]
            else:
                NEWPolynomialDegrees[VarIdx] = PolynomialDegrees
        else:
            print "Determined system needs to be solved!!!"
            NEWPolynomialDegrees[VarIdx] = NewPolynomialDegrees[VarIdx][BestSparse_idx]#[VarIdx,BestSparse_idx]
        
        print("BestSparse_idx:",BestSparse_idx)
        print("DKL:\n", DKL[:,VarIdx])
        
        Best_idx.append(BestSparse_idx)

        
    return NEWPolynomialDegrees,Best_idx, NewPolynomialDegrees ,Coeffs,Nr_of_Sparse_Indexes