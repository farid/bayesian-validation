#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 24 08:51:48 2018

@author: farid
"""
import numpy as np


def OutputMatrix(File):
    """Required for reading the output files. e.g. 'davarzani2014a_1.22mps_1_staggered-storage.csv'. """
    data_Output = np.genfromtxt(File, delimiter=",")
    #P_1 = data_Output[:,0]
    #P_2 = data_Output[:,1]
    Weights = data_Output[:,2]
    return Weights

def BME_Calculator(Path, NofVar, NofItr):
    BMEMatrix = np.zeros((NofVar, NofItr))
    for NofV in range(NofVar):
        for i in range(NofItr):
            Weights=OutputMatrix(Path[NofV] + "Weights_Variant_%s_Iterations_%s.txt"%(NofV+1 , i+1))
            BMEMatrix[NofV,i] = np.mean(Weights)
    
    return BMEMatrix
    
def normalize_column(A):
    normed_A=np.empty((len(A), 0))
    for col in range(len(A[0])):
         B = (A[:,col] / np.sum(A[:,col]))
         normed_A=np.hstack((normed_A,B[:,None]))
    return normed_A
    
    
if __name__ == "__main__":   

    Path = ["/temp/farid/LH2/Scripts/NewCodes_04_2018/Output_Mass_Loss/", "/temp/farid/LH2/Scripts/NewCodes_04_2018/Output_Evaporation/"]
    NofVar = 2
    NofItr = 5
    
    BMEMatrix = BME_Calculator(Path, NofVar, NofItr)
    
    
    normed_BayesianEvidenceMatrix=normalize_column(BMEMatrix)
    log_normed_BayesianEvidenceMatrix=np.log10(normed_BayesianEvidenceMatrix)
    