#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 24 08:51:48 2018

@author: farid
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
from scipy import stats
from scipy.stats import norm
import matplotlib.mlab as mlab


def OutputMatrix(File):
    """Required for reading the output files. e.g. 'davarzani2014a_1.22mps_1_staggered-storage.csv'. """
    data_Output = np.genfromtxt(File, delimiter=",")
    
    return data_Output

def plot_Histogram(Post_MC_Vector,ParameterNames,Parameters):
    """
    This Function plots the histogram of posterior parameter sets.
    """
    fig = plt.figure()

    fig.set_figheight(12) #20
    fig.set_figwidth(20)
  
    header1 = []
    Colors = ['b','g','r','c','m']

    # Decide on shape of the figure
    if len(ParameterNames) < 3:
        Shape=(1,6)
    else:
        Shape=(2,6)
        
    # Loop over the paramters and plot their histograms        
    for i,ParameterName in enumerate(ParameterNames):
        if i < 3:
            location = (0, 2*i)
        else:
            location = (1, 2*i-5)
           
        ax1 = plt.subplot2grid(shape=Shape, loc=location, colspan=2)
        x1=Post_MC_Vector[:, i]
        # the histogram of the data
        n, bins, patches = ax1.hist(x1, 15, normed=True, facecolor=Colors[i], alpha=0.75)

        # ------ Fitting data -----------
        # Option I
        xs1 = np.linspace(Parameters[i][0], Parameters[i][1], 400)
        kde1 = stats.gaussian_kde(x1)
        # add a 'best fit' line
        ax1.plot(xs1, kde1(xs1) ,Colors[i]+'--', lw=2)
        
        # Option II
#        (mu, sigma) = norm.fit(x1)
#        # add a 'best fit' line
#        y = mlab.normpdf( bins, mu, sigma)
#        ax1.plot(bins, y, 'r--', linewidth=2)
        
        
        ax1.set_xlabel('Values of p$_%s$'%(i+1)) 
        ax1.set_ylabel('Density')
        ax1.grid(True)
        anchored_text = AnchoredText("$\mu$=%.4g \n $\sigma$=%.4g \n $S$=%.4f \n $k$=%.4f" %(np.mean(x1), np.std(x1), stats.skew(x1), stats.kurtosis(x1)), loc=2)
        ax1.add_artist(anchored_text)
        header1.append('P_%s'%(i+1))

    #plt.suptitle('Probablity Density Function')

    plt.tight_layout()
    plt.show()
    
    
if __name__ == "__main__":   

    Path = "/temp/farid/LH2/Scripts/NewCodes_04_2018/Outputs/"
    File= "Histogram_Posterior_calib.txt"
    ParameterNames = ['RegularizationLowSw','ThermalConductivitySolid', 'Permeability','Porosity', 'ModelError']
    
    
    Parametersets=OutputMatrix(Path+File)
    #print "Parametersets:", Parametersets
    p1Min, p1Max = 0.01, 0.12 # RegularizationLowSw >> ??? - ??? [Mosthaf et al. 2014]
    p2Min, p2Max = 5.5, 6.2  # ThermalConductivitySolid >> 4.3 - 6.3 [Mosthaf et al. 2014]
    p3Min, p3Max = 1.0e-12, 1.5e-10 # Permeability >> 1e-10 - 1e-12 [Mosthaf et al. 2014]
    p4Min, p4Max = 0.3, 0.35   # Porosity >> 0.3 - 0.5 [Mosthaf et al. 2014]
    p5Min, p5Max = -0.001, 0.001   # Model Error >>> Additional unknown
    
    Parameters = [(p1Min,p1Max),(p2Min,p2Max),(p3Min,p3Max),(p4Min,p4Max),(p5Min,p5Max)]
    
    
    plot_Histogram(Parametersets[:,:-1],ParameterNames,Parameters)
    
   
    