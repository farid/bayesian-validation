%% PCE METAMODELING: COMPARISON OF PCE CALCULATION STRATEGIES
%
% This example showcases how various strategies for the computation of
% polynomial chaos expansion (PCE) coefficients can be deployed in UQLab.
% The performance of the resulting metamodels are also compared.
% The full computational model of choice is the Ishigami function,
% a standard benchmark in PCE applications.

%% 1 - INITIALIZE UQLAB
%
% Clear all variables from the workspace, set the random number generator
% for reproducible results, and initialize the UQLab framework:
clearvars
rng(100,'twister')
uqlab

%% 
% Select the function and initial size of the ExpDesign
ExampleName = 'uq_ishigami'; % 1) uq_ishigami 2)uq_borehole
initNSamples = 50; %Recommended: 1) 50 2) 200
EnrichExpDesign = 'LHS'; % 1) D_Optimal 2) LHS
addedNSamples = 5;
ModifiedLOOThreshold = 1.0e-8; %Recommended: 1) 5.0e-8 2) 5.0e-6

%% 2 - COMPUTATIONAL MODEL
%
% The Ishigami function is defined as:
%
% $$Y(\mathbf{x}) = \sin(x_1) + 7 \sin^2(x_2) + 0.1 x_3^4 \sin(x_1)$$
%
% where $x_i \in [-\pi, \pi], \; i = 1,2,3.$
%
% This computation is carried out by the function
% |uq_ishigami(X)| supplied with UQLab.
% The input parameters of this function are gathered into the vector |X|.
% 
% Create a MODEL from the function file:

ModelOpts.mFile = ExampleName; 
myModel = uq_createModel(ModelOpts);

%% 3 - PROBABILISTIC INPUT MODEL
%
% The probabilistic input model consists of three independent uniform 
% random variables:
%
% $$X_i \sim \mathcal{U}(-\pi, \pi), \quad i = 1,2,3$$

%%
% Specify these marginals in UQLab:
if strcmp(ExampleName , 'uq_ishigami')
    for ii = 1:3
        InputOpts.Marginals(ii).Type = 'Uniform';
        InputOpts.Marginals(ii).Parameters = [-pi pi]; 
    end
else
    InputOpts.Marginals(1).Name = 'rw'; % Radius of the borehole
    InputOpts.Marginals(1).Type = 'Gaussian';
    InputOpts.Marginals(1).Parameters = [0.10 0.0161812]; % (m)

    InputOpts.Marginals(2).Name = 'r'; % Radius of influence
    InputOpts.Marginals(2).Type = 'Lognormal';
    InputOpts.Marginals(2).Parameters = [7.71 1.0056]; % (m)

    InputOpts.Marginals(3).Name = 'Tu'; % Transmissivity, upper aquifer
    InputOpts.Marginals(3).Type = 'Uniform';
    InputOpts.Marginals(3).Parameters = [63070 115600]; % (m^2/yr)

    InputOpts.Marginals(4).Name = 'Hu'; % Potentiometric head, upper aquifer
    InputOpts.Marginals(4).Type = 'Uniform';
    InputOpts.Marginals(4).Parameters = [990 1110]; % (m)

    InputOpts.Marginals(5).Name = 'Tl'; % Transmissivity, lower aquifer
    InputOpts.Marginals(5).Type = 'Uniform';
    InputOpts.Marginals(5).Parameters = [63.1 116]; % (m^2/yr)

    InputOpts.Marginals(6).Name = 'Hl'; % Potentiometric head , lower aquifer
    InputOpts.Marginals(6).Type = 'Uniform';
    InputOpts.Marginals(6).Parameters = [700 820]; % (m)

    InputOpts.Marginals(7).Name = 'L'; % Length of the borehole
    InputOpts.Marginals(7).Type = 'Uniform';
    InputOpts.Marginals(7).Parameters = [1120 1680]; % (m)

    InputOpts.Marginals(8).Name = 'Kw'; % Borehole hydraulic conductivity
    InputOpts.Marginals(8).Type = 'Uniform';
    InputOpts.Marginals(8).Parameters = [9855 12045]; % (m/yr)
end
%%
% Create an INPUT object based on the specified marginals:
myInput = uq_createInput(InputOpts);

%% 4 - POLYNOMIAL CHAOS EXPANSION METAMODEL
%
% This section showcases several ways to calculate the polynomial
% chaos expansion (PCE) coefficients.

%%
% Select PCE as the metamodeling tool in UQLab:
MetaOpts.Type = 'Metamodel';
MetaOpts.MetaType = 'PCE';

%% 
% Assign the Ishigami function model as the full computational model 
% of the PCE metamodel:
MetaOpts.FullModel = myModel;

%% 4.1 SparseBayes calculation of the coefficients
%
% This methods automatically use the computational model
% created earlier to gather the model responses on the  nodes.

%%
% Specify the 'SparseBayes' calculation method
MetaOpts.Method = 'SparseBayes';

%%
% Specify the maximum polynomial degree:
MetaOpts.Degree = 1:3; %3:15;

%% 
% Specify a sparse truncation scheme (hyperbolic norm between $0.5 <q < 1.0 $):
MetaOpts.TruncOptions.qNorm = 0.50:0.25:1;
%% 
% Least-square methods rely on the evaluation of the model response on an
% experimental design. The following options configure UQLab to generate an
% experimental design of size $500$ based on a latin hypercube sampling of
% the input model (also available: 'MC', 'Sobol', 'Halton'):
% MetaOpts.ExpDesign.NSamples = 100;
% MetaOpts.ExpDesign.Sampling = 'LHS';

% 
Xinit = uq_getSample(initNSamples, 'LHS');
% Evaluate the full model response at the new sample points:
Yinit = uq_evalModel(myModel,Xinit);

% Give new experimental design
MetaOpts.ExpDesign.X = Xinit;
MetaOpts.ExpDesign.Y = Yinit;

%% 
% Create the SparseBayes-based PCE metamodel:
myPCE_SparseBayes = uq_createModel(MetaOpts);

%%
% Print a report on the calculated coefficients:
uq_print(myPCE_SparseBayes)

%%
% Create a visual representation of the distribution of the coefficients:
uq_display(myPCE_SparseBayes)

% Number of terms
Terms_SparseBayes = nnz(myPCE_SparseBayes.PCE.Coefficients);

%% 5 - SEQUENTIAL EXPERIMENTAL DESIGN
% In this section we review a sequential approach for enriching the initial
% experimental design according to a maximin distance criterion.
Xprev = Xinit;
Yprev = Yinit;
ModifiedLOO = myPCE_SparseBayes.Error.ModifiedLOO;
PCEModel = myPCE_SparseBayes;

while ModifiedLOO > ModifiedLOOThreshold
    
    SeqMetaOpts.FullModel = myModel;
    % Select PCE as the metamodeling tool in UQLab:
    SeqMetaOpts.Type = 'Metamodel';
    SeqMetaOpts.MetaType = 'PCE';

    %% 5.1 Enrichment of ExpDesign
    % TODO: Here we nead a function to sample based on D-Optimal Design
    % Create a new  sample of size $5$ from the input model:
    if strcmp(EnrichExpDesign , 'D_Optimal')
        % D-Optimal Design
        Xnew = D_Optimal_Design(PCEModel, addedNSamples);
    else
        % Random enrichment based on LHS
        Xnew = uq_getSample(addedNSamples);
    end
    %%
    % Evaluate the full model response at the new sample points:
    Ynew = uq_evalModel(myModel,Xnew);

    %%
    % Give new experimental design
    Xfull = [Xprev;Xnew];
    Yfull = [Yprev;Ynew];

    SeqMetaOpts.ExpDesign.X = Xfull;
    SeqMetaOpts.ExpDesign.Y = Yfull;
    
    % save the Experimental Design for next iteration
    Xprev = Xfull;
    Yprev = Yfull;

    %% 5.2 SparseBayes calculation of the coefficients

    % Specify the 'SparseBayes' calculation method
    SeqMetaOpts.Method = 'SparseBayes';

    %%
    % Specify the maximum polynomial degree:
    SeqMetaOpts.Degree = 3:15;
    
    % Specify a sparse truncation scheme (hyperbolic norm between $0.5 <q < 1.0 $):
    SeqMetaOpts.TruncOptions.qNorm = 0.75:0.25:1;
    
    % Create the SparseBayes-based PCE metamodel:
    myPCE_SparseBayes_Seq = uq_createModel(SeqMetaOpts);

    % Print a report on the calculated coefficients:
    uq_print(myPCE_SparseBayes_Seq)
    
    % Extract Modified LOO from Output
    ModifiedLOO = myPCE_SparseBayes_Seq.Error.ModifiedLOO;
    
    PCEModel = myPCE_SparseBayes_Seq;
end

%%
% Print a report on the calculated coefficients:
uq_print(PCEModel)

% Create a visual representation of the distribution of the coefficients:
uq_display(PCEModel)


%% 6 - VALIDATION OF THE METAMODELS

%% 6.1 Generation of a validation set
%
% Create a validation sample of size $10^4$ from the input model:
Xval = uq_getSample(1e4);

%%
% Evaluate the full model response at the validation sample points:
Yval = uq_evalModel(myModel,Xval);

%%
% Evaluate the corresponding responses
% for each of the three PCE metamodels created before:
YSparseBayes = uq_evalModel(myPCE_SparseBayes_Seq,Xval);
% Number of terms
Terms_SparseBayes = nnz(myPCE_SparseBayes_Seq.PCE.Coefficients);

YPCE = {YSparseBayes};

%% 5.2 Comparison of the results
%
% To visually assess the performance of each metamodel, produce scatter
% plots of the metamodel vs. the true response on the validation set:
uq_figure('Position', [50 50 650 600])
methodLabels = {'SparseBayes'};
termsLabels = {Terms_SparseBayes};

for i = 1:length(YPCE)

    subplot(1,1,i)
    hold on
    uq_plot(Yval, YPCE{i}, '+')
    uq_plot([min(Yval) max(Yval)], [min(Yval) max(Yval)])
    hold off
    axis equal 
    axis([min(Yval) max(Yval) min(Yval) max(Yval)]) 
    
    uq_setInterpreters(gca)
    title(strcat(methodLabels{i}, ' (',num2str(termsLabels{i}),' nnz Coeffs)'))
    xlabel('$\mathrm{Y_{true}}$')
    ylabel('$\mathrm{Y_{PC}}$')
    set(gca, 'FontSize', 14)

end