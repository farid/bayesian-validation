function Xnew = D_Optimal_Design(PCEModel, NrofSamples)
%% D-Optimal Design of Experiment
% This example is taken from MATLAB website.

% To avoid changes in original PCEMODEL
currentPCEModel = PCEModel;

% All suggestions
allCandidates = uq_getSample(1e4);

% Add all suggestion as new ExpDesign X
currentPCEModel.ExpDesign.X = allCandidates;

% create the experimental design X
[~, currentPCEModel.ExpDesign.U] = uq_getExpDesignSample(currentPCEModel);
    
% Create PSI matrix A for all suggestions
univ_p_val = uq_PCE_eval_unipoly(currentPCEModel);

% Psi for all candidates
Psi = uq_PCE_create_Psi(currentPCEModel.PCE.Basis.Indices, univ_p_val); % for multiple case: current_model.PCE(current_output)
 


C = [ones(size(Psi,1),1) Psi Psi.^2];   %compute model terms including
                                                         % a constant and all squared terms
R = candexch(C,NrofSamples);  % find a D-optimal NrofSamples-point subset
Xnew = allCandidates(R,:);       % get factor settings

end